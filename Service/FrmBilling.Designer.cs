﻿namespace Service
{
    partial class FrmBilling
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBilling));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panAdd = new System.Windows.Forms.Panel();
            this.btnprintBill = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.btnser = new System.Windows.Forms.Button();
            this.dtpJobCardDate = new System.Windows.Forms.DateTimePicker();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.dataGridJobCard = new System.Windows.Forms.DataGridView();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtgstval = new System.Windows.Forms.TextBox();
            this.txtnetval = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cmbtax = new System.Windows.Forms.ComboBox();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.DataGridDescription = new System.Windows.Forms.DataGridView();
            this.txttottot = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSpares = new System.Windows.Forms.TextBox();
            this.txtOil = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpJobDate = new System.Windows.Forms.DateTimePicker();
            this.txtengine = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRegNo = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtKm = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtJobCardNo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.dataGridServiceAdvice = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtOthers = new System.Windows.Forms.TextBox();
            this.txtotherValu = new System.Windows.Forms.TextBox();
            this.txtWtrService = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtlabour = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtcolor = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.buttrqok = new System.Windows.Forms.Button();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtsparetot = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panAdd.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJobCard)).BeginInit();
            this.grBack.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridServiceAdvice)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panAdd
            // 
            this.panAdd.BackColor = System.Drawing.Color.White;
            this.panAdd.Controls.Add(this.btnprintBill);
            this.panAdd.Controls.Add(this.btnEdit);
            this.panAdd.Controls.Add(this.btnAdd);
            this.panAdd.Controls.Add(this.btnBack);
            this.panAdd.Controls.Add(this.btnSave);
            this.panAdd.Controls.Add(this.btnExit);
            this.panAdd.Controls.Add(this.btnDelete);
            this.panAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panAdd.Location = new System.Drawing.Point(0, 551);
            this.panAdd.Name = "panAdd";
            this.panAdd.Size = new System.Drawing.Size(900, 37);
            this.panAdd.TabIndex = 27;
            // 
            // btnprintBill
            // 
            this.btnprintBill.BackColor = System.Drawing.Color.White;
            this.btnprintBill.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnprintBill.Image = global::Service.Properties.Resources.Preview;
            this.btnprintBill.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnprintBill.Location = new System.Drawing.Point(5, 3);
            this.btnprintBill.Name = "btnprintBill";
            this.btnprintBill.Size = new System.Drawing.Size(77, 31);
            this.btnprintBill.TabIndex = 7;
            this.btnprintBill.Text = "Print";
            this.btnprintBill.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnprintBill.UseVisualStyleBackColor = false;
            this.btnprintBill.Click += new System.EventHandler(this.btnprintBill_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEdit.Image = global::Service.Properties.Resources.if_edit_notes_46798;
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(666, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(74, 31);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.Image = global::Service.Properties.Resources.if_add_46776;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(591, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(74, 31);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(821, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(77, 31);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "Back";
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Image = global::Service.Properties.Resources.if_save_46830__1_;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(741, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(78, 31);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(825, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(77, 31);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelete.Image = global::Service.Properties.Resources.if_trash_46839___Copy;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(743, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(81, 31);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPrint.Image = global::Service.Properties.Resources.Preview;
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(466, 358);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(77, 31);
            this.btnPrint.TabIndex = 6;
            this.btnPrint.Text = "Print";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.UseVisualStyleBackColor = false;
            // 
            // grFront
            // 
            this.grFront.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grFront.Controls.Add(this.btnser);
            this.grFront.Controls.Add(this.dtpJobCardDate);
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.dataGridJobCard);
            this.grFront.Controls.Add(this.btnPrint);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(2, -2);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(899, 549);
            this.grFront.TabIndex = 28;
            this.grFront.TabStop = false;
            this.grFront.Enter += new System.EventHandler(this.grFront_Enter);
            // 
            // btnser
            // 
            this.btnser.BackColor = System.Drawing.Color.White;
            this.btnser.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnser.Image = ((System.Drawing.Image)(resources.GetObject("btnser.Image")));
            this.btnser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnser.Location = new System.Drawing.Point(822, 42);
            this.btnser.Name = "btnser";
            this.btnser.Size = new System.Drawing.Size(73, 30);
            this.btnser.TabIndex = 89;
            this.btnser.Text = "Search";
            this.btnser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnser.UseVisualStyleBackColor = false;
            this.btnser.Click += new System.EventHandler(this.btnser_Click);
            // 
            // dtpJobCardDate
            // 
            this.dtpJobCardDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpJobCardDate.Location = new System.Drawing.Point(773, 13);
            this.dtpJobCardDate.Name = "dtpJobCardDate";
            this.dtpJobCardDate.Size = new System.Drawing.Size(122, 26);
            this.dtpJobCardDate.TabIndex = 2;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(4, 45);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(812, 26);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // dataGridJobCard
            // 
            this.dataGridJobCard.AllowUserToAddRows = false;
            this.dataGridJobCard.BackgroundColor = System.Drawing.Color.White;
            this.dataGridJobCard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridJobCard.Location = new System.Drawing.Point(4, 73);
            this.dataGridJobCard.Name = "dataGridJobCard";
            this.dataGridJobCard.ReadOnly = true;
            this.dataGridJobCard.RowHeadersVisible = false;
            this.dataGridJobCard.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridJobCard.Size = new System.Drawing.Size(891, 462);
            this.dataGridJobCard.TabIndex = 0;
            // 
            // grBack
            // 
            this.grBack.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grBack.Controls.Add(this.panel1);
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.txtSpares);
            this.grBack.Controls.Add(this.txtOil);
            this.grBack.Controls.Add(this.label15);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.dtpJobDate);
            this.grBack.Controls.Add(this.txtengine);
            this.grBack.Controls.Add(this.label16);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.txtRegNo);
            this.grBack.Controls.Add(this.label12);
            this.grBack.Controls.Add(this.txtKm);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.txtCustomer);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.txtMobile);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.txtJobCardNo);
            this.grBack.Controls.Add(this.label13);
            this.grBack.Controls.Add(this.dataGridServiceAdvice);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.textBox1);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.txtOthers);
            this.grBack.Controls.Add(this.txtotherValu);
            this.grBack.Controls.Add(this.txtWtrService);
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.txtlabour);
            this.grBack.Controls.Add(this.label14);
            this.grBack.Controls.Add(this.buttrqok);
            this.grBack.Controls.Add(this.txtprice);
            this.grBack.Controls.Add(this.txtqty);
            this.grBack.Controls.Add(this.label20);
            this.grBack.Controls.Add(this.label19);
            this.grBack.Controls.Add(this.label17);
            this.grBack.Controls.Add(this.txtitem);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.txtsparetot);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(0, -2);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(903, 549);
            this.grBack.TabIndex = 29;
            this.grBack.TabStop = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(20, 81);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(31, 18);
            this.label22.TabIndex = 443;
            this.label22.Text = "GST";
            // 
            // txtgstval
            // 
            this.txtgstval.Location = new System.Drawing.Point(151, 78);
            this.txtgstval.Name = "txtgstval";
            this.txtgstval.Size = new System.Drawing.Size(141, 26);
            this.txtgstval.TabIndex = 442;
            this.txtgstval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtnetval
            // 
            this.txtnetval.Location = new System.Drawing.Point(150, 122);
            this.txtnetval.Name = "txtnetval";
            this.txtnetval.Size = new System.Drawing.Size(141, 26);
            this.txtnetval.TabIndex = 441;
            this.txtnetval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(73, 125);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 18);
            this.label21.TabIndex = 440;
            this.label21.Text = "Net Value";
            // 
            // cmbtax
            // 
            this.cmbtax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbtax.FormattingEnabled = true;
            this.cmbtax.Items.AddRange(new object[] {
            "10.00 AM",
            "10.30 AM",
            "11.00 AM",
            "11.30 AM",
            "12.00 PM",
            "12.30 PM",
            "01.00 PM",
            "01.30 PM",
            "02.00 PM",
            "02.30 PM",
            "03.00 PM",
            "03.30 PM",
            "04.00 PM",
            "04.30 PM",
            "05.00 PM",
            "05.30 PM",
            "06.00 PM",
            "06.30 PM",
            "07.00 PM",
            "07.30 PM",
            "08.00 PM",
            "08.30 PM",
            "09.00 PM"});
            this.cmbtax.Location = new System.Drawing.Point(65, 78);
            this.cmbtax.Name = "cmbtax";
            this.cmbtax.Size = new System.Drawing.Size(84, 26);
            this.cmbtax.TabIndex = 439;
            this.cmbtax.SelectedIndexChanged += new System.EventHandler(this.cmbtax_SelectedIndexChanged);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button18);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.DataGridDescription);
            this.grSearch.Location = new System.Drawing.Point(57, 256);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(394, 299);
            this.grSearch.TabIndex = 397;
            this.grSearch.Visible = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.Location = new System.Drawing.Point(172, 268);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(100, 28);
            this.button18.TabIndex = 394;
            this.button18.Text = "Select (F2)";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(282, 269);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(100, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close (F10)";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(4, 3);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(378, 264);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCommon_CellContentClick);
            this.DataGridCommon.DoubleClick += new System.EventHandler(this.DataGridCommon_DoubleClick);
            this.DataGridCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // DataGridDescription
            // 
            this.DataGridDescription.AllowUserToAddRows = false;
            this.DataGridDescription.BackgroundColor = System.Drawing.Color.White;
            this.DataGridDescription.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridDescription.Location = new System.Drawing.Point(-386, 99);
            this.DataGridDescription.Name = "DataGridDescription";
            this.DataGridDescription.RowHeadersVisible = false;
            this.DataGridDescription.Size = new System.Drawing.Size(382, 283);
            this.DataGridDescription.TabIndex = 0;
            // 
            // txttottot
            // 
            this.txttottot.Location = new System.Drawing.Point(151, 44);
            this.txttottot.Name = "txttottot";
            this.txttottot.Size = new System.Drawing.Size(141, 26);
            this.txttottot.TabIndex = 44;
            this.txttottot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txttottot.TextChanged += new System.EventHandler(this.txttottot_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(74, 47);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 18);
            this.label8.TabIndex = 43;
            this.label8.Text = "Tot Value";
            // 
            // txtSpares
            // 
            this.txtSpares.Location = new System.Drawing.Point(742, 256);
            this.txtSpares.Name = "txtSpares";
            this.txtSpares.Size = new System.Drawing.Size(141, 26);
            this.txtSpares.TabIndex = 39;
            this.txtSpares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSpares.TextChanged += new System.EventHandler(this.txtSpares_TextChanged);
            // 
            // txtOil
            // 
            this.txtOil.Location = new System.Drawing.Point(742, 223);
            this.txtOil.Name = "txtOil";
            this.txtOil.Size = new System.Drawing.Size(141, 26);
            this.txtOil.TabIndex = 38;
            this.txtOil.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOil.TextChanged += new System.EventHandler(this.txtOil_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(653, 263);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 18);
            this.label15.TabIndex = 37;
            this.label15.Text = "Labour Cost";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(654, 230);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 18);
            this.label2.TabIndex = 35;
            this.label2.Text = "Spares Cost";
            // 
            // dtpJobDate
            // 
            this.dtpJobDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpJobDate.Location = new System.Drawing.Point(129, 63);
            this.dtpJobDate.Name = "dtpJobDate";
            this.dtpJobDate.Size = new System.Drawing.Size(149, 26);
            this.dtpJobDate.TabIndex = 18;
            // 
            // txtengine
            // 
            this.txtengine.Location = new System.Drawing.Point(129, 30);
            this.txtengine.Name = "txtengine";
            this.txtengine.Size = new System.Drawing.Size(149, 26);
            this.txtengine.TabIndex = 30;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(74, 34);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 18);
            this.label16.TabIndex = 29;
            this.label16.Text = "Bill No";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(316, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Reg No";
            // 
            // txtRegNo
            // 
            this.txtRegNo.Location = new System.Drawing.Point(374, 30);
            this.txtRegNo.Name = "txtRegNo";
            this.txtRegNo.Size = new System.Drawing.Size(179, 26);
            this.txtRegNo.TabIndex = 0;
            this.txtRegNo.Click += new System.EventHandler(this.txtRegNo_Click);
            this.txtRegNo.TextChanged += new System.EventHandler(this.txtRegNo_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(60, 67);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 18);
            this.label12.TabIndex = 19;
            this.label12.Text = "Bill Date ";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtKm
            // 
            this.txtKm.Location = new System.Drawing.Point(129, 160);
            this.txtKm.Name = "txtKm";
            this.txtKm.Size = new System.Drawing.Size(424, 26);
            this.txtKm.TabIndex = 1;
            this.txtKm.Leave += new System.EventHandler(this.txtKm_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Customer";
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new System.Drawing.Point(129, 95);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(422, 26);
            this.txtCustomer.TabIndex = 5;
            this.txtCustomer.TextChanged += new System.EventHandler(this.txtCustomer_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(71, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Mobile";
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(129, 127);
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(424, 26);
            this.txtMobile.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 164);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Service Adviceor ";
            // 
            // txtJobCardNo
            // 
            this.txtJobCardNo.Location = new System.Drawing.Point(374, 63);
            this.txtJobCardNo.Name = "txtJobCardNo";
            this.txtJobCardNo.Size = new System.Drawing.Size(179, 26);
            this.txtJobCardNo.TabIndex = 20;
            this.txtJobCardNo.Visible = false;
            this.txtJobCardNo.TextChanged += new System.EventHandler(this.txtJobCardNo_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(290, 67);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 18);
            this.label13.TabIndex = 21;
            this.label13.Text = "JobCard No";
            this.label13.Visible = false;
            // 
            // dataGridServiceAdvice
            // 
            this.dataGridServiceAdvice.BackgroundColor = System.Drawing.Color.White;
            this.dataGridServiceAdvice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridServiceAdvice.Location = new System.Drawing.Point(13, 252);
            this.dataGridServiceAdvice.Name = "dataGridServiceAdvice";
            this.dataGridServiceAdvice.RowHeadersVisible = false;
            this.dataGridServiceAdvice.Size = new System.Drawing.Size(532, 252);
            this.dataGridServiceAdvice.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(340, 440);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 18);
            this.label7.TabIndex = 37;
            this.label7.Text = "Spares";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(385, 458);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 18);
            this.label6.TabIndex = 35;
            this.label6.Text = "Oil";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(254, 450);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(157, 26);
            this.textBox1.TabIndex = 401;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(201, 458);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 18);
            this.label10.TabIndex = 400;
            this.label10.Text = "Total";
            // 
            // txtOthers
            // 
            this.txtOthers.Location = new System.Drawing.Point(592, 322);
            this.txtOthers.Name = "txtOthers";
            this.txtOthers.Size = new System.Drawing.Size(141, 26);
            this.txtOthers.TabIndex = 404;
            // 
            // txtotherValu
            // 
            this.txtotherValu.Location = new System.Drawing.Point(742, 322);
            this.txtotherValu.Name = "txtotherValu";
            this.txtotherValu.Size = new System.Drawing.Size(141, 26);
            this.txtotherValu.TabIndex = 405;
            this.txtotherValu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtotherValu.TextChanged += new System.EventHandler(this.txtotherValu_TextChanged);
            // 
            // txtWtrService
            // 
            this.txtWtrService.Location = new System.Drawing.Point(742, 289);
            this.txtWtrService.Name = "txtWtrService";
            this.txtWtrService.Size = new System.Drawing.Size(141, 26);
            this.txtWtrService.TabIndex = 403;
            this.txtWtrService.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWtrService.TextChanged += new System.EventHandler(this.txtWtrService_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(640, 293);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 18);
            this.label11.TabIndex = 402;
            this.label11.Text = "Water Service";
            // 
            // txtlabour
            // 
            this.txtlabour.Location = new System.Drawing.Point(742, 357);
            this.txtlabour.Name = "txtlabour";
            this.txtlabour.Size = new System.Drawing.Size(141, 26);
            this.txtlabour.TabIndex = 409;
            this.txtlabour.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(654, 364);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 18);
            this.label14.TabIndex = 408;
            this.label14.Text = "Total  Value";
            // 
            // txtcolor
            // 
            this.txtcolor.Location = new System.Drawing.Point(151, 10);
            this.txtcolor.Name = "txtcolor";
            this.txtcolor.Size = new System.Drawing.Size(141, 26);
            this.txtcolor.TabIndex = 407;
            this.txtcolor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcolor.TextChanged += new System.EventHandler(this.txtcolor_TextChanged_1);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(74, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 18);
            this.label18.TabIndex = 406;
            this.label18.Text = "Discounts";
            // 
            // buttrqok
            // 
            this.buttrqok.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttrqok.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrqok.Image = global::Service.Properties.Resources.ok;
            this.buttrqok.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttrqok.Location = new System.Drawing.Point(504, 211);
            this.buttrqok.Name = "buttrqok";
            this.buttrqok.Size = new System.Drawing.Size(39, 37);
            this.buttrqok.TabIndex = 438;
            this.buttrqok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrqok.UseVisualStyleBackColor = false;
            this.buttrqok.Click += new System.EventHandler(this.buttrqok_Click);
            // 
            // txtprice
            // 
            this.txtprice.Location = new System.Drawing.Point(395, 220);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(92, 26);
            this.txtprice.TabIndex = 415;
            // 
            // txtqty
            // 
            this.txtqty.Location = new System.Drawing.Point(297, 220);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(92, 26);
            this.txtqty.TabIndex = 414;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(412, 199);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 18);
            this.label20.TabIndex = 413;
            this.label20.Text = "Price";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(308, 199);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 18);
            this.label19.TabIndex = 412;
            this.label19.Text = "Qty";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 199);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 18);
            this.label17.TabIndex = 411;
            this.label17.Text = "Itemname";
            // 
            // txtitem
            // 
            this.txtitem.Location = new System.Drawing.Point(13, 220);
            this.txtitem.Name = "txtitem";
            this.txtitem.Size = new System.Drawing.Size(278, 26);
            this.txtitem.TabIndex = 410;
            this.txtitem.Click += new System.EventHandler(this.txtitem_Click);
            this.txtitem.TextChanged += new System.EventHandler(this.txtitem_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(335, 517);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 18);
            this.label9.TabIndex = 398;
            this.label9.Text = "Total";
            // 
            // txtsparetot
            // 
            this.txtsparetot.Location = new System.Drawing.Point(388, 509);
            this.txtsparetot.Name = "txtsparetot";
            this.txtsparetot.Size = new System.Drawing.Size(157, 26);
            this.txtsparetot.TabIndex = 399;
            this.txtsparetot.TextChanged += new System.EventHandler(this.txtsparetot_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtcolor);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.txtgstval);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtnetval);
            this.panel1.Controls.Add(this.txttottot);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.cmbtax);
            this.panel1.Location = new System.Drawing.Point(591, 385);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 157);
            this.panel1.TabIndex = 444;
            // 
            // FrmBilling
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 592);
            this.Controls.Add(this.panAdd);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.grFront);
            this.MaximizeBox = false;
            this.Name = "FrmBilling";
            this.Text = "Billing ";
            this.Load += new System.EventHandler(this.FrmBilling_Load);
            this.panAdd.ResumeLayout(false);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJobCard)).EndInit();
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridServiceAdvice)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panAdd;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.DateTimePicker dtpJobCardDate;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView dataGridJobCard;
        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.DataGridView dataGridServiceAdvice;
        private System.Windows.Forms.TextBox txtengine;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtJobCardNo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtpJobDate;
        private System.Windows.Forms.TextBox txtKm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMobile;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRegNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txttottot;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSpares;
        private System.Windows.Forms.TextBox txtOil;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.TextBox txtsparetot;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnprintBill;
        private System.Windows.Forms.TextBox txtWtrService;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtotherValu;
        private System.Windows.Forms.TextBox txtOthers;
        private System.Windows.Forms.TextBox txtcolor;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtlabour;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView DataGridDescription;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtitem;
        private System.Windows.Forms.Button buttrqok;
        internal System.Windows.Forms.Button btnser;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtgstval;
        private System.Windows.Forms.TextBox txtnetval;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cmbtax;
        private System.Windows.Forms.Panel panel1;
    }
}
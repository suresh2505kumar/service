﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;

namespace Service
{
    public partial class FrmItem : Form
    {
        public FrmItem()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();


        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);

        SqlCommand qur = new SqlCommand();
        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnGridClose_Click(object sender, EventArgs e)
        {
            grItemGroup.Visible = false;
        }

        private void FrmItem_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            GetData();
            GetItem(0);
            LoadButton(1);
            tax();
        }
        public void tax()
        {
            
            string qur = "select Generalname as tax,uid from generalm where   Typem_uid in (6) and active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            txttax.DataSource = null;
            txttax.DataSource = tab;
            txttax.DisplayMember = "tax";
            txttax.ValueMember = "uid";
            txttax.SelectedIndex = -1;
           
        } 
        private void LoadButton(int Id)
        {
            try
            {
                if (Id == 1) //Load
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                    btnSave.Text = "Save";
                    GetItem(0);
                }
                else if (Id == 2)//Add
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Save";
                }
                else if (Id == 3)//Edit
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            LoadButton(1);
           
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                chckActive.Checked = true;
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void GetData()
        {
            try
            {
                SqlParameter[] para = {
                    new SqlParameter("@TypeM_Uid",5)
                };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralM", para);
                DataGridItemGrp.DataSource = null;
                DataGridItemGrp.AutoGenerateColumns = false;
                DataGridItemGrp.ColumnCount = 3;
                DataGridItemGrp.Columns[0].Name = "Uid";
                DataGridItemGrp.Columns[0].HeaderText = "Uid";
                DataGridItemGrp.Columns[0].DataPropertyName = "Uid";
                DataGridItemGrp.Columns[0].Visible = false;
                DataGridItemGrp.Columns[1].Name = "Name";
                DataGridItemGrp.Columns[1].HeaderText = "Name";
                DataGridItemGrp.Columns[1].DataPropertyName = "GeneralName";
                DataGridItemGrp.Columns[1].Width = 260;
                DataGridItemGrp.Columns[2].Name = "Active";
                DataGridItemGrp.Columns[2].HeaderText = "Active";
                DataGridItemGrp.Columns[2].DataPropertyName = "Active";
                DataGridItemGrp.Columns[2].Visible = false;
                DataGridItemGrp.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        private void txtItemGroup_Click(object sender, EventArgs e)
        {
            try
            {
                grItemGroup.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridItemGrp_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Index = DataGridItemGrp.SelectedCells[0].RowIndex;
                txtItemGroup.Text = DataGridItemGrp.Rows[Index].Cells[1].Value.ToString();
                txtItemGroup.Tag = DataGridItemGrp.Rows[Index].Cells[0].Value.ToString();
                grItemGroup.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool Active = false;
                if (chckActive.Checked == true)
                {
                    Active = true;
                }
                else
                {
                    Active = false;
                }
                if (btnSave.Text == "Save")
                {
                    SqlParameter[] para = {
                        new SqlParameter("@ItemCode",txtItemCode.Text),
                        new SqlParameter("@ItemName",txtItemName.Text),
                        new SqlParameter("@PartNo",txtPartNo.Text),
                        new SqlParameter("@PartName",txtPartName.Text),
                        new SqlParameter("@UOM",cmbUom.Text),
                        new SqlParameter("@itemgroup_Uid",txtItemGroup.Tag),
                        new SqlParameter("@HSNCode",txtHsnCode.Text),
                        new SqlParameter("@Active",Active),
                        new SqlParameter("@ItemUid","0"),
                        new SqlParameter("@Price",Convert.ToDecimal(txtPrice.Text)),
                        new SqlParameter("@tax",txttax.SelectedValue)
                    };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_ItemM", para);
                }
                else
                {
                    SqlParameter[] para = {
                        new SqlParameter("@ItemCode",txtItemCode.Text),
                        new SqlParameter("@ItemName",txtItemName.Text),
                        new SqlParameter("@PartNo",txtPartNo.Text),
                        new SqlParameter("@PartName",txtPartName.Text),
                        new SqlParameter("@UOM",cmbUom.Text),
                        new SqlParameter("@itemgroup_Uid",txtItemGroup.Tag),
                        new SqlParameter("@HSNCode",txtHsnCode.Text),
                        new SqlParameter("@Active",Active),
                        new SqlParameter("@ItemUid",txtItemName.Tag),
                        new SqlParameter("@Price",Convert.ToDecimal(txtPrice.Text)),
                         new SqlParameter("@tax",txttax.SelectedValue)
                    };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_ItemM", para);
                }
                MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearControl();
                GetItem(0);
                LoadButton(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ClearControl()
        {
            txtItemCode.Text = string.Empty;
            txtItemName.Text = string.Empty;
            txtPartNo.Text = string.Empty;
            txtPartName.Text = string.Empty;
            txtHsnCode.Text = string.Empty;
            txtItemGroup.Text = string.Empty;
            txtItemName.Tag = string.Empty;
            txtItemGroup.Tag = string.Empty;
            cmbUom.SelectedIndex = -1;
            txtPrice.Text = string.Empty;
        }

        protected void GetItem(int Uid)
        {
            try
            {
                SqlParameter[] para = {
                    new SqlParameter("@Uid",Uid)
                };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetItemM", para);
                bs.DataSource = dt;
                if (Uid == 0)
                {
                    DataGridItem.DataSource = null;
                    DataGridItem.AutoGenerateColumns = false;
                    DataGridItem.ColumnCount = 9;
                    DataGridItem.Columns[0].Name = "Uid";
                    DataGridItem.Columns[0].HeaderText = "Uid";
                    DataGridItem.Columns[0].DataPropertyName = "Uid";
                    DataGridItem.Columns[0].Visible = false;

                    DataGridItem.Columns[1].Name = "ItemCode";
                    DataGridItem.Columns[1].HeaderText = "ItemCode";
                    DataGridItem.Columns[1].DataPropertyName = "ItemCode";

                    DataGridItem.Columns[2].Name = "ItemName";
                    DataGridItem.Columns[2].HeaderText = "ItemName";
                    DataGridItem.Columns[2].DataPropertyName = "ItemName";
                    DataGridItem.Columns[2].Width = 150;
                    DataGridItem.Columns[3].Name = "PartNo";
                    DataGridItem.Columns[3].HeaderText = "PartNo";
                    DataGridItem.Columns[3].DataPropertyName = "CusItemCode";
                    DataGridItem.Columns[3].Visible = false;

                    DataGridItem.Columns[4].Name = "PartName";
                    DataGridItem.Columns[4].HeaderText = "PartName";
                    DataGridItem.Columns[4].DataPropertyName = "CusItemName";
                    DataGridItem.Columns[4].Visible = false;

                    DataGridItem.Columns[5].Name = "ItemGroup";
                    DataGridItem.Columns[5].HeaderText = "ItemGroup";
                    DataGridItem.Columns[5].DataPropertyName = "GeneralName";

                    DataGridItem.Columns[6].Name = "ItemPrice";
                    DataGridItem.Columns[6].HeaderText = "Price";
                    DataGridItem.Columns[6].DataPropertyName = "ItemPrice";

                    DataGridItem.Columns[7].Name = "UOM";
                    DataGridItem.Columns[7].HeaderText = "UOM";
                    DataGridItem.Columns[7].DataPropertyName = "UOM";
                    DataGridItem.Columns[7].Width = 70;

                    DataGridItem.Columns[8].Name = "tax";
                    DataGridItem.Columns[8].HeaderText = "tax";
                    DataGridItem.Columns[8].DataPropertyName = "tax";
                    DataGridItem.Columns[3].Visible = false;
                    DataGridItem.DataSource = bs;
                }
                else
                {
                    txtItemCode.Text = dt.Rows[0]["ItemCode"].ToString();
                    txtItemName.Text = dt.Rows[0]["ItemName"].ToString();
                    txtPartName.Text = dt.Rows[0]["CusItemName"].ToString();
                    txtHsnCode.Text = dt.Rows[0]["HSNCode"].ToString();
                    txtItemGroup.Text = dt.Rows[0]["GeneralName"].ToString();
                    txtItemName.Tag = dt.Rows[0]["Uid"].ToString();
                    txtItemGroup.Tag = dt.Rows[0]["Itemgroup_uid"].ToString();
                    cmbUom.Text = dt.Rows[0]["Uom"].ToString();
                    txttax.Text = dt.Rows[0]["tax"].ToString();
                    txtPrice.Text = dt.Rows[0]["ItemPrice"].ToString();
                    txtPartNo.Text = dt.Rows[0]["CusItemCode"].ToString();
                    bool Ac = Convert.ToBoolean(dt.Rows[0]["Active"].ToString());
                    if (Ac == true)
                    {
                        chckActive.Checked = true;
                    }
                    else
                    {
                        chckActive.Checked = false;
                    }
                    LoadButton(3);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridItem.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGridItem.Rows[Index].Cells[0].Value.ToString());
                GetItem(Uid);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridItem.SelectedCells[0].RowIndex;
                DialogResult res = MessageBox.Show("Do you Want Delete this Item ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (res == DialogResult.Yes)
                {
                    int Uid = Convert.ToInt32(DataGridItem.Rows[Index].Cells[0].Value.ToString());
                    SqlParameter[] para = {
                        new SqlParameter("@Uid",Uid)
                    };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_DeteItem", para);
                    MessageBox.Show("Record Has been Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    GetItem(0);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtItemGroup_Enter(object sender, EventArgs e)
        {
            txtItemGroup_Click(sender, e);
        }

        private void txtItemGroup_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                grItemGroup.Visible = false;
            }
            else if(e.KeyCode == Keys.Enter)
            {
                int Index = DataGridItemGrp.SelectedCells[0].RowIndex;
                txtItemGroup.Text = DataGridItemGrp.Rows[Index].Cells[1].Value.ToString();
                txtItemGroup.Tag = DataGridItemGrp.Rows[Index].Cells[0].Value.ToString();
                grItemGroup.Visible = false;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridItemGrp.SelectedCells[0].RowIndex;
                txtItemGroup.Text = DataGridItemGrp.Rows[Index].Cells[1].Value.ToString();
                txtItemGroup.Tag = DataGridItemGrp.Rows[Index].Cells[0].Value.ToString();
                grItemGroup.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("ItemCode Like '%{0}%' or ItemName Like '%{1}%'", txtSearch.Text, txtSearch.Text);
        }

        private void txttax_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Windows.Forms;

namespace Service
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
            this.FormClosing += FrmMain_FormClosing;
        }
        MenuStrip MnuStrip;
        ToolStripMenuItem MnuStripItem;
        SQLDBHelper db = new SQLDBHelper();
        public static string MenuKey;
        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    DialogResult result = MessageBox.Show("Do you really want to exit?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                    if (result == DialogResult.Yes)
                    {
                        Environment.Exit(0);
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            MdiClient ctlmdi;
            foreach (Control ctl in this.Controls)
            {
                try
                {
                    ctlmdi = (MdiClient)ctl;
                    ctlmdi.BackColor = this.BackColor;
                }
                catch (InvalidCastException)
                {
                }
            }
            MnuStrip = new MenuStrip();
            this.Controls.Add(MnuStrip);
            LeadParentMenu();
        }
        protected void LeadParentMenu()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@UserId", FrmLogin.UserId) };
                DataTable dt = db.GetData(CommandType.StoredProcedure,"SP_GetParentMenu", para);
                foreach (DataRow dr in dt.Rows)
                {
                    MnuStripItem = new ToolStripMenuItem(dr["MAINMNU"].ToString());
                    SubMenu(MnuStripItem, Convert.ToInt32(dr["MENUPARVAL"].ToString()));
                    MnuStrip.Items.Add(MnuStripItem);
                }
                this.MainMenuStrip = MnuStrip;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        public void SubMenu(ToolStripMenuItem mnu, int submenu)
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@Submenu", submenu), new SqlParameter("@UserId", FrmLogin.UserId) };
                DataTable dtchild = db.GetData(CommandType.StoredProcedure, "SP_GetSubMenu", para);
                foreach (DataRow dr in dtchild.Rows)
                {
                    ToolStripMenuItem SSMenu = new ToolStripMenuItem(dr["FRM_NAME"].ToString(), null, new EventHandler(ChildClick));
                    //LoadSubMneuofSub(SSMenu, dr["MNUSUBMENU"].ToString());
                    mnu.DropDownItems.Add(SSMenu);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void ChildClick(object sender, EventArgs e)
        {
            SqlParameter[] para = { new SqlParameter("@Frm_Name", sender.ToString()) };
            DataTable dtransaction = db.GetData(CommandType.StoredProcedure, "SP_getFrom", para);
            Assembly frmAssembly = Assembly.LoadFile(Application.ExecutablePath);
            foreach (Type type in frmAssembly.GetTypes())
            {
                //MessageBox.Show(type.Name);
                if (type.BaseType == typeof(Form))
                {
                    if (type.Name == dtransaction.Rows[0][0].ToString())
                    {
                        MenuKey = dtransaction.Rows[0][1].ToString();
                        Form frmShow = (Form)frmAssembly.CreateInstance(type.ToString());
                        // then when you want to close all of them simple call the below code

                        foreach (Form form in this.MdiChildren)
                        {
                            form.Close();
                        }
                        frmShow.MdiParent = this;
                        frmShow.MdiParent = this;
                        frmShow.StartPosition = FormStartPosition.CenterScreen;
                        //frmShow.ControlBox = false;
                        frmShow.Show();
                    }
                }
            }
        }
    }
}

﻿namespace Service
{
    partial class FrmMec
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMec));
            this.Genpan = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.EditPnl = new System.Windows.Forms.Panel();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.butnsave = new System.Windows.Forms.Button();
            this.txtmachinecopde = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtgen = new System.Windows.Forms.TextBox();
            this.lblgen = new System.Windows.Forms.Label();
            this.button21 = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.panAdd = new System.Windows.Forms.Panel();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.EditPnl.SuspendLayout();
            this.panAdd.SuspendLayout();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.label1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Location = new System.Drawing.Point(0, 0);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(488, 313);
            this.Genpan.TabIndex = 84;
            this.Genpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Genpan_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 20);
            this.label1.TabIndex = 87;
            this.label1.Text = "Mechanic Master";
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP.Location = new System.Drawing.Point(6, 74);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(479, 227);
            this.HFGP.TabIndex = 3;
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(6, 50);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(352, 22);
            this.txtscr1.TabIndex = 1;
            // 
            // EditPnl
            // 
            this.EditPnl.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.EditPnl.Controls.Add(this.btnaddrcan);
            this.EditPnl.Controls.Add(this.butnsave);
            this.EditPnl.Controls.Add(this.txtmachinecopde);
            this.EditPnl.Controls.Add(this.label2);
            this.EditPnl.Controls.Add(this.txtgen);
            this.EditPnl.Controls.Add(this.lblgen);
            this.EditPnl.Location = new System.Drawing.Point(0, 0);
            this.EditPnl.Name = "EditPnl";
            this.EditPnl.Size = new System.Drawing.Size(488, 307);
            this.EditPnl.TabIndex = 92;
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(254, 190);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 30);
            this.btnaddrcan.TabIndex = 203;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // butnsave
            // 
            this.butnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.butnsave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butnsave.Image = global::Service.Properties.Resources.if_save_46830__1_;
            this.butnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butnsave.Location = new System.Drawing.Point(178, 190);
            this.butnsave.Name = "butnsave";
            this.butnsave.Size = new System.Drawing.Size(75, 30);
            this.butnsave.TabIndex = 202;
            this.butnsave.Text = "Save";
            this.butnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butnsave.UseVisualStyleBackColor = false;
            this.butnsave.Click += new System.EventHandler(this.butnsave_Click);
            // 
            // txtmachinecopde
            // 
            this.txtmachinecopde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmachinecopde.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmachinecopde.Location = new System.Drawing.Point(41, 136);
            this.txtmachinecopde.Margin = new System.Windows.Forms.Padding(4);
            this.txtmachinecopde.Name = "txtmachinecopde";
            this.txtmachinecopde.Size = new System.Drawing.Size(416, 22);
            this.txtmachinecopde.TabIndex = 93;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(38, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 18);
            this.label2.TabIndex = 95;
            this.label2.Text = "Mechanic Code";
            // 
            // txtgen
            // 
            this.txtgen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgen.Location = new System.Drawing.Point(41, 76);
            this.txtgen.Margin = new System.Windows.Forms.Padding(4);
            this.txtgen.Name = "txtgen";
            this.txtgen.Size = new System.Drawing.Size(416, 22);
            this.txtgen.TabIndex = 92;
            // 
            // lblgen
            // 
            this.lblgen.AutoSize = true;
            this.lblgen.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblgen.Location = new System.Drawing.Point(38, 53);
            this.lblgen.Name = "lblgen";
            this.lblgen.Size = new System.Drawing.Size(116, 18);
            this.lblgen.TabIndex = 91;
            this.lblgen.Text = "Mechanic Name";
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.White;
            this.button21.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button21.Image = global::Service.Properties.Resources.if_trash_46839;
            this.button21.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button21.Location = new System.Drawing.Point(157, 4);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(84, 30);
            this.button21.TabIndex = 242;
            this.button21.Text = "Delete";
            this.button21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button21.UseVisualStyleBackColor = false;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = global::Service.Properties.Resources.if_delete_46795;
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(247, 4);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(71, 30);
            this.buttnext1.TabIndex = 243;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = global::Service.Properties.Resources.if_edit_notes_467981;
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(80, 4);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(71, 30);
            this.butedit.TabIndex = 245;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Image = global::Service.Properties.Resources.if_add_46776;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(3, 4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(71, 30);
            this.button6.TabIndex = 244;
            this.button6.Text = "Add ";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // panAdd
            // 
            this.panAdd.BackColor = System.Drawing.Color.White;
            this.panAdd.Controls.Add(this.buttnext1);
            this.panAdd.Controls.Add(this.butedit);
            this.panAdd.Controls.Add(this.button21);
            this.panAdd.Controls.Add(this.button6);
            this.panAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panAdd.Location = new System.Drawing.Point(3, 309);
            this.panAdd.Name = "panAdd";
            this.panAdd.Size = new System.Drawing.Size(485, 37);
            this.panAdd.TabIndex = 93;
            // 
            // FrmMec
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 351);
            this.Controls.Add(this.panAdd);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.EditPnl);
            this.Name = "FrmMec";
            this.Text = "Mechanic Master";
            this.Load += new System.EventHandler(this.FrmMec_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.EditPnl.ResumeLayout(false);
            this.EditPnl.PerformLayout();
            this.panAdd.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.Panel EditPnl;
        private System.Windows.Forms.TextBox txtmachinecopde;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtgen;
        private System.Windows.Forms.Label lblgen;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button butnsave;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel panAdd;
    }
}
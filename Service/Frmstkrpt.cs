﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Data.OleDb;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



namespace Service

{

    public partial class Frmstkrpt : Form
    {

        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;


        private System.ComponentModel.Container components = null;

        ReportDocument doc = new ReportDocument();
        public Frmstkrpt()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }

        int uid = 0;
        int mode = 0;
        string tpuid = "";
        string MenuKey;
        private static Microsoft.Office.Interop.Excel.Workbook mWorkBook;
        private static Microsoft.Office.Interop.Excel.Sheets mWorkSheets;
        private static Microsoft.Office.Interop.Excel.Worksheet mWSheet1;
        private static Microsoft.Office.Interop.Excel.Application oXL;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();



        private void Titlep1()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";

            checkColumn.TrueValue = true;
            checkColumn.FalseValue = false;
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            HFGP.Columns.Add(checkColumn);
        }


        private void Titlep2()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            HFIT.Columns.Add(checkColumn);


        }

        private void Frmstkrpt_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 11, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 11, FontStyle.Bold);
            Genclass.Module.buttonstyleform(this);

            HFIT.RowHeadersVisible = false;

            HFGP.RowHeadersVisible = false;
            conn.Close();
            conn.Open();
            qur.CommandText = "delete from tmpitid";
            qur.ExecuteNonQuery();


            Titlep1();
            Titlep2();

            MenuKey = FrmMain.MenuKey;
            if (MenuKey == "10")
            {
                label4.Text = "Type";
                label1.Text = "Stock Register";
                cbogrp.Text = "Stock Statement";
                Genclass.Dtype = 1;

            }
            else if (MenuKey == "11")
            {
                cbogrp.Text = "Stock Ledger";
                Genclass.Dtype = 2;
            }
            loaditem();


        }

        private void loaditem()
        {
            conn.Close();
            conn.Open();

            Genclass.StrSrch = "";
            Genclass.StrSrch1 = "";
            Genclass.FSSQLSortStr = "Itemname";
            Genclass.FSSQLSortStr1 = "itemcode";

            if (Txtitem.Text != "")
            {
                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + Txtitem.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + Txtitem.Text + "%'";
                }
            }

            if (txtcode.Text != "")
            {
                if (Genclass.StrSrch1 == "")
                {
                    Genclass.StrSrch1 = Genclass.FSSQLSortStr1 + " like '%" + txtcode.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch1 = Genclass.StrSrch1 + " and " + Genclass.FSSQLSortStr1 + " like '%" + txtcode.Text + "%'";
                }
            }
            if (Txtitem.Text != "")
            {
                Genclass.StrSrch = " " + Genclass.StrSrch;
            }

            else
            {
                Genclass.StrSrch = "uid <> 0";
            }


            if (txtcode.Text != "")
            {

                Genclass.StrSrch1 = " " + Genclass.StrSrch1;

            }
            else
            {
                Genclass.StrSrch1 = "uid <> 0";
            }

            string quy = "select Itemname as Item,itemcode,uid from Itemm where active=1 and  " + Genclass.StrSrch + "  and " + Genclass.StrSrch1 + "  ";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);


            HFGP.AutoGenerateColumns = false;
            HFGP.Refresh();
            HFGP.DataSource = null;
            HFGP.Rows.Clear();


            HFGP.ColumnCount = tap.Columns.Count + 1;
            Genclass.i = 1;
            foreach (DataColumn column in tap.Columns)
            {
                HFGP.Columns[Genclass.i].Name = column.ColumnName;
                HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            HFGP.Columns[1].Width = 300;
            HFGP.Columns[2].Width = 150;
            HFGP.Columns[3].Visible = false;
            HFGP.DataSource = tap;
        }

        private void loadParty()
        {
            conn.Close();
            conn.Open();

            Genclass.StrSrch = "";
            Genclass.FSSQLSortStr = "Name";

            if (Txtitem.Text != "")
            {
                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + Txtitem.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + Txtitem.Text + "%'";
                }
            }

            if (Txtitem.Text != "")
            {
                Genclass.StrSrch = " " + Genclass.StrSrch;
            }
            else
            {
                Genclass.StrSrch = "uid <> 0";
            }

            string quy = "select Name as Party,Address1,uid from partym where active=1 and  " + Genclass.StrSrch + " order by name";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);


            HFGP.AutoGenerateColumns = false;
            HFGP.Refresh();
            HFGP.DataSource = null;
            HFGP.Rows.Clear();


            HFGP.ColumnCount = tap.Columns.Count + 1;
            Genclass.i = 1;
            foreach (DataColumn column in tap.Columns)
            {
                HFGP.Columns[Genclass.i].Name = column.ColumnName;
                HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }
            HFGP.Columns[3].Visible = false;
            HFGP.Columns[2].Width = 350;
            HFGP.Columns[1].Width = 250;
            HFGP.DataSource = tap;
        }



        private void chkact_Click(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in HFGP.Rows)
            //{
            //    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];

            //    if (chk.Value == chk.FalseValue || chk.Value == null)
            //    {
            //        chk.Value = chk.TrueValue;
            //    }
            //    else
            //    {
            //        chk.Value = chk.FalseValue;
            //    }

            //}

            //HFGP.EndEdit();

            if (chkact.Checked == true)
            {
                for (int i = 0; i < HFGP.RowCount; i++)
                {
                    HFGP[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < HFGP.RowCount; i++)
                {
                    HFGP[0, i].Value = false;
                }
            }

        }

        private void HFGP_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
            ch1 = (DataGridViewCheckBoxCell)HFGP.Rows[HFGP.CurrentRow.Index].Cells[0];

            if (ch1.Value == null)
                ch1.Value = false;
            switch (ch1.Value.ToString())
            {
                case "True":
                    {
                        ch1.Value = false;

                        break;
                    }
                case "False":
                    {
                        ch1.Value = true;
                        //Where should I put the selected cell here?
                        break;
                    }
            }
        }

        private void chkact1_Click(object sender, EventArgs e)
        {
            if (chkact1.Checked == true)
            {
                for (int i = 0; i < HFIT.RowCount; i++)
                {
                    if (HFGP.Rows[i].Cells[1].Value.ToString() != "")
                    {
                        HFIT[0, i].Value = true;
                    }
                }
            }
            else
            {
                for (int i = 0; i < HFIT.RowCount; i++)
                {
                    if (HFGP.Rows[i].Cells[1].Value.ToString() != "")
                    {
                        HFIT[0, i].Value = false;
                    }
                }
            }
        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
            ch1 = (DataGridViewCheckBoxCell)HFIT.Rows[HFIT.CurrentRow.Index].Cells[0];

            if (ch1.Value == null)
                ch1.Value = false;
            switch (ch1.Value.ToString())
            {
                case "True":
                    {
                        ch1.Value = false;

                        break;
                    }
                case "False":
                    {
                        ch1.Value = true;
                        //Where should I put the selected cell here?
                        break;
                    }
            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            //qur.CommandText = "drop table #tmpitid";
            //qur.ExecuteNonQuery();
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int i = 0;
            conn.Close();
            conn.Open();


            foreach (DataGridViewRow row in HFGP.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (cbogrp.Text == "Stock Statement" || cbogrp.Text == "Stock Ledger")
                {
                    if (Convert.ToBoolean(row.Cells[0].Value) == true)
                    {
                        qur.CommandText = "delete from tmpitid where itemid=" + HFGP.Rows[i].Cells[3].Value.ToString() + "";
                        qur.ExecuteNonQuery();
                        qur.CommandText = "insert into tmpitid values (" + HFGP.Rows[i].Cells[3].Value.ToString() + ")";
                        qur.ExecuteNonQuery();
                    }
                }

                else

                {
                    if (HFGP.Rows.Count > 1)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {
                            qur.CommandText = "delete from tmpitid where itemid=" + HFGP.Rows[i].Cells[3].Value.ToString() + "";
                            qur.ExecuteNonQuery();
                            qur.CommandText = "insert into tmpitid values (" + HFGP.Rows[i].Cells[3].Value.ToString() + ")";
                            qur.ExecuteNonQuery();
                        }
                    }
                }

                i = i + 1;

            }
            if (Genclass.Gbtxtid == 210 || Genclass.Gbtxtid == 230)
            {
                Loadfitparty();
            }

            else
            {
                Loadhfit();
            }



        }

        private void button2_Click(object sender, EventArgs e)
        {
            int i = 0;

            foreach (DataGridViewRow row in HFIT.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];

                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    conn.Close();
                    conn.Open();
                    qur.CommandText = "delete from tmpitid where itemid=" + HFIT.Rows[i].Cells[2].Value.ToString() + "";
                    qur.ExecuteNonQuery();

                }

                i = i + 1;

            }

            Loadhfit();
        }


        private void Loadhfit()
        {
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            string quy = "select b.itemname,a.itemid from tmpitid a inner join itemm b on a.itemid=b.uid ";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            HFIT.DataSource = tap;


            HFIT.Columns[1].Name = "Selected Item";
            HFIT.Columns[2].Name = "uid";

            HFIT.Columns[1].Width = 350;
            HFIT.Columns[2].Visible = false;

            HFIT.AutoGenerateColumns = false;
        }
        private void Loadfitparty()
        {
            //HFIT.Refresh();
            //HFIT.DataSource = null;
            //HFIT.Rows.Clear();

            string quy = "select b.name,a.itemid from tmpitid a inner join partym b on a.itemid=b.uid ";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            HFIT.DataSource = tap;


            HFIT.Columns[1].Name = "Selected Party";
            HFIT.Columns[2].Name = "uid";

            HFIT.Columns[1].Width = 310;
            HFIT.Columns[2].Visible = false;

            HFIT.AutoGenerateColumns = false;
        }
        private void Txtitem_TextChanged(object sender, EventArgs e)
        {
            conn.Close();
            if (Genclass.Gbtxtid == 230)
            {
                loadParty();

            }
            else
            {
                loaditem();
            }

        }

        private void cmdprt_Click(object sender, EventArgs e)
        {
            if (MenuKey == "10")
            {
                conn.Close();
                conn.Open();

                if (cbogrp.Text == "Stock Statement")
                {
                    Genclass.Dtype = 1;

                    int i = HFGP.SelectedCells[0].RowIndex;

                    qur.CommandText = "Exec SP_stkstmt '" + Fromdt.Text + "','" + Todt.Text + "'";
                    qur.ExecuteNonQuery();

                    textBox1.Text = Fromdt.Text;
                    textBox2.Text = Todt.Text;
                    Genclass.prtfrmdate = Convert.ToDateTime(textBox1.Text);
                    Genclass.prttodate = Convert.ToDateTime(textBox2.Text);
                    Crviewer crv = new Crviewer();
                    crv.Show();
                }
            }
            else
            {
                Genclass.Dtype = 2;

                conn.Close();
                conn.Open();
                int i = HFGP.SelectedCells[0].RowIndex;

                qur.CommandText = "Exec SP_stkledger '" + Fromdt.Text + "','" + Todt.Text + "'";
                qur.ExecuteNonQuery();

                textBox1.Text = Fromdt.Text;
                textBox2.Text = Todt.Text;
                Genclass.prtfrmdate = Convert.ToDateTime(textBox1.Text);
                Genclass.prttodate = Convert.ToDateTime(textBox2.Text);
                Crviewer crv = new Crviewer();
                crv.Show();
            


        }
            conn.Close();
        }
          
           
        





        private void chkact_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void HFGP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtcode_TextChanged(object sender, EventArgs e)
        {
            loaditem();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }


}
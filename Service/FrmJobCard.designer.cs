﻿namespace Service
{
    partial class FrmJobCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grCustomer = new System.Windows.Forms.GroupBox();
            this.txtmech = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.dtpmod = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.dtpsod = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.dtpcr = new System.Windows.Forms.DateTimePicker();
            this.txtcolor = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtframe = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtengine = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtlabour = new System.Windows.Forms.TextBox();
            this.txtSpares = new System.Windows.Forms.TextBox();
            this.txtOil = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbDTime = new System.Windows.Forms.ComboBox();
            this.txtJobCardNo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpJobDate = new System.Windows.Forms.DateTimePicker();
            this.btnNewCustomer = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbFuelLevel = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpdeleiveryDate = new System.Windows.Forms.DateTimePicker();
            this.cmbTypeOfJob = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtKm = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.txtRegNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grCustComplents = new System.Windows.Forms.GroupBox();
            this.DataGridDescription = new System.Windows.Forms.DataGridView();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtCustComp = new System.Windows.Forms.TextBox();
            this.grPhysicalCondition = new System.Windows.Forms.GroupBox();
            this.txtRightSide = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtLeftSide = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.grPC = new System.Windows.Forms.GroupBox();
            this.dataGridAccessories = new System.Windows.Forms.DataGridView();
            this.btnPOk = new System.Windows.Forms.Button();
            this.txtAcc = new System.Windows.Forms.TextBox();
            this.grServiceAdvice = new System.Windows.Forms.GroupBox();
            this.dataGridServiceAdvice = new System.Windows.Forms.DataGridView();
            this.button3 = new System.Windows.Forms.Button();
            this.txtServiceAdvise = new System.Windows.Forms.TextBox();
            this.panAdd = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.dataGridJobCard = new System.Windows.Forms.DataGridView();
            this.dtpJobCardDate = new System.Windows.Forms.DateTimePicker();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.grCustomer.SuspendLayout();
            this.grCustComplents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDescription)).BeginInit();
            this.grPhysicalCondition.SuspendLayout();
            this.grPC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAccessories)).BeginInit();
            this.grServiceAdvice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridServiceAdvice)).BeginInit();
            this.panAdd.SuspendLayout();
            this.grBack.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJobCard)).BeginInit();
            this.SuspendLayout();
            // 
            // grCustomer
            // 
            this.grCustomer.Controls.Add(this.txtmech);
            this.grCustomer.Controls.Add(this.label22);
            this.grCustomer.Controls.Add(this.label21);
            this.grCustomer.Controls.Add(this.dtpmod);
            this.grCustomer.Controls.Add(this.label20);
            this.grCustomer.Controls.Add(this.dtpsod);
            this.grCustomer.Controls.Add(this.label19);
            this.grCustomer.Controls.Add(this.dtpcr);
            this.grCustomer.Controls.Add(this.txtcolor);
            this.grCustomer.Controls.Add(this.label18);
            this.grCustomer.Controls.Add(this.txtframe);
            this.grCustomer.Controls.Add(this.label17);
            this.grCustomer.Controls.Add(this.txtengine);
            this.grCustomer.Controls.Add(this.label16);
            this.grCustomer.Controls.Add(this.txtlabour);
            this.grCustomer.Controls.Add(this.txtSpares);
            this.grCustomer.Controls.Add(this.txtOil);
            this.grCustomer.Controls.Add(this.label15);
            this.grCustomer.Controls.Add(this.label14);
            this.grCustomer.Controls.Add(this.label2);
            this.grCustomer.Controls.Add(this.cmbDTime);
            this.grCustomer.Controls.Add(this.txtJobCardNo);
            this.grCustomer.Controls.Add(this.label13);
            this.grCustomer.Controls.Add(this.label12);
            this.grCustomer.Controls.Add(this.dtpJobDate);
            this.grCustomer.Controls.Add(this.btnNewCustomer);
            this.grCustomer.Controls.Add(this.label9);
            this.grCustomer.Controls.Add(this.label8);
            this.grCustomer.Controls.Add(this.cmbFuelLevel);
            this.grCustomer.Controls.Add(this.label7);
            this.grCustomer.Controls.Add(this.dtpdeleiveryDate);
            this.grCustomer.Controls.Add(this.cmbTypeOfJob);
            this.grCustomer.Controls.Add(this.label6);
            this.grCustomer.Controls.Add(this.txtKm);
            this.grCustomer.Controls.Add(this.label5);
            this.grCustomer.Controls.Add(this.txtMobile);
            this.grCustomer.Controls.Add(this.label4);
            this.grCustomer.Controls.Add(this.txtCustomer);
            this.grCustomer.Controls.Add(this.label3);
            this.grCustomer.Controls.Add(this.txtModel);
            this.grCustomer.Controls.Add(this.txtRegNo);
            this.grCustomer.Controls.Add(this.label1);
            this.grCustomer.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grCustomer.Location = new System.Drawing.Point(6, 11);
            this.grCustomer.Name = "grCustomer";
            this.grCustomer.Size = new System.Drawing.Size(597, 376);
            this.grCustomer.TabIndex = 0;
            this.grCustomer.TabStop = false;
            this.grCustomer.Text = "Customer Details";
            // 
            // txtmech
            // 
            this.txtmech.Location = new System.Drawing.Point(131, 321);
            this.txtmech.Name = "txtmech";
            this.txtmech.Size = new System.Drawing.Size(202, 26);
            this.txtmech.TabIndex = 41;
            this.txtmech.TextChanged += new System.EventHandler(this.txtmech_TextChanged);
            this.txtmech.Leave += new System.EventHandler(this.txtmech_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(18, 327);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(107, 18);
            this.label22.TabIndex = 42;
            this.label22.Text = "Mechanic Name";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(371, 321);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(99, 18);
            this.label21.TabIndex = 40;
            this.label21.Text = "Modified Date ";
            // 
            // dtpmod
            // 
            this.dtpmod.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpmod.Location = new System.Drawing.Point(473, 315);
            this.dtpmod.Name = "dtpmod";
            this.dtpmod.Size = new System.Drawing.Size(104, 26);
            this.dtpmod.TabIndex = 39;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(426, 350);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 18);
            this.label20.TabIndex = 38;
            this.label20.Text = "SOD ";
            // 
            // dtpsod
            // 
            this.dtpsod.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpsod.Location = new System.Drawing.Point(473, 344);
            this.dtpsod.Name = "dtpsod";
            this.dtpsod.Size = new System.Drawing.Size(104, 26);
            this.dtpsod.TabIndex = 37;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(371, 292);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(92, 18);
            this.label19.TabIndex = 36;
            this.label19.Text = "Created Date ";
            // 
            // dtpcr
            // 
            this.dtpcr.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpcr.Location = new System.Drawing.Point(473, 286);
            this.dtpcr.Name = "dtpcr";
            this.dtpcr.Size = new System.Drawing.Size(104, 26);
            this.dtpcr.TabIndex = 35;
            // 
            // txtcolor
            // 
            this.txtcolor.Location = new System.Drawing.Point(473, 257);
            this.txtcolor.Name = "txtcolor";
            this.txtcolor.Size = new System.Drawing.Size(100, 26);
            this.txtcolor.TabIndex = 34;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(417, 256);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 18);
            this.label18.TabIndex = 33;
            this.label18.Text = "Colour";
            // 
            // txtframe
            // 
            this.txtframe.Location = new System.Drawing.Point(131, 289);
            this.txtframe.Name = "txtframe";
            this.txtframe.Size = new System.Drawing.Size(202, 26);
            this.txtframe.TabIndex = 32;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(54, 289);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 18);
            this.label17.TabIndex = 31;
            this.label17.Text = "Frame No";
            // 
            // txtengine
            // 
            this.txtengine.Location = new System.Drawing.Point(131, 254);
            this.txtengine.Name = "txtengine";
            this.txtengine.Size = new System.Drawing.Size(202, 26);
            this.txtengine.TabIndex = 30;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(54, 257);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 18);
            this.label16.TabIndex = 29;
            this.label16.Text = "Engine No";
            // 
            // txtlabour
            // 
            this.txtlabour.Location = new System.Drawing.Point(473, 219);
            this.txtlabour.Name = "txtlabour";
            this.txtlabour.Size = new System.Drawing.Size(100, 26);
            this.txtlabour.TabIndex = 28;
            // 
            // txtSpares
            // 
            this.txtSpares.Location = new System.Drawing.Point(473, 187);
            this.txtSpares.Name = "txtSpares";
            this.txtSpares.Size = new System.Drawing.Size(100, 26);
            this.txtSpares.TabIndex = 27;
            // 
            // txtOil
            // 
            this.txtOil.Location = new System.Drawing.Point(473, 154);
            this.txtOil.Name = "txtOil";
            this.txtOil.Size = new System.Drawing.Size(100, 26);
            this.txtOil.TabIndex = 26;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(418, 187);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 18);
            this.label15.TabIndex = 25;
            this.label15.Text = "Spares";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(417, 218);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 18);
            this.label14.TabIndex = 24;
            this.label14.Text = "Labour";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(441, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 18);
            this.label2.TabIndex = 23;
            this.label2.Text = "Oil";
            // 
            // cmbDTime
            // 
            this.cmbDTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDTime.FormattingEnabled = true;
            this.cmbDTime.Items.AddRange(new object[] {
            "10.00 AM",
            "10.30 AM",
            "11.00 AM",
            "11.30 AM",
            "12.00 PM",
            "12.30 PM",
            "01.00 PM",
            "01.30 PM",
            "02.00 PM",
            "02.30 PM",
            "03.00 PM",
            "03.30 PM",
            "04.00 PM",
            "04.30 PM",
            "05.00 PM",
            "05.30 PM",
            "06.00 PM",
            "06.30 PM",
            "07.00 PM",
            "07.30 PM",
            "08.00 PM",
            "08.30 PM",
            "09.00 PM"});
            this.cmbDTime.Location = new System.Drawing.Point(417, 122);
            this.cmbDTime.Name = "cmbDTime";
            this.cmbDTime.Size = new System.Drawing.Size(156, 26);
            this.cmbDTime.TabIndex = 22;
            // 
            // txtJobCardNo
            // 
            this.txtJobCardNo.Location = new System.Drawing.Point(380, 22);
            this.txtJobCardNo.Name = "txtJobCardNo";
            this.txtJobCardNo.Size = new System.Drawing.Size(126, 26);
            this.txtJobCardNo.TabIndex = 20;
            this.txtJobCardNo.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(296, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 18);
            this.label13.TabIndex = 21;
            this.label13.Text = "JobCard No";
            this.label13.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(85, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 18);
            this.label12.TabIndex = 19;
            this.label12.Text = "Date ";
            // 
            // dtpJobDate
            // 
            this.dtpJobDate.Enabled = false;
            this.dtpJobDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpJobDate.Location = new System.Drawing.Point(131, 22);
            this.dtpJobDate.Name = "dtpJobDate";
            this.dtpJobDate.Size = new System.Drawing.Size(149, 26);
            this.dtpJobDate.TabIndex = 18;
            // 
            // btnNewCustomer
            // 
            this.btnNewCustomer.BackColor = System.Drawing.Color.White;
            this.btnNewCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNewCustomer.Image = global::Service.Properties.Resources.if_add_46776;
            this.btnNewCustomer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNewCustomer.Location = new System.Drawing.Point(314, 60);
            this.btnNewCustomer.Name = "btnNewCustomer";
            this.btnNewCustomer.Size = new System.Drawing.Size(69, 27);
            this.btnNewCustomer.TabIndex = 3;
            this.btnNewCustomer.Text = "New";
            this.btnNewCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNewCustomer.UseVisualStyleBackColor = false;
            this.btnNewCustomer.Click += new System.EventHandler(this.button1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(418, 101);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 18);
            this.label9.TabIndex = 17;
            this.label9.Text = "Delivery Time";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(418, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 18);
            this.label8.TabIndex = 16;
            this.label8.Text = "Delivery Date ";
            // 
            // cmbFuelLevel
            // 
            this.cmbFuelLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFuelLevel.FormattingEnabled = true;
            this.cmbFuelLevel.Items.AddRange(new object[] {
            "0 Level",
            ".25 Level",
            ".50 Level",
            ".75 Level",
            "1 Level"});
            this.cmbFuelLevel.Location = new System.Drawing.Point(131, 222);
            this.cmbFuelLevel.Name = "cmbFuelLevel";
            this.cmbFuelLevel.Size = new System.Drawing.Size(215, 26);
            this.cmbFuelLevel.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(54, 226);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 18);
            this.label7.TabIndex = 14;
            this.label7.Text = "Fuel Level";
            // 
            // dtpdeleiveryDate
            // 
            this.dtpdeleiveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpdeleiveryDate.Location = new System.Drawing.Point(417, 73);
            this.dtpdeleiveryDate.Name = "dtpdeleiveryDate";
            this.dtpdeleiveryDate.Size = new System.Drawing.Size(156, 26);
            this.dtpdeleiveryDate.TabIndex = 4;
            // 
            // cmbTypeOfJob
            // 
            this.cmbTypeOfJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeOfJob.FormattingEnabled = true;
            this.cmbTypeOfJob.Location = new System.Drawing.Point(131, 190);
            this.cmbTypeOfJob.Name = "cmbTypeOfJob";
            this.cmbTypeOfJob.Size = new System.Drawing.Size(280, 26);
            this.cmbTypeOfJob.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(48, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Type of Job";
            // 
            // txtKm
            // 
            this.txtKm.Location = new System.Drawing.Point(131, 157);
            this.txtKm.Name = "txtKm";
            this.txtKm.Size = new System.Drawing.Size(280, 26);
            this.txtKm.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(55, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Kilometer";
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(131, 125);
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(280, 26);
            this.txtMobile.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(73, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Mobile";
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new System.Drawing.Point(131, 93);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(187, 26);
            this.txtCustomer.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Customer/Model";
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(319, 93);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(92, 26);
            this.txtModel.TabIndex = 3;
            // 
            // txtRegNo
            // 
            this.txtRegNo.Location = new System.Drawing.Point(131, 61);
            this.txtRegNo.Name = "txtRegNo";
            this.txtRegNo.Size = new System.Drawing.Size(179, 26);
            this.txtRegNo.TabIndex = 0;
            this.txtRegNo.TextChanged += new System.EventHandler(this.txtRegNo_TextChanged);
            this.txtRegNo.Leave += new System.EventHandler(this.txtRegNo_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Reg No";
            // 
            // grCustComplents
            // 
            this.grCustComplents.Controls.Add(this.DataGridDescription);
            this.grCustComplents.Controls.Add(this.btnOk);
            this.grCustComplents.Controls.Add(this.txtCustComp);
            this.grCustComplents.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grCustComplents.Location = new System.Drawing.Point(614, 12);
            this.grCustComplents.Name = "grCustComplents";
            this.grCustComplents.Size = new System.Drawing.Size(442, 276);
            this.grCustComplents.TabIndex = 1;
            this.grCustComplents.TabStop = false;
            this.grCustComplents.Text = "Customer Complients";
            // 
            // DataGridDescription
            // 
            this.DataGridDescription.BackgroundColor = System.Drawing.Color.White;
            this.DataGridDescription.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridDescription.Location = new System.Drawing.Point(7, 51);
            this.DataGridDescription.Margin = new System.Windows.Forms.Padding(4);
            this.DataGridDescription.Name = "DataGridDescription";
            this.DataGridDescription.Size = new System.Drawing.Size(425, 219);
            this.DataGridDescription.TabIndex = 217;
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.White;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOk.Image = global::Service.Properties.Resources.ok1;
            this.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOk.Location = new System.Drawing.Point(392, 24);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(40, 26);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "Ok";
            this.btnOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtCustComp
            // 
            this.txtCustComp.Location = new System.Drawing.Point(6, 24);
            this.txtCustComp.Name = "txtCustComp";
            this.txtCustComp.Size = new System.Drawing.Size(385, 26);
            this.txtCustComp.TabIndex = 3;
            this.txtCustComp.TextChanged += new System.EventHandler(this.txtCustComp_TextChanged);
            // 
            // grPhysicalCondition
            // 
            this.grPhysicalCondition.Controls.Add(this.txtRightSide);
            this.grPhysicalCondition.Controls.Add(this.label11);
            this.grPhysicalCondition.Controls.Add(this.txtLeftSide);
            this.grPhysicalCondition.Controls.Add(this.label10);
            this.grPhysicalCondition.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grPhysicalCondition.Location = new System.Drawing.Point(4, 393);
            this.grPhysicalCondition.Name = "grPhysicalCondition";
            this.grPhysicalCondition.Size = new System.Drawing.Size(324, 148);
            this.grPhysicalCondition.TabIndex = 2;
            this.grPhysicalCondition.TabStop = false;
            this.grPhysicalCondition.Text = "Physical Condition";
            // 
            // txtRightSide
            // 
            this.txtRightSide.Location = new System.Drawing.Point(6, 100);
            this.txtRightSide.Name = "txtRightSide";
            this.txtRightSide.Size = new System.Drawing.Size(307, 40);
            this.txtRightSide.TabIndex = 6;
            this.txtRightSide.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 18);
            this.label11.TabIndex = 20;
            this.label11.Text = "Right Side";
            // 
            // txtLeftSide
            // 
            this.txtLeftSide.Location = new System.Drawing.Point(6, 39);
            this.txtLeftSide.Name = "txtLeftSide";
            this.txtLeftSide.Size = new System.Drawing.Size(307, 37);
            this.txtLeftSide.TabIndex = 5;
            this.txtLeftSide.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 18);
            this.label10.TabIndex = 18;
            this.label10.Text = "Left Side";
            // 
            // grPC
            // 
            this.grPC.Controls.Add(this.dataGridAccessories);
            this.grPC.Controls.Add(this.btnPOk);
            this.grPC.Controls.Add(this.txtAcc);
            this.grPC.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grPC.Location = new System.Drawing.Point(334, 382);
            this.grPC.Name = "grPC";
            this.grPC.Size = new System.Drawing.Size(270, 161);
            this.grPC.TabIndex = 22;
            this.grPC.TabStop = false;
            this.grPC.Text = "Accessories";
            // 
            // dataGridAccessories
            // 
            this.dataGridAccessories.BackgroundColor = System.Drawing.Color.White;
            this.dataGridAccessories.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAccessories.Location = new System.Drawing.Point(7, 56);
            this.dataGridAccessories.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridAccessories.Name = "dataGridAccessories";
            this.dataGridAccessories.Size = new System.Drawing.Size(253, 101);
            this.dataGridAccessories.TabIndex = 216;
            // 
            // btnPOk
            // 
            this.btnPOk.BackColor = System.Drawing.Color.White;
            this.btnPOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPOk.Image = global::Service.Properties.Resources.ok1;
            this.btnPOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPOk.Location = new System.Drawing.Point(219, 25);
            this.btnPOk.Name = "btnPOk";
            this.btnPOk.Size = new System.Drawing.Size(44, 26);
            this.btnPOk.TabIndex = 8;
            this.btnPOk.Text = "Ok";
            this.btnPOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPOk.UseVisualStyleBackColor = false;
            this.btnPOk.Click += new System.EventHandler(this.btnPOk_Click);
            // 
            // txtAcc
            // 
            this.txtAcc.Location = new System.Drawing.Point(5, 25);
            this.txtAcc.Name = "txtAcc";
            this.txtAcc.Size = new System.Drawing.Size(213, 26);
            this.txtAcc.TabIndex = 7;
            this.txtAcc.TextChanged += new System.EventHandler(this.txtAcc_TextChanged);
            // 
            // grServiceAdvice
            // 
            this.grServiceAdvice.Controls.Add(this.dataGridServiceAdvice);
            this.grServiceAdvice.Controls.Add(this.button3);
            this.grServiceAdvice.Controls.Add(this.txtServiceAdvise);
            this.grServiceAdvice.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grServiceAdvice.Location = new System.Drawing.Point(614, 293);
            this.grServiceAdvice.Name = "grServiceAdvice";
            this.grServiceAdvice.Size = new System.Drawing.Size(442, 248);
            this.grServiceAdvice.TabIndex = 23;
            this.grServiceAdvice.TabStop = false;
            this.grServiceAdvice.Text = "Service Advice";
            // 
            // dataGridServiceAdvice
            // 
            this.dataGridServiceAdvice.BackgroundColor = System.Drawing.Color.White;
            this.dataGridServiceAdvice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridServiceAdvice.Location = new System.Drawing.Point(7, 46);
            this.dataGridServiceAdvice.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridServiceAdvice.Name = "dataGridServiceAdvice";
            this.dataGridServiceAdvice.Size = new System.Drawing.Size(425, 196);
            this.dataGridServiceAdvice.TabIndex = 218;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Image = global::Service.Properties.Resources.ok1;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(396, 18);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(40, 26);
            this.button3.TabIndex = 10;
            this.button3.Text = "Ok";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtServiceAdvise
            // 
            this.txtServiceAdvise.Location = new System.Drawing.Point(5, 18);
            this.txtServiceAdvise.Name = "txtServiceAdvise";
            this.txtServiceAdvise.Size = new System.Drawing.Size(391, 26);
            this.txtServiceAdvise.TabIndex = 9;
            this.txtServiceAdvise.TextChanged += new System.EventHandler(this.txtServiceAdvise_TextChanged);
            // 
            // panAdd
            // 
            this.panAdd.BackColor = System.Drawing.Color.White;
            this.panAdd.Controls.Add(this.btnPrint);
            this.panAdd.Controls.Add(this.btnEdit);
            this.panAdd.Controls.Add(this.btnAdd);
            this.panAdd.Controls.Add(this.btnBack);
            this.panAdd.Controls.Add(this.btnSave);
            this.panAdd.Controls.Add(this.btnExit);
            this.panAdd.Controls.Add(this.btnDelete);
            this.panAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panAdd.Location = new System.Drawing.Point(7, 559);
            this.panAdd.Name = "panAdd";
            this.panAdd.Size = new System.Drawing.Size(1062, 37);
            this.panAdd.TabIndex = 24;
            this.panAdd.Paint += new System.Windows.Forms.PaintEventHandler(this.panAdd_Paint);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPrint.Image = global::Service.Properties.Resources.Preview;
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(4, 3);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(77, 31);
            this.btnPrint.TabIndex = 6;
            this.btnPrint.Text = "Print";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEdit.Image = global::Service.Properties.Resources.if_edit_notes_46798;
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(824, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(74, 31);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.Image = global::Service.Properties.Resources.if_add_46776;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(749, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(74, 31);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(981, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(77, 31);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "Back";
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Image = global::Service.Properties.Resources.if_save_46830__1_;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(903, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(77, 31);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(981, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(77, 31);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelete.Image = global::Service.Properties.Resources.if_trash_46839___Copy;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(899, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(81, 31);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.grCustComplents);
            this.grBack.Controls.Add(this.grServiceAdvice);
            this.grBack.Controls.Add(this.grPhysicalCondition);
            this.grBack.Controls.Add(this.grPC);
            this.grBack.Controls.Add(this.grCustomer);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(10, 4);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(1059, 549);
            this.grBack.TabIndex = 25;
            this.grBack.TabStop = false;
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.dataGridJobCard);
            this.grFront.Controls.Add(this.dtpJobCardDate);
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(10, 4);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(1067, 549);
            this.grFront.TabIndex = 26;
            this.grFront.TabStop = false;
            this.grFront.Enter += new System.EventHandler(this.grFront_Enter);
            // 
            // dataGridJobCard
            // 
            this.dataGridJobCard.AllowUserToAddRows = false;
            this.dataGridJobCard.BackgroundColor = System.Drawing.Color.White;
            this.dataGridJobCard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridJobCard.Location = new System.Drawing.Point(4, 77);
            this.dataGridJobCard.Name = "dataGridJobCard";
            this.dataGridJobCard.ReadOnly = true;
            this.dataGridJobCard.RowHeadersVisible = false;
            this.dataGridJobCard.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridJobCard.Size = new System.Drawing.Size(1051, 472);
            this.dataGridJobCard.TabIndex = 3;
            // 
            // dtpJobCardDate
            // 
            this.dtpJobCardDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpJobCardDate.Location = new System.Drawing.Point(931, 16);
            this.dtpJobCardDate.Name = "dtpJobCardDate";
            this.dtpJobCardDate.Size = new System.Drawing.Size(122, 26);
            this.dtpJobCardDate.TabIndex = 2;
            this.dtpJobCardDate.ValueChanged += new System.EventHandler(this.dtpJobCardDate_ValueChanged);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(4, 45);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(1052, 26);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // FrmJobCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1076, 604);
            this.Controls.Add(this.panAdd);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.grFront);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmJobCard";
            this.Text = "Job Card";
            this.Load += new System.EventHandler(this.FrmJobCard_Load);
            this.grCustomer.ResumeLayout(false);
            this.grCustomer.PerformLayout();
            this.grCustComplents.ResumeLayout(false);
            this.grCustComplents.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDescription)).EndInit();
            this.grPhysicalCondition.ResumeLayout(false);
            this.grPhysicalCondition.PerformLayout();
            this.grPC.ResumeLayout(false);
            this.grPC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAccessories)).EndInit();
            this.grServiceAdvice.ResumeLayout(false);
            this.grServiceAdvice.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridServiceAdvice)).EndInit();
            this.panAdd.ResumeLayout(false);
            this.grBack.ResumeLayout(false);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJobCard)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grCustomer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbFuelLevel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpdeleiveryDate;
        private System.Windows.Forms.ComboBox cmbTypeOfJob;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtKm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMobile;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.TextBox txtRegNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grCustComplents;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox txtCustComp;
        private System.Windows.Forms.Button btnNewCustomer;
        private System.Windows.Forms.GroupBox grPhysicalCondition;
        private System.Windows.Forms.RichTextBox txtRightSide;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox txtLeftSide;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox grPC;
        private System.Windows.Forms.Button btnPOk;
        private System.Windows.Forms.TextBox txtAcc;
        private System.Windows.Forms.GroupBox grServiceAdvice;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtServiceAdvise;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtpJobDate;
        private System.Windows.Forms.TextBox txtJobCardNo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.ComboBox cmbDTime;
        private System.Windows.Forms.TextBox txtlabour;
        private System.Windows.Forms.TextBox txtSpares;
        private System.Windows.Forms.TextBox txtOil;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpJobCardDate;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.TextBox txtcolor;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtframe;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtengine;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtmech;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker dtpmod;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DateTimePicker dtpsod;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DateTimePicker dtpcr;
        private System.Windows.Forms.DataGridView dataGridAccessories;
        private System.Windows.Forms.DataGridView DataGridDescription;
        private System.Windows.Forms.DataGridView dataGridServiceAdvice;
        private System.Windows.Forms.DataGridView dataGridJobCard;
    }
}
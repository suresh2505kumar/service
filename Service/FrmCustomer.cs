﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Service
{
    public partial class FrmCustomer : Form
    {
        public FrmCustomer()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        int AutoUid;
        int LastId;
        private void txtMobile_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar);
        }

        private void FrmCustomer_Load(object sender, EventArgs e)
        {
            GetCustomer(0);
            LoadButton(1);
        }

        private void LoadButton(int Id)
        {
            try
            {
                if (Id == 1) //Load
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                    btnSave.Text = "Save";
                }
                else if (Id == 2)//Add
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Save";
                }
                else if (Id == 3)//Edit
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            LoadButton(2);
            LoadBikegrid();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            LoadButton(1);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool Active;
                if (chckActive.Checked == true)
                {
                    Active = true;
                }
                else
                {
                    Active = false;
                }
                int Uid = 0;
                if (btnSave.Text == "Save")
                {
                    SqlParameter[] para = {
                        new SqlParameter("@Name",txtName.Text),
                        new SqlParameter("@Code",txtCode.Text),
                        new SqlParameter("@Address1",txtDoorNo.Text),
                        new SqlParameter("@Address2",txtStreet.Text),
                        new SqlParameter("@City",txtCity.Text),
                        new SqlParameter("@Mobile",txtMobile.Text),
                        new SqlParameter("@Mobile1",txtMobile1.Text),
                        new SqlParameter("@Phone",txtPhone.Text),
                        new SqlParameter("@Dob",Convert.ToDateTime(dtpDob.Text)),
                        new SqlParameter("@EmailId",txtEmail.Text),
                        new SqlParameter("@CompanyId",1),
                        new SqlParameter("@Active",Active),
                        new SqlParameter("@ReturnUid",DbType.Int32)
                    };
                    para[12].Direction = ParameterDirection.Output;
                    Uid = db.ExecuteQuery(CommandType.StoredProcedure, "SP_CustomerM", para, 12);

                    for (int i = 0; i < DataGridBike.Rows.Count; i++)
                    {
                        SqlParameter[] paraDet = {
                            new SqlParameter("@CustUid",Uid),
                            new SqlParameter("@RegNo",DataGridBike.Rows[i].Cells[1].Value.ToString()),
                            new SqlParameter("@BrandUid",DataGridBike.Rows[i].Cells[0].Value.ToString()),
                            new SqlParameter("@Active",true),
                            new SqlParameter("@Uid","0")
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "SP_CustBike", paraDet);
                    }
                    SqlParameter[] paraUp = {
                        new SqlParameter("@Uid",AutoUid),
                        new SqlParameter("@F1",LastId)
                    };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_UpdateGeneralMCustCode", paraUp);
                    MessageBox.Show("Record has been saved Successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    SqlParameter[] para = {
                        new SqlParameter("@Name",txtName.Text),
                        new SqlParameter("@Code",txtCode.Text),
                        new SqlParameter("@Address1",txtDoorNo.Text),
                        new SqlParameter("@Address2",txtStreet.Text),
                        new SqlParameter("@City",txtCity.Text),
                        new SqlParameter("@Mobile",txtMobile.Text),
                        new SqlParameter("@Mobile1",txtMobile1.Text),
                        new SqlParameter("@Phone",txtPhone.Text),
                        new SqlParameter("@Dob",Convert.ToDateTime(dtpDob.Text)),
                        new SqlParameter("@EmailId",txtEmail.Text),
                        new SqlParameter("@CompanyId",1),
                        new SqlParameter("@Active",Active),
                        new SqlParameter("@ReturnUid",DbType.Int32),
                        new SqlParameter("@Uid",txtCode.Tag)
                    };
                    para[12].Direction = ParameterDirection.Output;
                    Uid = db.ExecuteQuery(CommandType.StoredProcedure, "SP_CustomerM", para);
                    for (int i = 0; i < DataGridBike.Rows.Count; i++)
                    {
                        SqlParameter[] paraDet = {
                            new SqlParameter("@CustUid",Uid),
                            new SqlParameter("@RegNo",DataGridBike.Rows[i].Cells[1].Value.ToString()),
                            new SqlParameter("@BrandUid",DataGridBike.Rows[i].Cells[0].Value.ToString()),
                            new SqlParameter("@Active",true),
                            new SqlParameter("@Uid",DataGridBike.Rows[i].Cells[3].Value.ToString())
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "SP_CustBike", paraDet);
                    }
                    MessageBox.Show("Record has been updated Successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                ClearControl();
                GetCustomer(0);
                LoadButton(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void chckActive_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void LoadBikegrid()
        {
            try
            {
                DataGridBike.DataSource = null;
                DataGridBike.AutoGenerateColumns = false;
                DataGridBike.ColumnCount = 4;
                DataGridBike.Columns[0].Name = "Uid";
                DataGridBike.Columns[0].HeaderText = "Uid";
                DataGridBike.Columns[0].Visible = false;
                DataGridBike.Columns[1].Name = "RegNo";
                DataGridBike.Columns[1].HeaderText = "RegNo";
                DataGridBike.Columns[1].Width = 175;
                DataGridBike.Columns[2].Name = "Model";
                DataGridBike.Columns[2].HeaderText = "Model";
                DataGridBike.Columns[3].Name = "BUid";
                DataGridBike.Columns[3].HeaderText = "BUid";
                DataGridBike.Columns[3].Visible = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ClearControl()
        {
            txtCode.Text = string.Empty;
            txtName.Text = string.Empty;
            txtDoorNo.Text = string.Empty;
            txtStreet.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtMobile.Text = string.Empty;
            txtMobile1.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtRegNo.Text = string.Empty;
            txtModel.Text = string.Empty;
            txtEmail.Text = string.Empty;
        }

        private void txtName_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtName.Text != string.Empty)
                {
                    SqlParameter[] para = { new SqlParameter("@GeneralName", txtName.Text), new SqlParameter("@TypeM_Uid", 4) };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralMSpecific", para);
                    int Id = Convert.ToInt32(dt.Rows[0]["f1"].ToString()) + 1;
                    string s = txtName.Text;
                    string code = s.Substring(0, 1) + Id.ToString("D5");
                    txtCode.Text = code;
                    AutoUid = Convert.ToInt32(dt.Rows[0]["Uid"].ToString());
                    LastId = Id;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtModel_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                GeModeltData();
                grModel.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void GeModeltData()
        {
            try
            {
                SqlParameter[] para = {
                    new SqlParameter("@TypeM_Uid",1)
                };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralM", para);
                DataGridModel.DataSource = null;
                DataGridModel.AutoGenerateColumns = false;
                DataGridModel.ColumnCount = 2;
                DataGridModel.Columns[0].Name = "Uid";
                DataGridModel.Columns[0].HeaderText = "Uid";
                DataGridModel.Columns[0].DataPropertyName = "Uid";
                DataGridModel.Columns[0].Visible = false;
                DataGridModel.Columns[1].Name = "Name";
                DataGridModel.Columns[1].HeaderText = "Name";
                DataGridModel.Columns[1].DataPropertyName = "GeneralName";
                DataGridModel.Columns[1].Width = 260;
                DataGridModel.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void grModel_Enter(object sender, EventArgs e)
        {

        }

        private void btnGridClose_Click(object sender, EventArgs e)
        {
            grModel.Visible = false;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                bool entryFound = false;
                foreach (DataGridViewRow row in DataGridBike.Rows)
                {
                    object val2 = row.Cells[1].Value;
                    object val3 = row.Cells[2].Value;
                    object val4 = row.Cells[3].Value;
                    if (val2 != null && val2.ToString() == txtRegNo.Text)
                    {
                        MessageBox.Show("Entry already exist", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        entryFound = true;
                        break;
                    }
                }

                if (!entryFound)
                {
                    int Index = DataGridModel.SelectedCells[0].RowIndex;
                    int rowId = DataGridBike.Rows.Add();
                    DataGridViewRow row = DataGridBike.Rows[rowId];
                    row.Cells[0].Value = DataGridModel.Rows[Index].Cells[0].Value.ToString();
                    row.Cells[1].Value = txtRegNo.Text;
                    row.Cells[2].Value = DataGridModel.Rows[Index].Cells[1].Value.ToString();
                    row.Cells[3].Value = 0;
                }
                grModel.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void GetCustomer(int Uid)
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@Uid", Uid) };
                if(Uid == 0)
                {
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetCustM", para);
                    bs.DataSource = dt;
                    DataGridCustomer.DataSource = null;
                    DataGridCustomer.AutoGenerateColumns = false;
                    DataGridCustomer.ColumnCount = 6;
                    DataGridCustomer.Columns[0].Name = "Uid";
                    DataGridCustomer.Columns[0].HeaderText = "Uid";
                    DataGridCustomer.Columns[0].DataPropertyName = "Uid";
                    DataGridCustomer.Columns[0].Visible = false;
                    DataGridCustomer.Columns[1].Name = "RegNo";
                    DataGridCustomer.Columns[1].HeaderText = "RegNo";
                    DataGridCustomer.Columns[1].DataPropertyName = "RegNo";

                    DataGridCustomer.Columns[2].Name = "Model";
                    DataGridCustomer.Columns[2].HeaderText = "Model";
                    DataGridCustomer.Columns[2].DataPropertyName = "GeneralName";

                    DataGridCustomer.Columns[3].Name = "Name";
                    DataGridCustomer.Columns[3].HeaderText = "Name";
                    DataGridCustomer.Columns[3].DataPropertyName = "Name";
                    DataGridCustomer.Columns[3].Width = 140;
                    DataGridCustomer.Columns[4].Name = "Mobile";
                    DataGridCustomer.Columns[4].HeaderText = "Mobile";
                    DataGridCustomer.Columns[4].DataPropertyName = "Mobile";

                    DataGridCustomer.Columns[5].Name = "Email";
                    DataGridCustomer.Columns[5].HeaderText = "Email";
                    DataGridCustomer.Columns[5].DataPropertyName = "EmailId";
                    DataGridCustomer.Columns[5].Width = 240;
                    DataGridCustomer.DataSource = bs;
                }
                else
                {
                    DataSet ds = db.GetMultipleData(CommandType.StoredProcedure, "SP_GetCustM", para);
                    DataTable dt = ds.Tables[0];
                    DataTable dtDet = ds.Tables[1];
                    txtCode.Text = dt.Rows[0]["Code"].ToString();
                    txtName.Text = dt.Rows[0]["Name"].ToString();
                    txtDoorNo.Text = dt.Rows[0]["Address1"].ToString();
                    txtStreet.Text = dt.Rows[0]["Address2"].ToString();
                    txtCity.Text = dt.Rows[0]["City"].ToString();
                    txtMobile.Text = dt.Rows[0]["Mobile"].ToString();
                    txtMobile1.Text = dt.Rows[0]["Mobile1"].ToString();
                    txtPhone.Text = dt.Rows[0]["Phone"].ToString();
                    txtEmail.Text = dt.Rows[0]["EmailId"].ToString();
                    dtpDob.Text = Convert.ToDateTime(dt.Rows[0]["Dob"].ToString()).ToString("dd-MMM-yyyy");
                    txtCode.Tag = dt.Rows[0]["Uid"].ToString();
                    for (int i = 0; i < dtDet.Rows.Count; i++)
                    {
                        LoadBikegrid();
                        int rowId = DataGridBike.Rows.Add();
                        DataGridViewRow row = DataGridBike.Rows[rowId];
                        row.Cells[0].Value = dtDet.Rows[i]["Brandid"].ToString();
                        row.Cells[1].Value = dtDet.Rows[i]["RegNo"].ToString();
                        row.Cells[2].Value = dtDet.Rows[i]["Model"].ToString();
                        row.Cells[3].Value = dtDet.Rows[i]["Uid"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridCustomer.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGridCustomer.Rows[Index].Cells[0].Value.ToString());
                GetCustomer(Uid);
                LoadButton(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtRegNo_KeyDown(object sender, KeyEventArgs e)
        {
            if ((sender as TextBox).SelectionStart == 0)
            {
                e.Handled = (e.KeyCode == Keys.Space);
            }
            else
            {
                e.Handled = false;
            }
                
        }

        private void txtRegNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ') e.Handled = true;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("RegNo Like '%{0}%' or GeneralName Like '%{1}%' or  Name Like '%{2}%'  or  mobile Like '%{3}%' or  emailid Like '%{4}%'", txtSearch.Text, txtSearch.Text, txtSearch.Text, txtSearch.Text, txtSearch.Text);
        }
    }
}

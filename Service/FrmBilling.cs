﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;

using System.Windows.Forms;

namespace Service
{
    public partial class FrmBilling : Form
    {
        public FrmBilling()
        {
            InitializeComponent();
        }
        int mode = 0;
        int Uid = 0;
        SQLDBHelper db = new SQLDBHelper();
        int LastJobNo;
        BindingSource bsParty = new BindingSource();
        BindingSource bsItem = new BindingSource();
        BindingSource bs = new BindingSource();
        public int SelectId = 0;
        string MenuKey;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);

        SqlCommand qur = new SqlCommand();
        private void FrmBilling_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

            MenuKey = FrmMain.MenuKey;
            if (MenuKey == "13")
            {
                panel1.Visible = true;
                this.Text = "Billing";
                Genclass.Dtype = 4;
            }
            else if (MenuKey == "12")
            {
                panel1.Visible = false;
                this.Text = "Estimate";
                Genclass.Dtype = 5;
                LoadItemDataGrid();
            }
            LoadButton(1);
            dtpJobCardDate.Value = DateTime.Now;
            txtRegNo.Focus();
            //LoadGetJobCard(0);
            Loadgrid();
            grSearch.Visible = false;
            Genclass.sum1 = 0;
            getsa();
            tax();
        }
        private void sparetot()
        {
            Genclass.sum1 = 0;
            for (int i = 0; i < dataGridServiceAdvice.RowCount; i++)
            {
                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(dataGridServiceAdvice.Rows[i].Cells[5].Value);
                txtsparetot.Text = Genclass.sum1.ToString();
                txtOil.Text = Genclass.sum1.ToString();
                txtlabour.Text = Genclass.sum1.ToString();
                txttottot.Text = Genclass.sum1.ToString();
            }
        }
        private void LoadButton(int Id)
        {
            try
            {
                if (Id == 1) //Load
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                    btnSave.Text = "Save";
                }
                else if (Id == 2)//Add
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Save";
                }
                else if (Id == 3)//Edit
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        protected void LoadGetJobCard(int Uid)
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@Uid", Uid) };
                if (Uid == 0)
                {


                    DataSet dt = db.GetMultipleData(CommandType.StoredProcedure, "SP_BILLING", para);
                    bs.DataSource = dt;
                    //dataGridJobCard.DataSource = null;
                    //dataGridJobCard.AutoGenerateColumns = false;
                    dataGridJobCard.ColumnCount = 18;
                    dataGridJobCard.Columns[0].Name = "UID";
                    dataGridJobCard.Columns[0].HeaderText = "UID";
                    dataGridJobCard.Columns[0].DataPropertyName = "UID";
                    dataGridJobCard.Columns[0].Visible = false;
                    dataGridJobCard.Columns[1].Name = "BILLNO";
                    dataGridJobCard.Columns[1].HeaderText = "BILLNO";
                    dataGridJobCard.Columns[1].DataPropertyName = "BILLNO";
                    dataGridJobCard.Columns[1].Width = 110;
                    dataGridJobCard.Columns[2].Name = "BILLDATE";
                    dataGridJobCard.Columns[2].HeaderText = "BILLDATE";
                    dataGridJobCard.Columns[2].DataPropertyName = "BILLDATE";
                    dataGridJobCard.Columns[2].Width = 110;
                    dataGridJobCard.Columns[3].Name = "RegNo";
                    dataGridJobCard.Columns[3].HeaderText = "RegNo";
                    dataGridJobCard.Columns[3].DataPropertyName = "Regno";
                    dataGridJobCard.Columns[3].Width = 110;
                    dataGridJobCard.Columns[4].Name = "JOBCARDNO";
                    dataGridJobCard.Columns[4].HeaderText = "JOBCARDNO";
                    dataGridJobCard.Columns[4].DataPropertyName = "JOBCARDNO";

                    dataGridJobCard.Columns[4].Width = 200;
                    dataGridJobCard.Columns[5].Name = "NAME";
                    dataGridJobCard.Columns[5].HeaderText = "NAME";
                    dataGridJobCard.Columns[5].DataPropertyName = "NAME";
                    dataGridJobCard.Columns[5].Width = 200;
                    dataGridJobCard.Columns[6].Name = "MOBILE";
                    dataGridJobCard.Columns[6].HeaderText = "MOBILE";
                    dataGridJobCard.Columns[6].DataPropertyName = "MOBILE";

                    dataGridJobCard.Columns[7].Name = "SERVICEAD";
                    dataGridJobCard.Columns[7].HeaderText = "SERVICEAD";
                    dataGridJobCard.Columns[7].DataPropertyName = "SERVICEAD";

                    dataGridJobCard.Columns[8].Name = "SPCOST";
                    dataGridJobCard.Columns[8].HeaderText = "SPCOST";
                    dataGridJobCard.Columns[8].DataPropertyName = "SPCOST";


                    dataGridJobCard.Columns[9].Name = "LPCOST";
                    dataGridJobCard.Columns[9].HeaderText = "LPCOST";
                    dataGridJobCard.Columns[9].DataPropertyName = "LPCOST";

                    dataGridJobCard.Columns[10].Name = "TOTCOST";
                    dataGridJobCard.Columns[10].HeaderText = "TOTCOST";
                    dataGridJobCard.Columns[10].DataPropertyName = "TOTCOST";

                    dataGridJobCard.Columns[11].Name = "DIS";
                    dataGridJobCard.Columns[11].HeaderText = "DIS";
                    dataGridJobCard.Columns[11].DataPropertyName = "DIS";

                    dataGridJobCard.Columns[12].Name = "TOTAL";
                    dataGridJobCard.Columns[12].HeaderText = "TOTAL";
                    dataGridJobCard.Columns[12].DataPropertyName = "TOTAL";

                    dataGridJobCard.Columns[13].Name = "JOBCARDUID";
                    dataGridJobCard.Columns[13].HeaderText = "JOBCARDUID";
                    dataGridJobCard.Columns[13].DataPropertyName = "JOBCARDUID";


                    dataGridJobCard.Columns[14].Name = "CUSTUID";
                    dataGridJobCard.Columns[14].HeaderText = "CUSTUID";
                    dataGridJobCard.Columns[14].DataPropertyName = "CUSTUID";

                    dataGridJobCard.Columns[15].Name = "Wtrservice";
                    dataGridJobCard.Columns[15].HeaderText = "Water Service";
                    dataGridJobCard.Columns[15].DataPropertyName = "Wtrservice";

                    dataGridJobCard.Columns[16].Name = "Other";
                    dataGridJobCard.Columns[16].HeaderText = "OTher";
                    dataGridJobCard.Columns[16].DataPropertyName = "Other";

                    dataGridJobCard.Columns[17].Name = "OtherCost";
                    dataGridJobCard.Columns[17].HeaderText = "Other Cost";
                    dataGridJobCard.Columns[17].DataPropertyName = "OtherCost";

                    dataGridJobCard.Columns[6].Visible = false;
                    dataGridJobCard.Columns[7].Visible = false;
                    dataGridJobCard.Columns[8].Visible = false;
                    dataGridJobCard.Columns[9].Visible = false;
                    dataGridJobCard.Columns[10].Visible = false;
                    dataGridJobCard.Columns[11].Visible = false;
                    dataGridJobCard.Columns[12].Visible = false;
                    dataGridJobCard.Columns[13].Visible = false;
                    dataGridJobCard.Columns[14].Visible = false;
                    dataGridJobCard.Columns[15].Visible = false;
                    dataGridJobCard.Columns[16].Visible = false;
                    dataGridJobCard.Columns[17].Visible = false;
                    dataGridJobCard.DataSource = dt.Tables[1];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();
                DateTime str9 = Convert.ToDateTime(dtpJobCardDate.Text);
                if (Genclass.Dtype == 4)
                {
                    string quy = "SELECT DISTINCT  A.UID,A.BILLNO,A.BILLDATE,A.REGNO,A.JOBCARDNO,C.NAME,C.MOBILE,A.SERVICEAD,A.SPCOST,A.LPCOST,A.TOTCOST,A.DIS,A.TOTAL,A.JOBCARDUID,A.CUSTUID,A.WTRSERVICE,A.OTHER,A.OTHERCOST FROM  billm A  INNER JOIN billD B ON A.UID=B.HEADID INNER JOIN CUSTM C ON A.CUSTUID=C.UID   where  month(billdate) = " + str9.Month + " and year(billdate) = " + str9.Year + "   Order by a.Uid desc";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                else if (Genclass.Dtype == 5)
                {
                    string quy = "SELECT DISTINCT  A.UID,A.BILLNO AS ESTIMATENO,A.BILLDATE ESTIMATEDATE,A.REGNO,A.JOBCARDNO,C.NAME,C.MOBILE,A.SERVICEAD,A.SPCOST,A.LPCOST,A.TOTCOST,A.DIS,A.TOTAL,A.JOBCARDUID,A.CUSTUID,A.WTRSERVICE,A.OTHER,A.OTHERCOST FROM  ESTIMATEM A  INNER JOIN ESTIMATED B ON A.UID=B.HEADID INNER JOIN CUSTM C ON A.CUSTUID=C.UID   where  month(billdate) = " + str9.Month + " and year(billdate) = " + str9.Year + "   Order by a.Uid desc";
                    Genclass.cmd = new SqlCommand(quy, conn);


                }
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                dataGridJobCard.AutoGenerateColumns = false;
                dataGridJobCard.Refresh();
                dataGridJobCard.DataSource = null;
                dataGridJobCard.Rows.Clear();
                dataGridJobCard.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    dataGridJobCard.Columns[Genclass.i].Name = column.ColumnName;
                    dataGridJobCard.Columns[Genclass.i].HeaderText = column.ColumnName;
                    dataGridJobCard.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }
                //dataGridJobCard.Columns[0].Name = "UID";
                //dataGridJobCard.Columns[0].HeaderText = "UID";
                //dataGridJobCard.Columns[0].DataPropertyName = "UID";
                dataGridJobCard.Columns[0].Visible = false;
                //dataGridJobCard.Columns[1].Name = "BILLNO";
                //dataGridJobCard.Columns[1].HeaderText = "BILLNO";
                //dataGridJobCard.Columns[1].DataPropertyName = "BILLNO";
                dataGridJobCard.Columns[1].Width = 110;
                //dataGridJobCard.Columns[2].Name = "BILLDATE";
                //dataGridJobCard.Columns[2].HeaderText = "BILLDATE";
                //dataGridJobCard.Columns[2].DataPropertyName = "BILLDATE";
                dataGridJobCard.Columns[2].Width = 110;
                //dataGridJobCard.Columns[3].Name = "RegNo";
                //dataGridJobCard.Columns[3].HeaderText = "RegNo";
                //dataGridJobCard.Columns[3].DataPropertyName = "Regno";
                dataGridJobCard.Columns[3].Width = 110;
                //dataGridJobCard.Columns[4].Name = "JOBCARDNO";
                //dataGridJobCard.Columns[4].HeaderText = "JOBCARDNO";
                //dataGridJobCard.Columns[4].DataPropertyName = "JOBCARDNO";

                dataGridJobCard.Columns[4].Width = 150;
                //dataGridJobCard.Columns[5].Name = "NAME";
                //dataGridJobCard.Columns[5].HeaderText = "NAME";
                //dataGridJobCard.Columns[5].DataPropertyName = "NAME";
                dataGridJobCard.Columns[5].Width = 200;
                //dataGridJobCard.Columns[6].Name = "MOBILE";
                //dataGridJobCard.Columns[6].HeaderText = "MOBILE";
                //dataGridJobCard.Columns[6].DataPropertyName = "MOBILE";
                dataGridJobCard.Columns[6].Width = 100;

                //dataGridJobCard.Columns[7].Name = "SERVICEAD";
                //dataGridJobCard.Columns[7].HeaderText = "SERVICEAD";
                //dataGridJobCard.Columns[7].DataPropertyName = "SERVICEAD";

                //dataGridJobCard.Columns[8].Name = "SPCOST";
                //dataGridJobCard.Columns[8].HeaderText = "SPCOST";
                //dataGridJobCard.Columns[8].DataPropertyName = "SPCOST";


                //dataGridJobCard.Columns[9].Name = "LPCOST";
                //dataGridJobCard.Columns[9].HeaderText = "LPCOST";
                //dataGridJobCard.Columns[9].DataPropertyName = "LPCOST";

                //dataGridJobCard.Columns[10].Name = "TOTCOST";
                //dataGridJobCard.Columns[10].HeaderText = "TOTCOST";
                dataGridJobCard.Columns[10].DataPropertyName = "TOTCOST";

                //dataGridJobCard.Columns[11].Name = "DIS";
                //dataGridJobCard.Columns[11].HeaderText = "DIS";
                //dataGridJobCard.Columns[11].DataPropertyName = "DIS";

                //dataGridJobCard.Columns[12].Name = "TOTAL";
                //dataGridJobCard.Columns[12].HeaderText = "TOTAL";
                //dataGridJobCard.Columns[12].DataPropertyName = "TOTAL";
                dataGridJobCard.Columns[12].Width = 100;
                //dataGridJobCard.Columns[13].Name = "JOBCARDUID";
                //dataGridJobCard.Columns[13].HeaderText = "JOBCARDUID";
                //dataGridJobCard.Columns[13].DataPropertyName = "JOBCARDUID";


                //dataGridJobCard.Columns[14].Name = "CUSTUID";
                //dataGridJobCard.Columns[14].HeaderText = "CUSTUID";
                //dataGridJobCard.Columns[14].DataPropertyName = "CUSTUID";


                dataGridJobCard.Columns[7].Visible = false;
                dataGridJobCard.Columns[8].Visible = false;
                dataGridJobCard.Columns[9].Visible = false;
                dataGridJobCard.Columns[10].Visible = false;
                dataGridJobCard.Columns[11].Visible = false;

                dataGridJobCard.Columns[13].Visible = false;
                dataGridJobCard.Columns[14].Visible = false;

                dataGridJobCard.Columns[15].Visible = false;

                dataGridJobCard.Columns[16].Visible = false;
                dataGridJobCard.Columns[17].Visible = false;
                dataGridJobCard.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();

            }
        }
        protected void ClearControl()
        {
            txtRegNo.Text = string.Empty;
            txtCustomer.Text = string.Empty;
            txtMobile.Text = string.Empty;
            txtJobCardNo.Text = string.Empty;
            txtKm.Text = string.Empty;
            txtOil.Text = "0";
            txtSpares.Text = "0";
            txtlabour.Text = "0";
            txtWtrService.Text = string.Empty;
            txtOthers.Text = string.Empty;
            txtotherValu.Text = string.Empty;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                mode = 1;
                Genclass.sum5 = 0;

                if (Genclass.Dtype == 4)
                {
                    label16.Text = "Bill No";
                    label12.Text = "Bill Date";
                    label17.Visible = false;
                    label19.Visible = false;
                    label20.Visible = false;
                    txtitem.Visible = false;
                    txtqty.Visible = false;
                    txtprice.Visible = false;
                    buttrqok.Visible = false;

                    label11.Visible = true;
                    label18.Visible = true;
                    txtWtrService.Visible = true;
                    txtcolor.Visible = true;
                }
                else if (Genclass.Dtype == 5)
                {
                    label16.Text = "Estimate No";
                    label12.Text = "Estimate Date";
                    label11.Visible = false;
                    label18.Visible = false;
                    txtWtrService.Visible = false;
                    txtcolor.Visible = false;
                    label17.Visible = true;
                    label19.Visible = true;
                    label20.Visible = true;
                    txtitem.Visible = true;
                    txtqty.Visible = true;
                    txtprice.Visible = true;
                    buttrqok.Visible = true;
                }
                Genclass.Module.Gendocno();
                txtengine.Text = Genclass.ST;
                //txtJobCardNo.Text = GetLastJobNo();
                ClearControl();
                LoadButton(2);
                //loaslabour();
                DataGridDescription.Refresh();
                DataGridDescription.DataSource = null;
                DataGridDescription.Rows.Clear();
                dataGridServiceAdvice.Refresh();
                dataGridServiceAdvice.DataSource = null;
                dataGridServiceAdvice.Rows.Clear();
                getsa();
                txtcolor.Text = "0";
                txtsparetot.Text = "0";
                txttottot.Text = "0";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtRegNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("RegNo LIKE '%{0}%' ", txtRegNo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtRegNo_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsParty.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(txtRegNo);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Party Search";
        }
        protected void FillGrid1(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 6;
                DataGridCommon.Columns[0].Name = "juid";
                DataGridCommon.Columns[0].HeaderText = "juid";
                DataGridCommon.Columns[0].DataPropertyName = "juid";
                DataGridCommon.Columns[1].Name = "regno";
                DataGridCommon.Columns[1].HeaderText = "Regno";
                DataGridCommon.Columns[1].DataPropertyName = "regno";
                DataGridCommon.Columns[2].Name = "jobcardno";
                DataGridCommon.Columns[2].HeaderText = "jobcardno";
                DataGridCommon.Columns[2].DataPropertyName = "jobcardno";
                DataGridCommon.Columns[3].Name = "Name";
                DataGridCommon.Columns[3].HeaderText = "Name";
                DataGridCommon.Columns[3].DataPropertyName = "Name";
                DataGridCommon.Columns[4].Name = "mobile";
                DataGridCommon.Columns[4].HeaderText = "mobile";
                DataGridCommon.Columns[4].DataPropertyName = "mobile";
                DataGridCommon.Columns[5].Name = "uid";
                DataGridCommon.Columns[5].HeaderText = "uid";
                DataGridCommon.Columns[5].DataPropertyName = "uid";


                DataGridCommon.Columns[1].Width = 350;
                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;
                DataGridCommon.Columns[4].Visible = false;
                DataGridCommon.Columns[5].Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "itemname";
                DataGridCommon.Columns[1].HeaderText = "itemname";
                DataGridCommon.Columns[1].DataPropertyName = "itemname";



                DataGridCommon.Columns[1].Width = 350;
                DataGridCommon.DataSource = bsItem;
                DataGridCommon.Columns[0].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Genclass.type == 1)
                {
                    dt = db.GetData(CommandType.StoredProcedure, "SP_GETREGNO");
                    bsParty.DataSource = dt;
                }
                else if (Genclass.type == 2)
                {
                    dt = db.GetData(CommandType.StoredProcedure, "SP_GETITEMMAS");
                    bsItem.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void loadspare()
        {
            string quy = "select distinct d.uid as juid,f.itemname,f.uid,d.qty,d.rate,case  f.uom  when 'Ltrs' then  isnull(d.rate,0) else isnull(d.qty,0)* isnull(d.rate,0)  end Value  from jobcard a left join JobCardlist b on a.juid=b.juid left join storesissuem  c on a.juid=c.jobcarduid left join storesissued  d on c.uid = d.siuid  left join custm e on a.custuid = e.uid inner join itemm f on d.itemuid = f.uid   where a.juid=" + txtRegNo.Tag + "";
            Genclass.cmd = new SqlCommand(quy, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            dataGridServiceAdvice.AutoGenerateColumns = false;
            dataGridServiceAdvice.Refresh();
            dataGridServiceAdvice.DataSource = null;
            dataGridServiceAdvice.Rows.Clear();
            dataGridServiceAdvice.ColumnCount = tap.Columns.Count;
            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dataGridServiceAdvice.Columns[Genclass.i].Name = column.ColumnName;
                dataGridServiceAdvice.Columns[Genclass.i].HeaderText = column.ColumnName;
                dataGridServiceAdvice.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }
            dataGridServiceAdvice.DataSource = null;
            dataGridServiceAdvice.AutoGenerateColumns = false;
            dataGridServiceAdvice.ColumnCount = 6;
            dataGridServiceAdvice.Columns[0].Name = "juid";
            dataGridServiceAdvice.Columns[0].HeaderText = "juid";
            dataGridServiceAdvice.Columns[0].Visible = false; ;
            dataGridServiceAdvice.Columns[1].Name = "itemname";
            dataGridServiceAdvice.Columns[1].HeaderText = "Spare";
            dataGridServiceAdvice.Columns[1].Width = 200;
            dataGridServiceAdvice.Columns[2].Name = "uid";
            dataGridServiceAdvice.Columns[2].HeaderText = "uid";
            dataGridServiceAdvice.Columns[2].Visible = false;
            dataGridServiceAdvice.Columns[3].Name = "qty";
            dataGridServiceAdvice.Columns[3].HeaderText = "Qty";
            dataGridServiceAdvice.Columns[3].Width = 100;
            dataGridServiceAdvice.Columns[4].Name = "rate";
            dataGridServiceAdvice.Columns[4].HeaderText = "Rate";
            dataGridServiceAdvice.Columns[4].Width = 100;
            dataGridServiceAdvice.Columns[5].Name = "Value";
            dataGridServiceAdvice.Columns[5].HeaderText = "Value";
            dataGridServiceAdvice.Columns[5].Width = 100;

            dataGridServiceAdvice.DataSource = tap;


        }

        private void loaslabour()
        {

            DataGridDescription.DataSource = null;
            DataGridDescription.AutoGenerateColumns = false;
            DataGridDescription.ColumnCount = 6;
            DataGridDescription.Columns[0].Name = "juid";
            DataGridDescription.Columns[0].HeaderText = "juid";
            DataGridDescription.Columns[0].Visible = false;
            DataGridDescription.Columns[1].Name = "Labour";
            DataGridDescription.Columns[1].HeaderText = "Labour";
            DataGridDescription.Columns[1].Width = 200;
            DataGridDescription.Columns[2].Name = "uid";
            DataGridDescription.Columns[2].HeaderText = "uid";
            DataGridDescription.Columns[2].Visible = false;
            DataGridDescription.Columns[3].Name = "qty";
            DataGridDescription.Columns[3].HeaderText = "Qty";
            DataGridDescription.Columns[3].Width = 100;
            DataGridDescription.Columns[4].Name = "rate";
            DataGridDescription.Columns[4].HeaderText = "Rate";
            DataGridDescription.Columns[4].Width = 100;
            DataGridDescription.Columns[5].Name = "Value";
            DataGridDescription.Columns[5].HeaderText = "Value";
            DataGridDescription.Columns[5].Width = 100;
        }
        public void tax()
        {
            conn.Open();
            string qur = "select Generalname  as type,uid as uid from generalm where   Typem_uid in (6)  and active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cmbtax.DataSource = null;
            cmbtax.DataSource = tab;
            cmbtax.DisplayMember = "type";
            cmbtax.ValueMember = "uid";
            cmbtax.SelectedIndex = -1;
            conn.Close();
        }
        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {
                    txtRegNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtRegNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                    txtCustomer.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtMobile.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();

                    txtJobCardNo.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtCustomer.Tag = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                    if (Genclass.Dtype == 4)
                    {
                        loadspare();
                        sparetot();
                    }
                }
                else if (Genclass.type == 2)
                {
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }


                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;
            if (Genclass.type == 1)
            {
                txtRegNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtRegNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                txtCustomer.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                txtMobile.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();

                txtJobCardNo.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtCustomer.Tag = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                if (Genclass.Dtype == 4)
                {
                    loadspare();
                    sparetot();
                }
            }
            else if (Genclass.type == 2)
            {
                txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

            }
            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {
                    txtRegNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtRegNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                    txtCustomer.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtMobile.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();

                    txtJobCardNo.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtCustomer.Tag = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                    if (Genclass.Dtype == 4)
                    {
                        loadspare();
                        sparetot();
                    }
                }
                else if (Genclass.type == 2)
                {
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }
                grSearch.Visible = false;
                SelectId = 0;
            }
        }
        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void txtcolor_TextChanged(object sender, EventArgs e)
        {
            if (txtcolor.Text != string.Empty && txtlabour.Text != string.Empty)
            {
                double dis, dis1;
                dis = Convert.ToDouble(txtlabour.Text) * (Convert.ToDouble(txtcolor.Text) / 100);
                dis1 = Convert.ToDouble(txtlabour.Text) - dis;
                txttottot.Text = dis1.ToString();

            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            LoadButton(1);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (mode == 2)
                {
                    conn.Close();
                    conn.Open();
                    if (Genclass.Dtype == 4)
                    {
                        qur.CommandText = "delete from billd where headid=" + txtengine.Tag + "";
                        qur.ExecuteNonQuery();
                    }
                    else if (Genclass.Dtype == 5)
                    {
                        qur.CommandText = "delete from ESTIMATED where headid=" + txtengine.Tag + "";
                        qur.ExecuteNonQuery();

                    }
                }
                if (txtOthers.Text == "")
                {

                    txtOthers.Text = "0";

                }

                if (txtotherValu.Text == "")
                {

                    txtotherValu.Text = "0";

                }
                if (txtWtrService.Text == "")
                {

                    txtWtrService.Text = "0";

                }

                if (txtcolor.Text == "")
                {

                    txtcolor.Text = "0";

                }
                if (mode == 1)
                {
                    conn.Close();
                    conn.Open();
                    SqlParameter[] para = {
                        new SqlParameter("@UID","0"),
                        new SqlParameter("@BILLNO",txtengine.Text),
                        new SqlParameter("@billdate",Convert.ToDateTime(dtpJobDate.Text)),
                        new SqlParameter("@RegNo",txtRegNo.Text),
                        new SqlParameter("@JOBCARDNO",Convert.ToString(txtJobCardNo.Text)),
                        new SqlParameter("@JobCardUid",txtRegNo.Tag),
                        new SqlParameter("@CUSTUID",txtCustomer.Tag),
                        new SqlParameter("@SERVICEAD",txtKm.Text),
                        new SqlParameter("@SPCOST",Convert.ToDecimal(txtOil.Text)),
                        new SqlParameter("@LPCOST",Convert.ToDecimal(txtSpares.Text)),
                        new SqlParameter("@TOTCOST",Convert.ToDecimal(txtlabour.Text)),
                        new SqlParameter("@DIS",Convert.ToDecimal(txtcolor.Text)),
                        new SqlParameter("@TOTAL",Convert.ToDecimal(txttottot.Text)),
                        new SqlParameter("@Wtrservice",Convert.ToDecimal(txtWtrService.Text)),
                        new SqlParameter("@Other",txtOthers.Text),
                        new SqlParameter("@OtherCost",Convert.ToDecimal(txtotherValu.Text)),
                       new SqlParameter("@Returnid",SqlDbType.Int),
                    };
                    if (Genclass.Dtype == 4)
                    {

                        para[16].Direction = ParameterDirection.Output;
                        Genclass.h = (db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_BILLM", para, conn, 16));

                    }

                    else if (Genclass.Dtype == 5)
                    {
                        para[16].Direction = ParameterDirection.Output;
                        Genclass.h = (db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_ESTIMATEM", para, conn, 16));

                    }

                    for (int i = 0; i < dataGridServiceAdvice.Rows.Count - 1; i++)
                    {
                        SqlParameter[] paraAcc = {
                            new SqlParameter("@HEADID",Genclass.h),
                            new SqlParameter("@SPLID",dataGridServiceAdvice.Rows[i].Cells[0].Value),
                             new SqlParameter("@ITEMUID",dataGridServiceAdvice.Rows[i].Cells[2].Value),
                            new SqlParameter("@QTY",dataGridServiceAdvice.Rows[i].Cells[3].Value),
                            new SqlParameter("@RATE",dataGridServiceAdvice.Rows[i].Cells[4].Value),
                            new SqlParameter("@VALUE",dataGridServiceAdvice.Rows[i].Cells[5].Value),
                            new SqlParameter("@TYPE",1)
                        };
                        if (Genclass.Dtype == 4)
                        {

                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_BILLD", paraAcc, conn);
                        }
                        else if (Genclass.Dtype == 5)
                        {
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_ESTIMATED", paraAcc, conn);
                        }

                    }

                    if (mode == 1)
                    {
                        qur.CommandText = "update doctypem set lastsno=lastsno+1 where docuid=" + Genclass.Dtype + "";
                        qur.ExecuteNonQuery();
                    }
                    MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    conn.Close();
                    conn.Open();
                    SqlParameter[] para = {
                        new SqlParameter("@BILLNO",txtengine.Text),
                        new SqlParameter("@billdate",Convert.ToDateTime(dtpJobDate.Text)),
                        new SqlParameter("@RegNo",txtRegNo.Text),
                        new SqlParameter("@JOBCARDNO",Convert.ToString(txtJobCardNo.Text)),
                        new SqlParameter("@JobCardUid",txtRegNo.Tag),
                        new SqlParameter("@CUSTUID",txtCustomer.Tag),
                        new SqlParameter("@SERVICEAD",txtKm.Text),
                        new SqlParameter("@SPCOST",Convert.ToDecimal(txtOil.Text)),
                        new SqlParameter("@LPCOST",Convert.ToDecimal(txtSpares.Text)),
                        new SqlParameter("@TOTCOST",Convert.ToDecimal(txtlabour.Text)),
                        new SqlParameter("@DIS",Convert.ToDecimal(txtcolor.Text)),
                        new SqlParameter("@TOTAL",Convert.ToDecimal(txttottot.Text)),
                        new SqlParameter("@Wtrservice",Convert.ToDecimal(txtWtrService.Text)),
                        new SqlParameter("@Other",txtOthers.Text),
                        new SqlParameter("@OtherCost",Convert.ToDecimal(txtotherValu.Text)),
                        new SqlParameter("@UID",txtengine.Tag),
                    };

                    if (Genclass.Dtype == 4)
                    {

                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_BILLMUPDATE", para, conn);
                    }
                    else if (Genclass.Dtype == 5)
                    {
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_ESTIMATEMUPDATE", para, conn);
                    }


                    conn.Close();
                    conn.Open();
                    for (int i = 0; i < dataGridServiceAdvice.Rows.Count - 1; i++)
                    {

                        SqlParameter[] paraAcc = {
                            new SqlParameter("@HEADID",txtengine.Tag),
                            new SqlParameter("@SPLID",dataGridServiceAdvice.Rows[i].Cells[0].Value),
                             new SqlParameter("@ITEMUID",dataGridServiceAdvice.Rows[i].Cells[2].Value),
                            new SqlParameter("@QTY",dataGridServiceAdvice.Rows[i].Cells[3].Value),
                            new SqlParameter("@RATE",dataGridServiceAdvice.Rows[i].Cells[4].Value),
                            new SqlParameter("@VALUE",dataGridServiceAdvice.Rows[i].Cells[5].Value),
                            new SqlParameter("@TYPE",1)
                        };
                        if (Genclass.Dtype == 4)
                        {

                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_BILLD", paraAcc, conn);
                        }
                        else if (Genclass.Dtype == 5)
                        {
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_ESTIMATED", paraAcc, conn);
                        }
                    }
                    MessageBox.Show("Record has been updated successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            grBack.Visible = false;
            grFront.Visible = true;
            Loadgrid();
            LoadButton(1);
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            //bs.Filter = string.Format("RegNo Like '%{0}%' or jobcardno Like '%{1}%' or  billno Like '%{2}%'  or  billdate Like '%{3}%' or  name Like '%{4}%'", txtSearch.Text, txtSearch.Text, txtSearch.Text, txtSearch.Text, txtSearch.Text);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            mode = 2;
            txttottot.Text = "0";
            Genclass.sum5 = 0;
            if (Genclass.Dtype == 4)
            {
                label16.Text = "Bill No";
                label12.Text = "Bill Date";
                label17.Visible = false;
                label19.Visible = false;
                label20.Visible = false;
                txtitem.Visible = false;
                txtqty.Visible = false;
                txtprice.Visible = false;
                buttrqok.Visible = false;

                label11.Visible = true;
                label18.Visible = true;
                txtWtrService.Visible = true;
                txtcolor.Visible = true;
            }
            else if (Genclass.Dtype == 5)
            {
                label16.Text = "Estimate No";
                label12.Text = "Estimate Date";
                label11.Visible = false;
                label18.Visible = false;
                txtWtrService.Visible = false;
                txtcolor.Visible = false;
                label17.Visible = true;
                label19.Visible = true;
                label20.Visible = true;
                txtitem.Visible = true;
                txtqty.Visible = true;
                txtprice.Visible = true;
                buttrqok.Visible = true;
            }
            int Index = dataGridJobCard.SelectedCells[0].RowIndex;
            int Uid = Convert.ToInt32(dataGridJobCard.Rows[Index].Cells[0].Value.ToString());
            txtengine.Tag = Convert.ToInt32(dataGridJobCard.Rows[Index].Cells[0].Value.ToString());
            LoadButton(3);

            txtcolor.Text = "0";
            txtsparetot.Text = "0";
            DataGridDescription.Refresh();
            DataGridDescription.DataSource = null;
            DataGridDescription.Rows.Clear();
            dataGridServiceAdvice.Refresh();
            dataGridServiceAdvice.DataSource = null;
            dataGridServiceAdvice.Rows.Clear();
            txtengine.Text = dataGridJobCard.Rows[Index].Cells[1].Value.ToString();
            dtpJobDate.Text = dataGridJobCard.Rows[Index].Cells[2].Value.ToString();
            txtRegNo.Text = dataGridJobCard.Rows[Index].Cells[3].Value.ToString();
            txtJobCardNo.Text = dataGridJobCard.Rows[Index].Cells[4].Value.ToString();
            txtCustomer.Text = dataGridJobCard.Rows[Index].Cells[5].Value.ToString();
            txtMobile.Text = dataGridJobCard.Rows[Index].Cells[6].Value.ToString();
            txtKm.Text = dataGridJobCard.Rows[Index].Cells[7].Value.ToString();
            txtOil.Text = dataGridJobCard.Rows[Index].Cells[8].Value.ToString();
            txtSpares.Text = dataGridJobCard.Rows[Index].Cells[9].Value.ToString();
            txtlabour.Text = dataGridJobCard.Rows[Index].Cells[10].Value.ToString();
            txtcolor.Text = dataGridJobCard.Rows[Index].Cells[11].Value.ToString();
            txttottot.Text = dataGridJobCard.Rows[Index].Cells[12].Value.ToString();
            txtRegNo.Tag = dataGridJobCard.Rows[Index].Cells[13].Value.ToString();
            txtCustomer.Tag = dataGridJobCard.Rows[Index].Cells[14].Value.ToString();
            txtWtrService.Text = dataGridJobCard.Rows[Index].Cells[15].Value.ToString();
            txtOthers.Text = dataGridJobCard.Rows[Index].Cells[16].Value.ToString();
            txtotherValu.Text = dataGridJobCard.Rows[Index].Cells[17].Value.ToString();

            if (Genclass.Dtype == 4)
            {

                loadspareedit();
            }
            else if (Genclass.Dtype == 5)
            {
                LoadItemDataGrid();
                editload();
            }

            fun2();
        }
        private void editload()
        {

            string quy = "select distinct b.splid as juid,c.itemname,c.uid as itemuid,b.qty,b.rate,b.Value,b.uid  from ESTIMATEM  a inner join ESTIMATEd b on a.uid=b.headid inner join itemm c on b.itemuid=c.uid   where a.uid=" + txtengine.Tag + "";
            Genclass.cmd = new SqlCommand(quy, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            for (int k = 0; k < tap.Rows.Count; k++)
            {
                var index = dataGridServiceAdvice.Rows.Add();

                dataGridServiceAdvice.Rows[index].Cells[0].Value = tap.Rows[k]["juid"].ToString();
                dataGridServiceAdvice.Rows[index].Cells[1].Value = tap.Rows[k]["itemname"].ToString();

                dataGridServiceAdvice.Rows[index].Cells[2].Value = tap.Rows[k]["itemuid"].ToString();

                dataGridServiceAdvice.Rows[index].Cells[3].Value = tap.Rows[k]["qty"].ToString();

                dataGridServiceAdvice.Rows[index].Cells[4].Value = tap.Rows[k]["Rate"].ToString();
                dataGridServiceAdvice.Rows[index].Cells[5].Value = tap.Rows[k]["Value"].ToString();
                dataGridServiceAdvice.Rows[index].Cells[6].Value = tap.Rows[k]["uid"].ToString();
            }
        }
        private void fun2()
        {
            Genclass.sum5 = 0;
            for (int i = 0; i < dataGridServiceAdvice.Rows.Count; i++)
            {
                Genclass.sum5 = Genclass.sum5 + Convert.ToDouble(dataGridServiceAdvice.Rows[i].Cells[5].Value);
            }
            txtsparetot.Text = Genclass.sum5.ToString();

        }
        private void loadspareedit()
        {
            string quy = "select distinct b.splid as juid,c.itemname,c.uid,b.qty,b.rate,b.Value,b.uid  from billm  a inner join billd b on a.uid=b.headid inner join itemm c on b.itemuid=c.uid   where a.uid=" + txtengine.Tag + "";
            Genclass.cmd = new SqlCommand(quy, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            dataGridServiceAdvice.AutoGenerateColumns = false;
            dataGridServiceAdvice.Refresh();
            dataGridServiceAdvice.DataSource = null;
            dataGridServiceAdvice.Rows.Clear();
            dataGridServiceAdvice.ColumnCount = tap.Columns.Count;
            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dataGridServiceAdvice.Columns[Genclass.i].Name = column.ColumnName;
                dataGridServiceAdvice.Columns[Genclass.i].HeaderText = column.ColumnName;
                dataGridServiceAdvice.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dataGridServiceAdvice.DataSource = null;
            dataGridServiceAdvice.AutoGenerateColumns = false;
            dataGridServiceAdvice.ColumnCount = 7;
            dataGridServiceAdvice.Columns[0].Name = "juid";
            dataGridServiceAdvice.Columns[0].HeaderText = "juid";
            dataGridServiceAdvice.Columns[0].Visible = false; ;
            dataGridServiceAdvice.Columns[1].Name = "itemname";
            dataGridServiceAdvice.Columns[1].HeaderText = "Spare";
            dataGridServiceAdvice.Columns[1].Width = 200;
            dataGridServiceAdvice.Columns[2].Name = "uid";
            dataGridServiceAdvice.Columns[2].HeaderText = "uid";
            dataGridServiceAdvice.Columns[2].Visible = false;
            dataGridServiceAdvice.Columns[3].Name = "qty";
            dataGridServiceAdvice.Columns[3].HeaderText = "Qty";
            dataGridServiceAdvice.Columns[3].Width = 100;
            dataGridServiceAdvice.Columns[4].Name = "rate";
            dataGridServiceAdvice.Columns[4].HeaderText = "Rate";
            dataGridServiceAdvice.Columns[4].Width = 100;
            dataGridServiceAdvice.Columns[5].Name = "Value";
            dataGridServiceAdvice.Columns[5].HeaderText = "Value";
            dataGridServiceAdvice.Columns[5].Width = 100;
            dataGridServiceAdvice.Columns[6].Name = "uid";
            dataGridServiceAdvice.Columns[6].HeaderText = "uid";
            dataGridServiceAdvice.Columns[6].Visible = false;
            dataGridServiceAdvice.DataSource = tap;
        }

        private void txttottot_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtJobCardNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtOil_TextChanged(object sender, EventArgs e)
        {
            if (txtOil.Text != string.Empty)
            {
                if (txtSpares.Text == "")
                {
                    txtSpares.Text = "0";
                    return;
                }
                double dis6, dis8, dis9;
                dis6 = Convert.ToDouble(txtSpares.Text) + (Convert.ToDouble(txtOil.Text));
                txtlabour.Text = dis6.ToString();
                if (txtcolor.Text == "")
                {
                    txtcolor.Text = "0";
                }
                dis8 = Convert.ToDouble(txtlabour.Text) * (Convert.ToDouble(txtcolor.Text) / 100);

                dis9 = Convert.ToDouble(txtlabour.Text) - dis8;
                txttottot.Text = dis9.ToString();
            }
        }
        private void txtlabour_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSpares_TextChanged(object sender, EventArgs e)
        {
            if (txtSpares.Text != string.Empty)
            {
                if (txtSpares.Text == "" || txtSpares.Text == "0")
                {
                    txtSpares.Text = "0";
                    return;
                }
                if (txtotherValu.Text == "" || txtotherValu.Text == "0")
                {
                    txtotherValu.Text = "0";

                }
                double dis6, dis8, dis9;
                dis6 = Convert.ToDouble(txtSpares.Text) + (Convert.ToDouble(txtOil.Text)) + Convert.ToDouble(txtotherValu.Text);
                txtlabour.Text = dis6.ToString();
                if (txtcolor.Text == "")
                {
                    txtcolor.Text = "0";
                }
                dis8 = Convert.ToDouble(txtlabour.Text) * (Convert.ToDouble(txtcolor.Text) / 100);
                dis9 = Convert.ToDouble(txtlabour.Text) - dis8;
                txttottot.Text = dis9.ToString();
            }
        }
        protected void getsa()
        {
            try
            {
                string Query = "Select uid,mechanicName from mechanicmas order by uid desc";
                DataTable dt = db.GetData(CommandType.Text, Query);
                AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string RegNo = dt.Rows[i]["mechanicName"].ToString();
                        coll.Add(RegNo);
                    }
                }
                txtKm.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtKm.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtKm.AutoCompleteCustomSource = coll;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void txtKm_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtKm.Text != string.Empty)
                {
                    string Query = "Select uid,mechanicName from  mechanicmas where  mechanicName ='" + txtKm.Text + "'";
                    DataTable dt = db.GetData(CommandType.Text, Query);
                    if (dt.Rows.Count != 0)
                    {
                        txtKm.Tag = dt.Rows[0]["uid"].ToString();

                    }
                    else
                    {
                        MessageBox.Show("Data Not Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtCustomer_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnprintBill_Click(object sender, EventArgs e)
        {
            try
            {
                int index = dataGridJobCard.SelectedCells[0].RowIndex;

                if (Genclass.Dtype == 4)
                {
                    GenParameters.ReportType = "Billing";
                }
                else if (Genclass.Dtype == 5)
                {

                    GenParameters.ReportType = "Estimate";
                }
                GenParameters.ReportId = Convert.ToInt32(dataGridJobCard.Rows[index].Cells[0].Value);
                FrmReportViewver rpt = new FrmReportViewver();
                rpt.MdiParent = this.MdiParent;
                rpt.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void grFront_Enter(object sender, EventArgs e)
        {

        }

        private void txtWtrService_TextChanged(object sender, EventArgs e)
        {
            if (txtWtrService.Text != string.Empty)
            {
                if (txtWtrService.Text == "")
                {
                    txtWtrService.Text = "0";
                }
                double dis6, dis8, dis9;
                dis6 = Convert.ToDouble(txtSpares.Text) + Convert.ToDouble(txtOil.Text) + Convert.ToDouble(txtWtrService.Text);
                txtlabour.Text = dis6.ToString();
                if (txtcolor.Text == "")
                {
                    txtcolor.Text = "0";
                }
                dis8 = Convert.ToDouble(txtlabour.Text) * (Convert.ToDouble(txtcolor.Text) / 100);
                dis9 = Convert.ToDouble(txtlabour.Text) - dis8;
                txttottot.Text = dis9.ToString();
            }
        }

        private void txtotherValu_TextChanged(object sender, EventArgs e)
        {
            if (txtotherValu.Text != string.Empty)
            {
                if (txtotherValu.Text == "")
                {
                    txtotherValu.Text = "0";
                }
                if (txtWtrService.Text == "")
                {
                    txtWtrService.Text = "0";
                }
                double dis6, dis8, dis9;
                dis6 = Convert.ToDouble(txtSpares.Text) + Convert.ToDouble(txtOil.Text) + Convert.ToDouble(txtWtrService.Text) + Convert.ToDouble(txtotherValu.Text);
                txtlabour.Text = dis6.ToString();
                if (txtcolor.Text == "")
                {
                    txtcolor.Text = "0";
                }
                dis8 = Convert.ToDouble(txtlabour.Text) * (Convert.ToDouble(txtcolor.Text) / 100);
                dis9 = Convert.ToDouble(txtlabour.Text) - dis8;
                txttottot.Text = dis9.ToString();
            }
        }

        private void txtcolor_TextChanged_1(object sender, EventArgs e)
        {
            if (txtcolor.Text != string.Empty)
            {
                if (txtcolor.Text == "" || txtcolor.Text == "0")
                {
                    txtcolor.Text = "0";
                    return;
                }
                if (txtWtrService.Text == "")
                {
                    txtWtrService.Text = "0";
                }
                double dis6, dis8, dis9;
                dis6 = Convert.ToDouble(txtSpares.Text) + Convert.ToDouble(txtOil.Text);
                txtlabour.Text = dis6.ToString();
                if (txtcolor.Text == "")
                {
                    txtcolor.Text = "0";
                }
                dis8 = Convert.ToDouble(txtlabour.Text) * (Convert.ToDouble(txtcolor.Text) / 100);
                dis9 = Convert.ToDouble(txtlabour.Text) - dis8;
                txttottot.Text = dis9.ToString();
            }
        }

        private void txtitem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsItem.Filter = string.Format("itemname LIKE '%{0}%' ", txtitem.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtitem_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = getParty();
            bsItem.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtitem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Party Search";
        }

        private void buttrqok_Click(object sender, EventArgs e)
        {
            if (txtitem.Text != string.Empty || txtqty.Text != string.Empty)
            {


                Genclass.strsql = "select distinct uid as itemuid,Itemname," + txtprice.Text + " as Price," + txtqty.Text + "  as Qty from itemm    where Uid = " + txtitem.Tag + "";




                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);



                for (int i = 0; i < tap1.Rows.Count; i++)
                {

                    var index = dataGridServiceAdvice.Rows.Add();
                    dataGridServiceAdvice.Rows[index].Cells[0].Value = "0";
                    dataGridServiceAdvice.Rows[index].Cells[1].Value = tap1.Rows[i]["Itemname"].ToString();

                    dataGridServiceAdvice.Rows[index].Cells[2].Value = tap1.Rows[i]["itemuid"].ToString();
                    dataGridServiceAdvice.Rows[index].Cells[3].Value = tap1.Rows[i]["Qty"].ToString();
                    dataGridServiceAdvice.Rows[index].Cells[4].Value = tap1.Rows[i]["Price"].ToString();
                    decimal deg = Convert.ToDecimal(tap1.Rows[i]["Qty"].ToString()) * Convert.ToDecimal(tap1.Rows[i]["Price"].ToString());
                    dataGridServiceAdvice.Rows[index].Cells[5].Value = deg.ToString();
                    dataGridServiceAdvice.Rows[index].Cells[6].Value = "0";

                    Genclass.sum5 = Genclass.sum5 + Convert.ToDouble(dataGridServiceAdvice.Rows[index].Cells[5].Value);
                }
                txtsparetot.Text = Genclass.sum5.ToString();

                //txtOil.Text= Genclass.sum5.ToString();
                //txtlabour.Text = Genclass.sum5.ToString();
                //txtlabour.Text = Genclass.sum5.ToString();
                //txttottot.Text= Genclass.sum5.ToString(); 

            }
            txtitem.Text = string.Empty;
            txtqty.Text = string.Empty;
            txtprice.Text = string.Empty;


        }

        private void LoadItemDataGrid()
        {
            try
            {
                dataGridServiceAdvice.DataSource = null;
                dataGridServiceAdvice.AutoGenerateColumns = false;
                dataGridServiceAdvice.ColumnCount = 7;

                dataGridServiceAdvice.Columns[0].Name = "juid";
                dataGridServiceAdvice.Columns[0].HeaderText = "juid";
                dataGridServiceAdvice.Columns[0].Visible = false;

                dataGridServiceAdvice.Columns[1].Name = "ItemName";
                dataGridServiceAdvice.Columns[1].HeaderText = "ItemName";
                dataGridServiceAdvice.Columns[1].Width = 200;

                dataGridServiceAdvice.Columns[2].Name = "ItemUid";
                dataGridServiceAdvice.Columns[2].HeaderText = "ItemUid";
                dataGridServiceAdvice.Columns[2].Visible = false;

                dataGridServiceAdvice.Columns[3].Name = "qty";
                dataGridServiceAdvice.Columns[3].HeaderText = "qty";
                dataGridServiceAdvice.Columns[3].Width = 100;
                dataGridServiceAdvice.Columns[4].Name = "Price";
                dataGridServiceAdvice.Columns[4].HeaderText = "Price";
                dataGridServiceAdvice.Columns[4].Width = 100;
                dataGridServiceAdvice.Columns[5].Name = "Value";
                dataGridServiceAdvice.Columns[5].HeaderText = "Value";
                dataGridServiceAdvice.Columns[5].Width = 80;
                dataGridServiceAdvice.Columns[6].Name = "uid";
                dataGridServiceAdvice.Columns[6].HeaderText = "uid";
                dataGridServiceAdvice.Columns[6].Visible = false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnser_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            DateTime str9 = Convert.ToDateTime(dtpJobCardDate.Text);
            if (Genclass.Dtype == 4)
            {
                string quy = "SELECT DISTINCT  A.UID,A.BILLNO,A.BILLDATE,A.REGNO,A.JOBCARDNO,C.NAME,C.MOBILE,A.SERVICEAD,A.SPCOST,A.LPCOST,A.TOTCOST,A.DIS,A.TOTAL,A.JOBCARDUID,A.CUSTUID FROM  billm A  INNER JOIN billD B ON A.UID=B.HEADID INNER JOIN CUSTM C ON A.CUSTUID=C.UID   where  month(billdate) = " + str9.Month + " and year(billdate) = " + str9.Year + "  and    BILLNO like '%" + txtSearch.Text + "%'  or   billdate like '%" + txtSearch.Text + "%' or   regno like '%" + txtSearch.Text + "%' or   JOBCARDNO like '%" + txtSearch.Text + "%'  or   name like '%" + txtSearch.Text + "%' or   mobile like '%" + txtSearch.Text + "%'  or TOTCOST   like '%" + txtSearch.Text + "%'  Order by a.Uid desc";
                Genclass.cmd = new SqlCommand(quy, conn);
            }
            else if (Genclass.Dtype == 5)
            {
                string quy = "SELECT DISTINCT  A.UID,A.BILLNO AS ESTIMATENO,A.BILLDATE ESTIMATEDATE,A.REGNO,A.JOBCARDNO,C.NAME,C.MOBILE,A.SERVICEAD,A.SPCOST,A.LPCOST,A.TOTCOST,A.DIS,A.TOTAL,A.JOBCARDUID,A.CUSTUID FROM  ESTIMATEM A  INNER JOIN ESTIMATED B ON A.UID=B.HEADID INNER JOIN CUSTM C ON A.CUSTUID=C.UID   where  month(billdate) = " + str9.Month + " and year(billdate) = " + str9.Year + "    BILLNO like '%" + txtSearch.Text + "%'  or   billdate like '%" + txtSearch.Text + "%' or   regno like '%" + txtSearch.Text + "%' or   JOBCARDNO like '%" + txtSearch.Text + "%'  or   name like '%" + txtSearch.Text + "%' or   mobile like '%" + txtSearch.Text + "%'  or TOTCOST   like '%" + txtSearch.Text + "%'  Order by a.Uid desc";
                Genclass.cmd = new SqlCommand(quy, conn);


            }
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            dataGridJobCard.AutoGenerateColumns = false;
            dataGridJobCard.Refresh();
            dataGridJobCard.DataSource = null;
            dataGridJobCard.Rows.Clear();
            dataGridJobCard.ColumnCount = tap.Columns.Count;
            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dataGridJobCard.Columns[Genclass.i].Name = column.ColumnName;
                dataGridJobCard.Columns[Genclass.i].HeaderText = column.ColumnName;
                dataGridJobCard.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }
            //dataGridJobCard.Columns[0].Name = "UID";
            //dataGridJobCard.Columns[0].HeaderText = "UID";
            //dataGridJobCard.Columns[0].DataPropertyName = "UID";
            dataGridJobCard.Columns[0].Visible = false;
            //dataGridJobCard.Columns[1].Name = "BILLNO";
            //dataGridJobCard.Columns[1].HeaderText = "BILLNO";
            //dataGridJobCard.Columns[1].DataPropertyName = "BILLNO";
            dataGridJobCard.Columns[1].Width = 110;
            //dataGridJobCard.Columns[2].Name = "BILLDATE";
            //dataGridJobCard.Columns[2].HeaderText = "BILLDATE";
            //dataGridJobCard.Columns[2].DataPropertyName = "BILLDATE";
            dataGridJobCard.Columns[2].Width = 110;
            //dataGridJobCard.Columns[3].Name = "RegNo";
            //dataGridJobCard.Columns[3].HeaderText = "RegNo";
            //dataGridJobCard.Columns[3].DataPropertyName = "Regno";
            dataGridJobCard.Columns[3].Width = 110;
            //dataGridJobCard.Columns[4].Name = "JOBCARDNO";
            //dataGridJobCard.Columns[4].HeaderText = "JOBCARDNO";
            //dataGridJobCard.Columns[4].DataPropertyName = "JOBCARDNO";

            dataGridJobCard.Columns[4].Width = 150;
            //dataGridJobCard.Columns[5].Name = "NAME";
            //dataGridJobCard.Columns[5].HeaderText = "NAME";
            //dataGridJobCard.Columns[5].DataPropertyName = "NAME";
            dataGridJobCard.Columns[5].Width = 200;
            //dataGridJobCard.Columns[6].Name = "MOBILE";
            //dataGridJobCard.Columns[6].HeaderText = "MOBILE";
            //dataGridJobCard.Columns[6].DataPropertyName = "MOBILE";
            dataGridJobCard.Columns[6].Width = 100;

            //dataGridJobCard.Columns[7].Name = "SERVICEAD";
            //dataGridJobCard.Columns[7].HeaderText = "SERVICEAD";
            //dataGridJobCard.Columns[7].DataPropertyName = "SERVICEAD";

            //dataGridJobCard.Columns[8].Name = "SPCOST";
            //dataGridJobCard.Columns[8].HeaderText = "SPCOST";
            //dataGridJobCard.Columns[8].DataPropertyName = "SPCOST";


            //dataGridJobCard.Columns[9].Name = "LPCOST";
            //dataGridJobCard.Columns[9].HeaderText = "LPCOST";
            //dataGridJobCard.Columns[9].DataPropertyName = "LPCOST";

            //dataGridJobCard.Columns[10].Name = "TOTCOST";
            //dataGridJobCard.Columns[10].HeaderText = "TOTCOST";
            dataGridJobCard.Columns[10].DataPropertyName = "TOTCOST";

            //dataGridJobCard.Columns[11].Name = "DIS";
            //dataGridJobCard.Columns[11].HeaderText = "DIS";
            //dataGridJobCard.Columns[11].DataPropertyName = "DIS";

            //dataGridJobCard.Columns[12].Name = "TOTAL";
            //dataGridJobCard.Columns[12].HeaderText = "TOTAL";
            //dataGridJobCard.Columns[12].DataPropertyName = "TOTAL";
            dataGridJobCard.Columns[12].Width = 100;
            //dataGridJobCard.Columns[13].Name = "JOBCARDUID";
            //dataGridJobCard.Columns[13].HeaderText = "JOBCARDUID";
            //dataGridJobCard.Columns[13].DataPropertyName = "JOBCARDUID";


            //dataGridJobCard.Columns[14].Name = "CUSTUID";
            //dataGridJobCard.Columns[14].HeaderText = "CUSTUID";
            //dataGridJobCard.Columns[14].DataPropertyName = "CUSTUID";


            dataGridJobCard.Columns[7].Visible = false;
            dataGridJobCard.Columns[8].Visible = false;
            dataGridJobCard.Columns[9].Visible = false;
            dataGridJobCard.Columns[10].Visible = false;
            dataGridJobCard.Columns[11].Visible = false;

            dataGridJobCard.Columns[13].Visible = false;
            dataGridJobCard.Columns[14].Visible = false;
            dataGridJobCard.DataSource = tap;
        }

        private void txtsparetot_TextChanged(object sender, EventArgs e)
        {
            if (txtsparetot.Text != string.Empty)
            {
                if (txtSpares.Text == "" || txtSpares.Text == "0")
                {
                    txtSpares.Text = "0";

                }
                if (txtOil.Text == "" || txtOil.Text == "0")
                {
                    txtOil.Text = "0";

                }
                if (txtWtrService.Text == "" || txtWtrService.Text == "0")
                {
                    txtWtrService.Text = "0";

                }
                if (txtotherValu.Text == "" || txtotherValu.Text == "0")
                {
                    txtotherValu.Text = "0";

                }
                txtOil.Text = txtsparetot.Text;
                double dis6, dis8, dis9;
                dis6 = Convert.ToDouble(txtSpares.Text) + Convert.ToDouble(txtsparetot.Text) + Convert.ToDouble(txtWtrService.Text) + Convert.ToDouble(txtotherValu.Text);
                txtlabour.Text = dis6.ToString();
                if (txtcolor.Text == "")
                {
                    txtcolor.Text = "0";
                }
                dis8 = Convert.ToDouble(txtlabour.Text) * (Convert.ToDouble(txtcolor.Text) / 100);
                dis9 = Convert.ToDouble(txtlabour.Text) - dis8;
                txttottot.Text = dis9.ToString();
            }
        }

        private void cmbtax_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbtax.Text != "" && cmbtax.ValueMember != "")
            {
                string qur = "select Generalname  as type,uid as uid,isnull(f1,0) as tax from generalm where   uid=" + cmbtax.SelectedValue + " ";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);



                if (tab.Rows.Count > 0)
                {

                    double pp = (Convert.ToDouble(txttottot.Text) * Convert.ToDouble(tab.Rows[0]["tax"].ToString())) / 100;

                    txtgstval.Text = pp.ToString();

                    double tt = Convert.ToDouble(txttottot.Text) + Convert.ToDouble(txtgstval.Text);

                    txtnetval.Text = tt.ToString();
                }
            }
        }
    }
}
﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Service
{
    class SQLDBHelper
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        public DataTable GetData(CommandType cmdType,string ProcedureName,SqlParameter[] para)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataSet GetMultipleData(CommandType cmdType, string ProcedureName, SqlParameter[] para)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetData(CommandType cmdType, string ProcedureName)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetData1(CommandType cmdType, string ProcedureName)
        {
            DataTable dt1 = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt1;
        }
        public DataSet GetDataWithParamMultiple(CommandType cmdType, string ProcedureName, SqlParameter[] para, SqlConnection DbConn)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Parameters.AddRange(para);
                cmd.Connection = DbConn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public int ExecuteQuery(CommandType cmdType, string ProcedureName,SqlParameter[] para)
        {
            int i = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                i = cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return i;
        }

        public int ExecuteQuery(CommandType cmdType, string ProcedureName)
        {
            int i = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                i = cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return i;
        }

        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlParameter[] para, SqlConnection conn, int Outpara)
        {
            int Id = 0;
            try
            {
                //conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = CommandText;
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                Id = Convert.ToInt32(para[Outpara].Value.ToString());//Solved
                //conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return Id;
        }
        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlParameter[] para, SqlConnection conn)
        {
            int Id = 0;
            try
            {
                //conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = CommandText;
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                Id = cmd.ExecuteNonQuery();
                //conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return Id;
        }
        public DataTable GetDataWithoutParam(CommandType cmdType, string ProcedureName, SqlConnection conn)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public int ExecuteQuery(CommandType cmdType, string ProcedureName, SqlParameter[] para,int OutPara)
        {
            int i = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                cmd.ExecuteNonQuery();
                i = Convert.ToInt32(para[OutPara].Value.ToString());
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return i;
        }
    }
}

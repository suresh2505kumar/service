﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using System.Configuration;

namespace Service
{
    public partial class FrmStoresIssue : Form
    {
        public FrmStoresIssue()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        int LastJobNo;
        BindingSource bsItem = new BindingSource();
        BindingSource bs = new BindingSource();
        int SelectedId = 0;
        BindingSource bsStores = new BindingSource();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);

        SqlCommand qur = new SqlCommand();
        private void FrmStoresIssue_Load(object sender, EventArgs e)
        {
         
            qur.Connection = conn;
            txtDocNo.Text = GetLastStoreIssueNo();
            getJobCardNo();
            GetItem(0);
            LoadButton(1);
            LoadItemDataGrid();
            LoadStoresIssue(1);
            this.DataGridStoreIssue.DefaultCellStyle.Font = new Font("calibri", 10);
            this.DataGridStoreIssue.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            DataGridStoreIssue.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DataGridStoreIssue.EnableHeadersVisualStyles = false;
            DataGridStoreIssue.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            DataGridStoreIssue.RowHeadersVisible = false;
            chckAll.Checked = true;

        }

        protected DataTable LoadStoresIssue(int tag)
        {
            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@LoadTag",tag),
                    new SqlParameter("@Date",Convert.ToDateTime(dtpGridDate.Text))
                };
                dt = db.GetData(CommandType.StoredProcedure, "SP_getStoresIssue", para);
                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                DataGridStore.AutoGenerateColumns = false;
                DataGridStore.DataSource = null;
                DataGridStore.ColumnCount = 8;
                DataGridStore.Columns[0].Name = "Uid";
                DataGridStore.Columns[0].HeaderText = "Uid";
                DataGridStore.Columns[0].DataPropertyName = "Uid";
                DataGridStore.Columns[0].Visible = false;

                DataGridStore.Columns[1].Name = "IssueDate";
                DataGridStore.Columns[1].HeaderText = "IssueDate";
                DataGridStore.Columns[1].DataPropertyName = "IssueDate";

                DataGridStore.Columns[2].Name = "IssueDocNo";
                DataGridStore.Columns[2].HeaderText = "IssueDocNo";
                DataGridStore.Columns[2].DataPropertyName = "IssueDocNo";

                DataGridStore.Columns[3].Name = "JobCardNo";
                DataGridStore.Columns[3].HeaderText = "JobCardNo";
                DataGridStore.Columns[3].DataPropertyName = "JobCardNo";

                DataGridStore.Columns[4].Name = "Name";
                DataGridStore.Columns[4].HeaderText = "Customer Name";
                DataGridStore.Columns[4].DataPropertyName = "Name";
                DataGridStore.Columns[4].Width = 180;
                DataGridStore.Columns[5].Name = "RegNo";
                DataGridStore.Columns[5].HeaderText = "Registration No";
                DataGridStore.Columns[5].DataPropertyName = "RegNo";

                DataGridStore.Columns[6].Name = "DeliveryDate";
                DataGridStore.Columns[6].HeaderText = "DeliveryDate";
                DataGridStore.Columns[6].DataPropertyName = "DeliveryDate";
                DataGridStore.Columns[6].Width = 150;
                DataGridStore.Columns[7].Name = "JobCardUid";
                DataGridStore.Columns[7].HeaderText = "JobCardUid";
                DataGridStore.Columns[7].DataPropertyName = "JobCardUid";
                DataGridStore.Columns[7].Visible = false;
                bsStores.DataSource = dt;
                DataGridStore.DataSource = bsStores;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadButton(int Id)
        {
            try
            {
                if (Id == 1) //Load
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                    btnSave.Text = "Save";
                }
                else if (Id == 2)//Add
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Save";
                }
                else if (Id == 3)//Edit
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetLastStoreIssueNo()
        {
            string StoreIssueNo = string.Empty;
            try
            {
                SqlParameter[] para = { new SqlParameter("@DocType", "Stores Issue") };
                DataTable dt = new DataTable();
                dt = db.GetData(CommandType.StoredProcedure, "SP_GetDocLastNoSt", para);
                if (dt.Rows.Count != 0)
                {


                    LastJobNo = Convert.ToInt32(dt.Rows[0]["LastSNo"].ToString()) ;
                    StoreIssueNo = dt.Rows[0]["PreFix"].ToString() + "/" + LastJobNo.ToString("D5");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return StoreIssueNo;
        }

        protected void getJobCardNo()
        {
            try
            {
                string Query = "Select Juid,JobCardNo from JobCard order by JUid desc";
                DataTable dt = db.GetData(CommandType.Text, Query);
                AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string RegNo = dt.Rows[i]["JobCardNo"].ToString();
                        coll.Add(RegNo);
                    }
                }
                txtJobCardNo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtJobCardNo.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtJobCardNo.AutoCompleteCustomSource = coll;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void txtJobCardNo_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtJobCardNo.Text != string.Empty)
                {
                    string Query1 = "Select Juid,CustUId from JobCard  where JobCardNo ='" + txtJobCardNo.Text + "'";
                    DataTable dt = db.GetData(CommandType.Text, Query1);
                    if (dt.Rows.Count != 0)
                    {
                        txtJobCardNo.Tag = dt.Rows[0]["Juid"].ToString();
                   
                        txtVechileNo.Tag = dt.Rows[0]["CustUId"].ToString();
                    }


                    string Query = "Select distinct a.Juid,a.CustUId,a.regNo from JobCard a inner join CustBike b on a.CustUId = b.CustUId Where a.JobCardNo ='" + txtJobCardNo.Text + "'  and a.CustUId= " + txtVechileNo.Tag + "  and a.juid="+ txtJobCardNo.Tag + "";
                    DataTable dt1 = db.GetData1(CommandType.Text, Query);
                    if (dt1.Rows.Count != 0)
                    {
                        txtJobCardNo.Tag = dt1.Rows[0]["Juid"].ToString();
                        txtVechileNo.Text = dt1.Rows[0]["regNo"].ToString();
                        txtVechileNo.Tag = dt1.Rows[0]["CustUId"].ToString();
                    }
                    else
                    {
                        MessageBox.Show("Data Not Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void GetItem(int Uid)
        {
            try
            {
                SqlParameter[] para = {
                    new SqlParameter("@Uid",Uid)
                };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetItemM", para);
                bsItem.DataSource = dt;
                if (Uid == 0)
                {
                    DataGridItem.DataSource = null;
                    DataGridItem.AutoGenerateColumns = false;
                    DataGridItem.ColumnCount = 5;
                    DataGridItem.Columns[0].Name = "Uid";
                    DataGridItem.Columns[0].HeaderText = "Uid";
                    DataGridItem.Columns[0].DataPropertyName = "Uid";
                    DataGridItem.Columns[0].Visible = false;

                    DataGridItem.Columns[1].Name = "ItemCode";
                    DataGridItem.Columns[1].HeaderText = "ItemCode";
                    DataGridItem.Columns[1].DataPropertyName = "ItemCode";

                    DataGridItem.Columns[2].Name = "ItemName";
                    DataGridItem.Columns[2].HeaderText = "ItemName";
                    DataGridItem.Columns[2].DataPropertyName = "ItemName";
                    DataGridItem.Columns[2].Width = 150;

                    DataGridItem.Columns[3].Name = "UOM";
                    DataGridItem.Columns[3].HeaderText = "UOM";
                    DataGridItem.Columns[3].DataPropertyName = "UOM";
                    DataGridItem.Columns[3].Width = 70;

                    DataGridItem.Columns[4].Name = "Price";
                    DataGridItem.Columns[4].HeaderText = "Price";
                    DataGridItem.Columns[4].DataPropertyName = "ItemPrice";
                    DataGridItem.DataSource = bsItem;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnGridClose_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedId = 1;
                int Index = DataGridItem.SelectedCells[0].RowIndex;
                txtItemName.Text = DataGridItem.Rows[Index].Cells[2].Value.ToString();
                txtItemName.Tag = DataGridItem.Rows[Index].Cells["Uid"].Value.ToString();
                txtQty.Tag = DataGridItem.Rows[Index].Cells[4].Value.ToString();
                txtRemarks.Tag = DataGridItem.Rows[Index].Cells[3].Value.ToString();
                grSearch.Visible = false;
                txtQty.Focus();
                SelectedId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridItem_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnSelect_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridItem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnSelect_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void LoadItemDataGrid()
        {
            try
            {
                DataGridStoreIssue.DataSource = null;
                DataGridStoreIssue.AutoGenerateColumns = false;
                DataGridStoreIssue.ColumnCount = 6;
                DataGridStoreIssue.Columns[0].Name = "ItemUid";
                DataGridStoreIssue.Columns[0].HeaderText = "ItemUid";
                DataGridStoreIssue.Columns[0].Visible = false;
                DataGridStoreIssue.Columns[1].Name = "ItemName";
                DataGridStoreIssue.Columns[1].HeaderText = "ItemName";
                DataGridStoreIssue.Columns[1].Width = 300;
                DataGridStoreIssue.Columns[2].Name = "UOM";
                DataGridStoreIssue.Columns[2].HeaderText = "UOM";

                DataGridStoreIssue.Columns[3].Name = "Qty";
                DataGridStoreIssue.Columns[3].HeaderText = "Qty";

                DataGridStoreIssue.Columns[4].Name = "Price";
                DataGridStoreIssue.Columns[4].HeaderText = "Price";

                DataGridStoreIssue.Columns[5].Name = "dUid";
                DataGridStoreIssue.Columns[5].HeaderText = "dUid";
                DataGridStoreIssue.Columns[5].Visible = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void txtItemName_Enter(object sender, EventArgs e)
        {
            grSearch.Visible = true;
        }

        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            if (SelectedId == 0)
            {
                bsItem.Filter = string.Format("ItemCode Like '%{0}%' or ItemName Like '%{1}%'", txtItemName.Text, txtItemName.Text);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtItemName.Text != string.Empty || txtQty.Text != string.Empty)
                {


                    Genclass.strsql = "select distinct uid as itemuid,Itemname," + txtQty.Tag + " as Price,'" + txtRemarks.Tag + "' as Uom," + txtQty.Text + "  as Qty from itemm    where Uid = " + txtItemName.Tag + "";




                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);



                    for (int i = 0; i < tap1.Rows.Count; i++)
                    {
                      
                        var index = DataGridStoreIssue.Rows.Add();
                        DataGridStoreIssue.Rows[index].Cells[0].Value = tap1.Rows[i]["itemuid"].ToString();

                        DataGridStoreIssue.Rows[index].Cells[1].Value = tap1.Rows[i]["Itemname"].ToString();
                        DataGridStoreIssue.Rows[index].Cells[2].Value = tap1.Rows[i]["Uom"].ToString();
                        if(DataGridStoreIssue.Rows[index].Cells[2].Value.ToString() == "Ltrs")
                        {

                            Genclass.sum5 = Convert.ToDouble(txtQty.Text)/1000;
                            DataGridStoreIssue.Rows[index].Cells[3].Value = Genclass.sum5.ToString();
                            DataGridStoreIssue.Rows[index].Cells[4].Value = 0;

                        }
                        else
                        {
                            DataGridStoreIssue.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
                            DataGridStoreIssue.Rows[index].Cells[4].Value = tap1.Rows[i]["Price"].ToString();


                        }


                        DataGridStoreIssue.Rows[index].Cells[5].Value = 0;



                        //    int Index = DataGridStoreIssue.Rows.Add();
                        //DataGridViewRow row = DataGridStoreIssue.Rows[Index];
                        //row.Cells[0].Value = txtItemName.Tag;
                        //row.Cells[1].Value = txtItemName.Text;
                        //row.Cells[2].Value = txtRemarks.Tag;
                        //row.Cells[3].Value = txtQty.Text;
                        //row.Cells[4].Value = txtQty.Tag;
                        //row.Cells[5].Value = 0;
                        txtItemName.Text = string.Empty;
                        txtQty.Text = string.Empty;
                    }
                }
                else
                {
                    MessageBox.Show("Item Name or Qty should not empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            LoadButton(2);
            txtJobCardNo.Text = string.Empty;
            txtItemName.Text = string.Empty;
            txtVechileNo.Text = string.Empty;
            txtQty.Text = string.Empty;
            txtRemarks.Text = string.Empty;

            Genclass.Dtype = 2;
            Genclass.Module.Gendocno();
            txtDocNo.Text = Genclass.ST;
            dtpDocDate.Value = DateTime.Now;
            DataGridStoreIssue.Refresh();
            DataGridStoreIssue.DataSource = null;
            DataGridStoreIssue.Rows.Clear();



        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnSave.Text == "Save")
                {
                    SqlParameter[] para = {
                        new SqlParameter("@DocNo",txtDocNo.Text),
                        new SqlParameter("@DocDate",Convert.ToDateTime(dtpDocDate.Text)),
                        new SqlParameter("@JobCardUid",txtJobCardNo.Tag),
                        new SqlParameter("@Remarks",txtRemarks.Text),
                        new SqlParameter("@Uid","0"),
                        new SqlParameter("@ReturnId",SqlDbType.Int)
                    };
                    para[5].Direction = ParameterDirection.Output;
                    txtDocNo.Tag = db.ExecuteQuery(CommandType.StoredProcedure, "SP_StoresIssueM", para, 5);

                    for (int i = 0; i < DataGridStoreIssue.Rows.Count-1; i++)
                    {
                        SqlParameter[] paraDet = {
                            new SqlParameter("@Uid","0"),
                            new SqlParameter("@SIUid",txtDocNo.Tag),
                            new SqlParameter("@ItemUid",DataGridStoreIssue.Rows[i].Cells[0].Value.ToString()),
                            new SqlParameter("@Qty",DataGridStoreIssue.Rows[i].Cells[3].Value.ToString()),
                            new SqlParameter("@Rate",DataGridStoreIssue.Rows[i].Cells[4].Value.ToString()),
                         };
                        int id = db.ExecuteQuery(CommandType.StoredProcedure, "SP_StoresIssueD", paraDet);
                    }
                    SqlParameter[] paraDoc = { new SqlParameter("@DocType", "Stores Issue"), new SqlParameter("@LastSNo", LastJobNo) };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_UpdateDocLastNo", paraDoc);
                }
                else
                {


                    conn.Close();
                    conn.Open();

                    qur.CommandText = "delete from  StoresIssued  where SIUid=" + txtDocNo.Tag + " ";
                    qur.ExecuteNonQuery();



                    SqlParameter[] para = {
                        new SqlParameter("@DocNo",txtDocNo.Text),
                        new SqlParameter("@DocDate",Convert.ToDateTime(dtpDocDate.Text)),
                        new SqlParameter("@JobCardUid",txtJobCardNo.Tag),
                        new SqlParameter("@Remarks",txtRemarks.Text),
                        new SqlParameter("@Uid",txtDocNo.Tag),
                     
                    };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_StoresIssueMupdate", para);
         

                    for (int i = 0; i < DataGridStoreIssue.Rows.Count-1; i++)
                    {
                        SqlParameter[] paraDet = {
                            new SqlParameter("@Uid","0"),
                            new SqlParameter("@SIUid",txtDocNo.Tag),
                            new SqlParameter("@ItemUid",DataGridStoreIssue.Rows[i].Cells[0].Value.ToString()),
                            new SqlParameter("@Qty",DataGridStoreIssue.Rows[i].Cells[3].Value.ToString()),
                            new SqlParameter("@Rate",DataGridStoreIssue.Rows[i].Cells[4].Value.ToString()),
                         };
                   
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_StoresIssueD", paraDet, conn);
                    }
                }
                string quyq1 = "select * from StoresIssueM a inner join  StoresIssued b on a.uid=b.siuid  where  a.uid=" + txtDocNo.Tag + " ";

                Genclass.cmd = new SqlCommand(quyq1, conn);
                SqlDataAdapter aptrq1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tapq1 = new DataTable();
                aptrq1.Fill(tapq1);
                if (tapq1.Rows.Count > 0)
                {
                    conn.Close();
                    conn.Open();
                    if (btnSave.Text == "Save")
                    {
                        qur.CommandText = "exec SP_StoreEstockleg " + txtDocNo.Tag + ",1";
                        qur.ExecuteNonQuery();
                    }
                    else

                    {
                        qur.CommandText = "exec SP_StoreEstockleg " + txtDocNo.Tag + ",2";
                        qur.ExecuteNonQuery();
                    }

                }

              
                MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoadStoresIssue(0);
                grBack.Visible = false;
                grFront.Visible = true;
                btnAdd.Visible = true;
                btnEdit.Visible = true;
                btnDelete.Visible = true;
                btnExit.Visible = true;
                btnSave.Visible = false;
                btnBack.Visible = false;
                btnSave.Text = "Save";
                chckAll.Checked = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            LoadButton(1);
        }

        private void chckAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chckAll.Checked == true)
            {
                LoadStoresIssue(0);
            }
            else
            {
                LoadStoresIssue(1);
            }
        }

        protected void ClearControl()
        {
            txtDocNo.Text = string.Empty;
            txtItemName.Text = string.Empty;
            txtJobCardNo.Text = string.Empty;
            txtQty.Text = string.Empty;
            txtVechileNo.Text = string.Empty;
            txtRemarks.Text = string.Empty;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridStore.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGridStore.Rows[Index].Cells[0].Value.ToString());
                txtDocNo.Tag = DataGridStore.Rows[Index].Cells[0].Value.ToString();
                SqlParameter[] para = { new SqlParameter("@Uid", txtDocNo.Tag) };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_EditStoresIssue", para);
                grFront.Visible = false;

                panAdd.Visible = true;
                grBack.Visible = true;
                if (dt.Rows.Count != 0)
                {

                    txtDocNo.Text = dt.Rows[0]["DocNo"].ToString();
                    txtJobCardNo.Text = dt.Rows[0]["JobCardNo"].ToString();
                    txtJobCardNo.Tag = dt.Rows[0]["JobCardUid"].ToString();
                    txtVechileNo.Text = dt.Rows[0]["RegNo"].ToString();
                    txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                }
                DataGridStoreIssue.Refresh();
                DataGridStoreIssue.DataSource = null;
                DataGridStoreIssue.Rows.Clear();
                //Getload(Uid);
                editload();
                panAdd.Visible = true;
                grBack.Visible = true;
                grFront.Visible = false;
                btnAdd.Visible = false;
                btnEdit.Visible = false;
          
                btnDelete.Visible = false;
                btnExit.Visible = false;
                btnSave.Visible = true;
                btnBack.Visible = true;
                btnSave.Text = "Update";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void editload()
        {

            Genclass.strsql = "Select b.uid,b.itemuid,d.itemname,d.uom,b.qty,b.Rate,a.JobCardUid from StoresIssueM a inner join StoresIssueD b on a.Uid = b.SIUid inner join JobCard c on a.JobCardUid = c.JUid inner join ItemM d on b.ItemUid = d.Uid Where a.uid = " + txtDocNo.Tag + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);


            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = DataGridStoreIssue.Rows.Add();

                DataGridStoreIssue.Rows[index].Cells[0].Value = tap1.Rows[k]["Itemuid"].ToString();
                DataGridStoreIssue.Rows[index].Cells[1].Value = tap1.Rows[k]["itemname"].ToString();
      
                DataGridStoreIssue.Rows[index].Cells[2].Value = tap1.Rows[k]["uom"].ToString();
             
                DataGridStoreIssue.Rows[index].Cells[3].Value = tap1.Rows[k]["Qty"].ToString();
           
                DataGridStoreIssue.Rows[index].Cells[4].Value = tap1.Rows[k]["Rate"].ToString();
                DataGridStoreIssue.Rows[index].Cells[5].Value = tap1.Rows[k]["JobCardUid"].ToString();

            }
        }
        private void Getload(int Uid)
        {
           
                SqlParameter[] para = { new SqlParameter("@Uid", Uid) };
            
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_EditStoresIssueNew", para);
                    bs.DataSource = dt;
                    DataGridStoreIssue.DataSource = null;
                    DataGridStoreIssue.AutoGenerateColumns = false;
                    DataGridStoreIssue.ColumnCount = 6;
                    DataGridStoreIssue.Columns[0].Name = "Itemuid";
                    DataGridStoreIssue.Columns[0].HeaderText = "Itemuid";
                    DataGridStoreIssue.Columns[0].DataPropertyName = "Itemuid";
                    DataGridStoreIssue.Columns[0].Visible = false;

                    DataGridStoreIssue.Columns[1].Name = "itemname";
                    DataGridStoreIssue.Columns[1].HeaderText = "itemname";
                    DataGridStoreIssue.Columns[1].DataPropertyName = "itemname";
                    DataGridStoreIssue.Columns[1].Width = 250;

                    DataGridStoreIssue.Columns[2].Name = "uom";
                    DataGridStoreIssue.Columns[2].HeaderText = "uom";
                    DataGridStoreIssue.Columns[2].DataPropertyName = "uom";
                    DataGridStoreIssue.Columns[2].Width = 100;

                    DataGridStoreIssue.Columns[3].Name = "Qty";
                    DataGridStoreIssue.Columns[3].HeaderText = "Qty";
                    DataGridStoreIssue.Columns[3].DataPropertyName = "Qty";
                    DataGridStoreIssue.Columns[3].Width = 140;

                    DataGridStoreIssue.Columns[4].Name = "Rate";
                    DataGridStoreIssue.Columns[4].HeaderText = "Rate";
                    DataGridStoreIssue.Columns[4].DataPropertyName = "Rate";
                    DataGridStoreIssue.Columns[4].Width = 100;

                    DataGridStoreIssue.Columns[5].Name = "JobCardUid";
                    DataGridStoreIssue.Columns[5].HeaderText = "JobCardUid";
                    DataGridStoreIssue.Columns[5].DataPropertyName = "JobCardUid";
               
                    DataGridStoreIssue.Columns[5].Visible = false;
          
            
                    DataGridStoreIssue.DataSource = bs;
                
            
            }
        private void txtJobCardNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void DataGridStoreIssue_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {

        }

        private void dtpGridDate_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}

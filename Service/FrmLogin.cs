﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Service
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        public static int UserId;
        public static string UserName;
        SQLDBHelper db = new SQLDBHelper();
        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtUserName.Text == string.Empty)
                {
                    MessageBox.Show("UserName can't Empty","Information",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    txtUserName.Text = string.Empty;
                    return;
                }
                else if (txtPassword.Text == string.Empty)
                {
                    MessageBox.Show("Password can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPassword.Text = string.Empty;
                    return;
                }
                else
                {
                    UserName = txtUserName.Text;
                    string Password = txtPassword.Text;
                    SqlParameter[] para = {
                        new SqlParameter("@UserName",UserName),
                        new SqlParameter("@Password",Password)
                    };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_UserLogin", para);
                    if(dt.Rows.Count != 0)
                    {
                        UserId = Convert.ToInt32(dt.Rows[0]["UserID"].ToString());
                        this.Hide();
                        FrmMain main = new FrmMain();
                        main.WindowState = FormWindowState.Maximized;
                        main.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}

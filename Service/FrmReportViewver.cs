﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Service
{
    public partial class FrmReportViewver : Form
    {
        public FrmReportViewver()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        private void FrmReportViewver_Load(object sender, EventArgs e)
        {
            if(GenParameters.ReportType=="JobCard")
            {
                LoadjobCard();
            }
            else if (GenParameters.ReportType == "Billing")
            {
                Billing_load();
            }
            else if (GenParameters.ReportType == "Estimate")
            {
                Estimate_load();
            }
            else if (GenParameters.ReportType == "JobCard Register")
            {
                JobcardReg_load();
            }

        }
        private void Billing_load()
        {
            ReportDocument doc = new ReportDocument();
            SqlDataAdapter da = new SqlDataAdapter("SP_BillingrptD", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = GenParameters.ReportId;
            DataSet ds = new DataSet();
            da.Fill(ds, "Billing");
            doc.Load(Application.StartupPath + "\\BillingReport.rpt");
            doc.SetDataSource(ds);
            cryReportPreview.ReportSource = doc;
        }
        private void JobcardReg_load()
        {
            ReportDocument doc = new ReportDocument();
            SqlDataAdapter da = new SqlDataAdapter("Prc_JC_GetJobCardRegister", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@fromdt", SqlDbType.DateTime).Value = Genclass.rrtt1;
            da.SelectCommand.Parameters.Add("@todate", SqlDbType.DateTime).Value = Genclass.err1;

            DataSet ds = new DataSet();
            da.Fill(ds, "JobcardReg");
            doc.Load(Application.StartupPath + "\\CryJobcardReg.rpt");
            doc.SetDataSource(ds);
            cryReportPreview.ReportSource = doc;
        }
        private void Estimate_load()
        {
            ReportDocument doc = new ReportDocument();
            SqlDataAdapter da = new SqlDataAdapter("SP_EstimaterptD", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = GenParameters.ReportId;
            DataSet ds = new DataSet();
            da.Fill(ds, "Billing");
            doc.Load(Application.StartupPath + "\\EstimateReport.rpt");
            doc.SetDataSource(ds);
            cryReportPreview.ReportSource = doc;
        }
        private void LoadjobCard()
        {
            ReportDocument doc = new ReportDocument();
            SqlDataAdapter da = new SqlDataAdapter("SP_JobCardPrint", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@Juid", SqlDbType.Int).Value = GenParameters.ReportId;
            DataSet ds = new DataSet();
            da.Fill(ds, "JobCard");
            SqlDataAdapter daDe = new SqlDataAdapter("Prc_JC_GetJobCardDetail", conn);
            daDe.SelectCommand.CommandType = CommandType.StoredProcedure;
            daDe.SelectCommand.Parameters.Add("@JobCardID", SqlDbType.Int).Value = GenParameters.ReportId;
            daDe.SelectCommand.Parameters.Add("@TypeId", SqlDbType.Int).Value = 8;
            DataSet dt = new DataSet();
            daDe.Fill(dt);

            string resposecode = dt.Tables[0].Rows[0]["ResponseCode"].ToString();
            if(resposecode != "101")
            {
                SqlDataAdapter daAcc = new SqlDataAdapter("Prc_JC_GetJobCardDetail", conn);
                daAcc.SelectCommand.CommandType = CommandType.StoredProcedure;
                daAcc.SelectCommand.Parameters.Add("@JobCardID", SqlDbType.Int).Value = GenParameters.ReportId;
                daAcc.SelectCommand.Parameters.Add("@TypeId", SqlDbType.Int).Value = 2;
                DataSet dtAcc = new DataSet();
                daAcc.Fill(dtAcc);

                doc.Load(Application.StartupPath + "\\CryJobCard.rpt");
                doc.SetDataSource(ds);
                doc.Subreports["CrysJobCardDetail.rpt"].SetDataSource(dt.Tables[1]);
                doc.Subreports["CryJobCardAcc.rpt"].SetDataSource(dtAcc.Tables[1]);
                cryReportPreview.ReportSource = doc;
            }
            else
            {
                MessageBox.Show("No data Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           
        }

        private void cryReportPreview_Load(object sender, EventArgs e)
        {
            if (GenParameters.ReportType == "JobCard Register")
            {
                JobcardReg_load();
            }
        }
    }
}

﻿namespace Service
{
    partial class FrmReportViewver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cryReportPreview = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // cryReportPreview
            // 
            this.cryReportPreview.ActiveViewIndex = -1;
            this.cryReportPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cryReportPreview.Cursor = System.Windows.Forms.Cursors.Default;
            this.cryReportPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cryReportPreview.Location = new System.Drawing.Point(0, 0);
            this.cryReportPreview.Name = "cryReportPreview";
            this.cryReportPreview.Size = new System.Drawing.Size(983, 513);
            this.cryReportPreview.TabIndex = 0;
            this.cryReportPreview.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            this.cryReportPreview.Load += new System.EventHandler(this.cryReportPreview_Load);
            // 
            // FrmReportViewver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 513);
            this.Controls.Add(this.cryReportPreview);
            this.Name = "FrmReportViewver";
            this.Text = "Preview";
            this.Load += new System.EventHandler(this.FrmReportViewver_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer cryReportPreview;
    }
}
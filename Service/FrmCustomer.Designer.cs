﻿namespace Service
{
    partial class FrmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grBack = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.grModel = new System.Windows.Forms.GroupBox();
            this.btnGridClose = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridModel = new System.Windows.Forms.DataGridView();
            this.chckActive = new System.Windows.Forms.CheckBox();
            this.txtDoorNo = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.DataGridBike = new System.Windows.Forms.DataGridView();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.txtRegNo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpDob = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMobile1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtStreet = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panAdd = new System.Windows.Forms.Panel();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridCustomer = new System.Windows.Forms.DataGridView();
            this.grBack.SuspendLayout();
            this.grModel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridBike)).BeginInit();
            this.panAdd.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.label12);
            this.grBack.Controls.Add(this.grModel);
            this.grBack.Controls.Add(this.chckActive);
            this.grBack.Controls.Add(this.txtDoorNo);
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.DataGridBike);
            this.grBack.Controls.Add(this.txtModel);
            this.grBack.Controls.Add(this.txtRegNo);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.dtpDob);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.txtEmail);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.txtPhone);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.txtMobile1);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.txtMobile);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.txtCity);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.txtStreet);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.txtName);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.txtCode);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(7, 1);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(705, 454);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(559, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 18);
            this.label12.TabIndex = 24;
            this.label12.Text = "Model";
            // 
            // grModel
            // 
            this.grModel.Controls.Add(this.btnGridClose);
            this.grModel.Controls.Add(this.btnSelect);
            this.grModel.Controls.Add(this.DataGridModel);
            this.grModel.Location = new System.Drawing.Point(411, 90);
            this.grModel.Name = "grModel";
            this.grModel.Size = new System.Drawing.Size(286, 275);
            this.grModel.TabIndex = 23;
            this.grModel.TabStop = false;
            this.grModel.Visible = false;
            this.grModel.Enter += new System.EventHandler(this.grModel_Enter);
            // 
            // btnGridClose
            // 
            this.btnGridClose.BackColor = System.Drawing.Color.White;
            this.btnGridClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGridClose.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnGridClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGridClose.Location = new System.Drawing.Point(198, 240);
            this.btnGridClose.Name = "btnGridClose";
            this.btnGridClose.Size = new System.Drawing.Size(79, 31);
            this.btnGridClose.TabIndex = 7;
            this.btnGridClose.Text = "Close";
            this.btnGridClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGridClose.UseVisualStyleBackColor = false;
            this.btnGridClose.Click += new System.EventHandler(this.btnGridClose_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.Color.White;
            this.btnSelect.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSelect.Image = global::Service.Properties.Resources.ok;
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelect.Location = new System.Drawing.Point(111, 240);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(86, 31);
            this.btnSelect.TabIndex = 6;
            this.btnSelect.Text = "Select";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // DataGridModel
            // 
            this.DataGridModel.AllowUserToAddRows = false;
            this.DataGridModel.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridModel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridModel.Location = new System.Drawing.Point(3, 16);
            this.DataGridModel.Name = "DataGridModel";
            this.DataGridModel.RowHeadersVisible = false;
            this.DataGridModel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridModel.Size = new System.Drawing.Size(277, 222);
            this.DataGridModel.TabIndex = 0;
            // 
            // chckActive
            // 
            this.chckActive.AutoSize = true;
            this.chckActive.Location = new System.Drawing.Point(241, 411);
            this.chckActive.Name = "chckActive";
            this.chckActive.Size = new System.Drawing.Size(65, 22);
            this.chckActive.TabIndex = 9;
            this.chckActive.Text = "Active";
            this.chckActive.UseVisualStyleBackColor = true;
            this.chckActive.CheckedChanged += new System.EventHandler(this.chckActive_CheckedChanged);
            // 
            // txtDoorNo
            // 
            this.txtDoorNo.Location = new System.Drawing.Point(82, 109);
            this.txtDoorNo.Name = "txtDoorNo";
            this.txtDoorNo.Size = new System.Drawing.Size(265, 26);
            this.txtDoorNo.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 113);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 18);
            this.label11.TabIndex = 22;
            this.label11.Text = "Door No";
            // 
            // DataGridBike
            // 
            this.DataGridBike.AllowUserToAddRows = false;
            this.DataGridBike.BackgroundColor = System.Drawing.Color.White;
            this.DataGridBike.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridBike.Location = new System.Drawing.Point(411, 88);
            this.DataGridBike.Name = "DataGridBike";
            this.DataGridBike.ReadOnly = true;
            this.DataGridBike.RowHeadersVisible = false;
            this.DataGridBike.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridBike.Size = new System.Drawing.Size(286, 341);
            this.DataGridBike.TabIndex = 21;
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(558, 61);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(139, 26);
            this.txtModel.TabIndex = 11;
            this.txtModel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtModel_MouseClick);
            // 
            // txtRegNo
            // 
            this.txtRegNo.Location = new System.Drawing.Point(411, 61);
            this.txtRegNo.MaxLength = 10;
            this.txtRegNo.Name = "txtRegNo";
            this.txtRegNo.Size = new System.Drawing.Size(147, 26);
            this.txtRegNo.TabIndex = 10;
            this.txtRegNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRegNo_KeyDown);
            this.txtRegNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRegNo_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(411, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 18);
            this.label10.TabIndex = 18;
            this.label10.Text = "RegNo";
            // 
            // dtpDob
            // 
            this.dtpDob.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDob.Location = new System.Drawing.Point(82, 406);
            this.dtpDob.Name = "dtpDob";
            this.dtpDob.Size = new System.Drawing.Size(141, 26);
            this.dtpDob.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(40, 410);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 18);
            this.label9.TabIndex = 16;
            this.label9.Text = "DOB";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(82, 364);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(265, 26);
            this.txtEmail.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(33, 368);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 18);
            this.label8.TabIndex = 14;
            this.label8.Text = "Email";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(82, 323);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(265, 26);
            this.txtPhone.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 327);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 18);
            this.label7.TabIndex = 12;
            this.label7.Text = "Phone";
            // 
            // txtMobile1
            // 
            this.txtMobile1.Location = new System.Drawing.Point(82, 282);
            this.txtMobile1.Name = "txtMobile1";
            this.txtMobile1.Size = new System.Drawing.Size(265, 26);
            this.txtMobile1.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 286);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Mobile1";
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(82, 238);
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(265, 26);
            this.txtMobile.TabIndex = 4;
            this.txtMobile.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMobile_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 242);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Mobile";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(82, 193);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(265, 26);
            this.txtCity.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "City";
            // 
            // txtStreet
            // 
            this.txtStreet.Location = new System.Drawing.Point(82, 151);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(265, 26);
            this.txtStreet.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Street";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(82, 61);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(265, 26);
            this.txtName.TabIndex = 0;
            this.txtName.Leave += new System.EventHandler(this.txtName_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name";
            // 
            // txtCode
            // 
            this.txtCode.Enabled = false;
            this.txtCode.Location = new System.Drawing.Point(82, 18);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(141, 26);
            this.txtCode.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Code";
            // 
            // panAdd
            // 
            this.panAdd.BackColor = System.Drawing.Color.White;
            this.panAdd.Controls.Add(this.btnEdit);
            this.panAdd.Controls.Add(this.btnAdd);
            this.panAdd.Controls.Add(this.btnDelete);
            this.panAdd.Controls.Add(this.btnBack);
            this.panAdd.Controls.Add(this.btnExit);
            this.panAdd.Controls.Add(this.btnSave);
            this.panAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panAdd.Location = new System.Drawing.Point(7, 458);
            this.panAdd.Name = "panAdd";
            this.panAdd.Size = new System.Drawing.Size(706, 37);
            this.panAdd.TabIndex = 2;
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEdit.Image = global::Service.Properties.Resources.if_edit_notes_46798;
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(468, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(74, 31);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.Image = global::Service.Properties.Resources.if_add_46776;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(393, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(74, 31);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Image = global::Service.Properties.Resources.if_save_46830__1_;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(547, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(77, 31);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelete.Image = global::Service.Properties.Resources.if_trash_46839___Copy;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(543, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(81, 31);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(625, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(77, 31);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "Back";
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(625, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(77, 31);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DataGridCustomer);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(7, 1);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(705, 453);
            this.grFront.TabIndex = 22;
            this.grFront.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(6, 15);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(691, 26);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // DataGridCustomer
            // 
            this.DataGridCustomer.AllowUserToAddRows = false;
            this.DataGridCustomer.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCustomer.Location = new System.Drawing.Point(6, 42);
            this.DataGridCustomer.Name = "DataGridCustomer";
            this.DataGridCustomer.ReadOnly = true;
            this.DataGridCustomer.RowHeadersVisible = false;
            this.DataGridCustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCustomer.Size = new System.Drawing.Size(692, 404);
            this.DataGridCustomer.TabIndex = 0;
            // 
            // FrmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(719, 501);
            this.Controls.Add(this.panAdd);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.grFront);
            this.MaximizeBox = false;
            this.Name = "FrmCustomer";
            this.Text = "Customer";
            this.Load += new System.EventHandler(this.FrmCustomer_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grModel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridBike)).EndInit();
            this.panAdd.ResumeLayout(false);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCustomer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtStreet;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMobile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMobile1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpDob;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.TextBox txtRegNo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView DataGridBike;
        private System.Windows.Forms.Panel panAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView DataGridCustomer;
        private System.Windows.Forms.TextBox txtDoorNo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chckActive;
        private System.Windows.Forms.GroupBox grModel;
        private System.Windows.Forms.DataGridView DataGridModel;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnGridClose;
        private System.Windows.Forms.Label label12;
    }
}
﻿namespace Service
{
    partial class FrmStoresIssue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grBack = new System.Windows.Forms.GroupBox();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.btnGridClose = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridItem = new System.Windows.Forms.DataGridView();
            this.DataGridStoreIssue = new System.Windows.Forms.DataGridView();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.txtVechileNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtJobCardNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpDocDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDocNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panAdd = new System.Windows.Forms.Panel();
            this.chckAll = new System.Windows.Forms.CheckBox();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.dtpGridDate = new System.Windows.Forms.DateTimePicker();
            this.DataGridStore = new System.Windows.Forms.DataGridView();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.grBack.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStoreIssue)).BeginInit();
            this.panAdd.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStore)).BeginInit();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.DataGridStoreIssue);
            this.grBack.Controls.Add(this.txtRemarks);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.btnOk);
            this.grBack.Controls.Add(this.txtQty);
            this.grBack.Controls.Add(this.txtItemName);
            this.grBack.Controls.Add(this.txtVechileNo);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.txtJobCardNo);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.dtpDocDate);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.txtDocNo);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(9, 2);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(693, 449);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // grSearch
            // 
            this.grSearch.Controls.Add(this.btnGridClose);
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.DataGridItem);
            this.grSearch.Location = new System.Drawing.Point(45, 117);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(450, 275);
            this.grSearch.TabIndex = 24;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            // 
            // btnGridClose
            // 
            this.btnGridClose.BackColor = System.Drawing.Color.White;
            this.btnGridClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGridClose.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnGridClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGridClose.Location = new System.Drawing.Point(364, 240);
            this.btnGridClose.Name = "btnGridClose";
            this.btnGridClose.Size = new System.Drawing.Size(79, 31);
            this.btnGridClose.TabIndex = 7;
            this.btnGridClose.Text = "Close";
            this.btnGridClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGridClose.UseVisualStyleBackColor = false;
            this.btnGridClose.Click += new System.EventHandler(this.btnGridClose_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.Color.White;
            this.btnSelect.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSelect.Image = global::Service.Properties.Resources.ok1;
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelect.Location = new System.Drawing.Point(294, 240);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(69, 31);
            this.btnSelect.TabIndex = 6;
            this.btnSelect.Text = "Select";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // DataGridItem
            // 
            this.DataGridItem.AllowUserToAddRows = false;
            this.DataGridItem.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridItem.Location = new System.Drawing.Point(3, 16);
            this.DataGridItem.Name = "DataGridItem";
            this.DataGridItem.ReadOnly = true;
            this.DataGridItem.RowHeadersVisible = false;
            this.DataGridItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridItem.Size = new System.Drawing.Size(441, 222);
            this.DataGridItem.TabIndex = 0;
            this.DataGridItem.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridItem_CellMouseDoubleClick);
            this.DataGridItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridItem_KeyDown);
            // 
            // DataGridStoreIssue
            // 
            this.DataGridStoreIssue.BackgroundColor = System.Drawing.Color.White;
            this.DataGridStoreIssue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStoreIssue.Location = new System.Drawing.Point(45, 117);
            this.DataGridStoreIssue.Margin = new System.Windows.Forms.Padding(4);
            this.DataGridStoreIssue.Name = "DataGridStoreIssue";
            this.DataGridStoreIssue.Size = new System.Drawing.Size(618, 243);
            this.DataGridStoreIssue.TabIndex = 215;
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(121, 367);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(542, 73);
            this.txtRemarks.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 370);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "Remarks";
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.White;
            this.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOk.Image = global::Service.Properties.Resources.ok1;
            this.btnOk.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnOk.Location = new System.Drawing.Point(610, 82);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(53, 25);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "Ok";
            this.btnOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(496, 81);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(113, 26);
            this.txtQty.TabIndex = 4;
            // 
            // txtItemName
            // 
            this.txtItemName.Location = new System.Drawing.Point(45, 81);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(450, 26);
            this.txtItemName.TabIndex = 3;
            this.txtItemName.TextChanged += new System.EventHandler(this.txtItemName_TextChanged);
            this.txtItemName.Enter += new System.EventHandler(this.txtItemName_Enter);
            // 
            // txtVechileNo
            // 
            this.txtVechileNo.Location = new System.Drawing.Point(383, 52);
            this.txtVechileNo.Name = "txtVechileNo";
            this.txtVechileNo.Size = new System.Drawing.Size(169, 26);
            this.txtVechileNo.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(302, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Vechile No";
            // 
            // txtJobCardNo
            // 
            this.txtJobCardNo.Location = new System.Drawing.Point(383, 17);
            this.txtJobCardNo.Name = "txtJobCardNo";
            this.txtJobCardNo.Size = new System.Drawing.Size(169, 26);
            this.txtJobCardNo.TabIndex = 1;
            this.txtJobCardNo.TextChanged += new System.EventHandler(this.txtJobCardNo_TextChanged);
            this.txtJobCardNo.Leave += new System.EventHandler(this.txtJobCardNo_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(296, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Job Card No";
            // 
            // dtpDocDate
            // 
            this.dtpDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDocDate.Location = new System.Drawing.Point(130, 49);
            this.dtpDocDate.Name = "dtpDocDate";
            this.dtpDocDate.Size = new System.Drawing.Size(100, 26);
            this.dtpDocDate.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Doc Date";
            // 
            // txtDocNo
            // 
            this.txtDocNo.Location = new System.Drawing.Point(130, 17);
            this.txtDocNo.Name = "txtDocNo";
            this.txtDocNo.Size = new System.Drawing.Size(160, 26);
            this.txtDocNo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Doc No";
            // 
            // panAdd
            // 
            this.panAdd.BackColor = System.Drawing.Color.White;
            this.panAdd.Controls.Add(this.chckAll);
            this.panAdd.Controls.Add(this.btnEdit);
            this.panAdd.Controls.Add(this.btnAdd);
            this.panAdd.Controls.Add(this.btnBack);
            this.panAdd.Controls.Add(this.btnSave);
            this.panAdd.Controls.Add(this.btnDelete);
            this.panAdd.Controls.Add(this.btnExit);
            this.panAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panAdd.Location = new System.Drawing.Point(9, 454);
            this.panAdd.Name = "panAdd";
            this.panAdd.Size = new System.Drawing.Size(758, 37);
            this.panAdd.TabIndex = 3;
            // 
            // chckAll
            // 
            this.chckAll.AutoSize = true;
            this.chckAll.Location = new System.Drawing.Point(13, 8);
            this.chckAll.Name = "chckAll";
            this.chckAll.Size = new System.Drawing.Size(44, 22);
            this.chckAll.TabIndex = 6;
            this.chckAll.Text = "All";
            this.chckAll.UseVisualStyleBackColor = true;
            this.chckAll.CheckedChanged += new System.EventHandler(this.chckAll_CheckedChanged);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEdit.Image = global::Service.Properties.Resources.if_edit_notes_46798;
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(520, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(74, 31);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.Image = global::Service.Properties.Resources.if_add_46776;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(445, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(74, 31);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(677, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(77, 31);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "Back";
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Image = global::Service.Properties.Resources.if_save_46830__1_;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(595, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 31);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelete.Image = global::Service.Properties.Resources.if_trash_46839___Copy;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(595, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(81, 31);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(677, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(77, 31);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.dtpGridDate);
            this.grFront.Controls.Add(this.DataGridStore);
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(9, 5);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(758, 443);
            this.grFront.TabIndex = 9;
            this.grFront.TabStop = false;
            // 
            // dtpGridDate
            // 
            this.dtpGridDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGridDate.Location = new System.Drawing.Point(628, 14);
            this.dtpGridDate.Name = "dtpGridDate";
            this.dtpGridDate.Size = new System.Drawing.Size(124, 26);
            this.dtpGridDate.TabIndex = 2;
            this.dtpGridDate.ValueChanged += new System.EventHandler(this.dtpGridDate_ValueChanged);
            // 
            // DataGridStore
            // 
            this.DataGridStore.AllowUserToAddRows = false;
            this.DataGridStore.BackgroundColor = System.Drawing.Color.White;
            this.DataGridStore.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStore.Location = new System.Drawing.Point(6, 39);
            this.DataGridStore.Name = "DataGridStore";
            this.DataGridStore.RowHeadersVisible = false;
            this.DataGridStore.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridStore.Size = new System.Drawing.Size(746, 395);
            this.DataGridStore.TabIndex = 0;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(6, 14);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(622, 26);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // FrmStoresIssue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(777, 500);
            this.Controls.Add(this.panAdd);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmStoresIssue";
            this.Text = "Stores Issue";
            this.Load += new System.EventHandler(this.FrmStoresIssue_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStoreIssue)).EndInit();
            this.panAdd.ResumeLayout(false);
            this.panAdd.PerformLayout();
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStore)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.DateTimePicker dtpDocDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtJobCardNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtVechileNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.Panel panAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.DataGridView DataGridStore;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.Button btnGridClose;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridItem;
        private System.Windows.Forms.DateTimePicker dtpGridDate;
        private System.Windows.Forms.CheckBox chckAll;
        private System.Windows.Forms.DataGridView DataGridStoreIssue;
    }
}
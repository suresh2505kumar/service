﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Service
{
    public partial class FrmPurchase : Form
    {
        public FrmPurchase()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsBillFrom = new BindingSource();


        private void FrmPurchase_Load(object sender, EventArgs e)
        {
            LoadItemGrid();
            LoadItemTax();
            LoadFrontGrid();
            tax();
            LoadButton(1);
        }

        public void tax()
        {
            string qur = "select Generalname as tax,Cast( F1 as int) uid from generalm where   Typem_uid in (6) and active=1 ";
            DataTable tab = db.GetData(CommandType.Text, qur);
            txtTaxPercentage.DataSource = null;
            txtTaxPercentage.DisplayMember = "tax";
            txtTaxPercentage.ValueMember = "uid";
            txtTaxPercentage.DataSource = tab;
        }

        private void LoadButton(int Id)
        {
            try
            {
                if (Id == 1) //Load
                {
                    GrBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                    btnSave.Text = "Save";
                }
                else if (Id == 2)//Add
                {
                    GrBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Save";
                }
                else if (Id == 3)//Edit
                {
                    GrBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadItemGrid()
        {
            DataGridItem.DataSource = null;
            DataGridItem.AutoGenerateColumns = false;
            DataGridItem.ColumnCount = 7;
            DataGridItem.Columns[0].Name = "Item Code";
            DataGridItem.Columns[0].HeaderText = "Item Code";
            DataGridItem.Columns[0].Width = 120;

            DataGridItem.Columns[1].Name = "Item name";
            DataGridItem.Columns[1].HeaderText = "Item name";
            DataGridItem.Columns[1].Width = 240;

            DataGridItem.Columns[2].Name = "UOM";
            DataGridItem.Columns[2].HeaderText = "UOM";
            DataGridItem.Columns[2].Width = 80;

            DataGridItem.Columns[3].Name = "Qty";
            DataGridItem.Columns[3].HeaderText = "Qty";
            DataGridItem.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridItem.Columns[3].Width = 90;

            DataGridItem.Columns[4].Name = "Rate";
            DataGridItem.Columns[4].HeaderText = "Rate";
            DataGridItem.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridItem.Columns[4].Width = 90;

            DataGridItem.Columns[5].Name = "Value";
            DataGridItem.Columns[5].HeaderText = "Value";
            DataGridItem.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridItem.Columns[5].Width = 100;

            DataGridItem.Columns[6].Name = "Uid";
            DataGridItem.Columns[6].HeaderText = "Uid";
            DataGridItem.Columns[6].Visible = false;
        }

        protected void LoadItemTax()
        {
            DataGridTax.DataSource = null;
            DataGridTax.AutoGenerateColumns = false;
            DataGridTax.ColumnCount = 4;

            DataGridTax.Columns[0].Name = "Taxable Value";
            DataGridTax.Columns[0].HeaderText = "Taxable Value";
            DataGridTax.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridTax.Columns[0].Width = 130;

            DataGridTax.Columns[1].Name = "Tax %";
            DataGridTax.Columns[1].HeaderText = "Tax %";
            DataGridTax.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridTax.Columns[1].Width = 130;

            DataGridTax.Columns[2].Name = "Tax Value";
            DataGridTax.Columns[2].HeaderText = "Tax Value";
            DataGridTax.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridTax.Columns[2].Width = 130;

            DataGridTax.Columns[3].Name = "Uid";
            DataGridTax.Columns[3].HeaderText = "Uid";
            DataGridTax.Columns[3].Visible = false;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtBillAddress.Text = string.Empty;
            txtBillNo.Text = string.Empty;
            txtBillTo.Text = string.Empty;
            TotaltaxableValue.Text = string.Empty;
            txtTotalTaxValue.Text = string.Empty;
            txtRoundedOff.Text = string.Empty;
            txtNetValue.Text = string.Empty;
            DataGridItem.Rows.Clear();
            DataGridTax.Rows.Clear();
            txtBillNo.Tag = "0";
            LoadButton(2);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            LoadFrontGrid();
            LoadButton(1);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtItemCode.Text == string.Empty)
                {
                    MessageBox.Show("Item Code can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtItemCode.Focus();
                    return;
                }
                else if (txtItemName.Text == string.Empty)
                {
                    MessageBox.Show("Item Name can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtItemName.Focus();
                    return;
                }
                else if(txtQty.Text == string.Empty)
                {
                    MessageBox.Show("Qty can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtQty.Focus();
                    return;
                }
                else if(txtRate.Text == string.Empty)
                {
                    MessageBox.Show("Rate can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtRate.Focus();
                    return;
                }
                else if(txtTotal.Text == string.Empty)
                {
                    MessageBox.Show("Total can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTotal.Focus();
                    return;
                }
                else
                {
                    int Index = DataGridItem.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridItem.Rows[Index];
                    dataGridViewRow.Cells[0].Value = txtItemCode.Text;
                    dataGridViewRow.Cells[1].Value = txtItemName.Text;
                    dataGridViewRow.Cells[2].Value = txtUom.Text;
                    dataGridViewRow.Cells[3].Value = txtQty.Text;
                    dataGridViewRow.Cells[4].Value = txtRate.Text;
                    dataGridViewRow.Cells[5].Value = txtTotal.Text;
                    dataGridViewRow.Cells[6].Value = "0";
                    txtItemName.Text = string.Empty;
                    txtItemCode.Text = string.Empty;
                    txtQty.Text = string.Empty;
                    txtRate.Text = string.Empty;
                    txtTotal.Text = string.Empty;
                    txtItemCode.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtBillNo.Text == string.Empty)
                {
                    MessageBox.Show("Bill Number Can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBillNo.Focus();
                    return;
                }
                else if (txtBillTo.Text == string.Empty)
                {
                    MessageBox.Show("Bill TO Can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBillTo.Focus();
                    return;
                }
                else if(DataGridItem.Rows.Count == 0)
                {
                    MessageBox.Show("Enter Item Details", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtItemCode.Focus();
                    return;
                }
                else
                {
                    if(txtBillNo.Tag == null)
                    {
                        txtBillNo.Tag = "0";
                    }
                    SqlParameter[] sqlparameters = {
                        new SqlParameter("@Uid",txtBillNo.Tag),
                        new SqlParameter("@BillNumber",txtBillNo.Text),
                        new SqlParameter("@BillDate",Convert.ToDateTime(DtpBillDate.Text)),
                        new SqlParameter("@CustomerUid",txtBillTo.Tag),
                        new SqlParameter("@TaxableValue",Convert.ToDecimal(TotaltaxableValue.Text)),
                        new SqlParameter("@TaxValue",Convert.ToDecimal(txtTotalTaxValue.Text)),
                        new SqlParameter("@Roff",Convert.ToDecimal(txtRoundedOff.Text)),
                        new SqlParameter("@NetValue",Convert.ToDecimal(txtNetValue.Text)),
                        new SqlParameter("@ReturnUid",SqlDbType.BigInt)
                    };
                    sqlparameters[8].Direction = ParameterDirection.Output;
                    int PUid = db.ExecuteQuery(CommandType.StoredProcedure, "Proc_PurchaseM", sqlparameters, 8);

                    for (int i = 0; i < DataGridItem.Rows.Count; i++)
                    {
                        decimal t = Convert.ToDecimal(DataGridItem.Rows[i].Cells[5].Value.ToString());
                        decimal Q = Convert.ToDecimal(DataGridItem.Rows[i].Cells[3].Value.ToString());
                        SqlParameter[] sqlparametersItem = {
                            new SqlParameter("@Uid",DataGridItem.Rows[i].Cells[6].Value.ToString()),
                            new SqlParameter("@PUid",PUid),
                            new SqlParameter("@ItemCode",DataGridItem.Rows[i].Cells[0].Value.ToString()),
                            new SqlParameter("@ItemName",DataGridItem.Rows[i].Cells[1].Value.ToString()),
                            new SqlParameter("@Uom",DataGridItem.Rows[i].Cells[2].Value.ToString()),
                            new SqlParameter("@MRPRate",Convert.ToDecimal(DataGridItem.Rows[i].Cells[3].Value.ToString())),
                            new SqlParameter("@Qty",Q),
                            new SqlParameter("@Rate",t/Q),
                            new SqlParameter("@TotalValue",t)
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "Proc_PurchaseList", sqlparametersItem);
                    }

                    for (int i = 0; i < DataGridTax.Rows.Count; i++)
                    {
                        SqlParameter[] sqlparameterstax = {
                            new SqlParameter("@Uid",DataGridTax.Rows[i].Cells[3].Value.ToString()),
                            new SqlParameter("@PUid",PUid),
                            new SqlParameter("@TxableValue",Convert.ToDecimal(DataGridTax.Rows[i].Cells[0].Value.ToString())),
                            new SqlParameter("@TaxPer",Convert.ToDecimal(DataGridTax.Rows[i].Cells[1].Value.ToString())),
                            new SqlParameter("@TaxValue",Convert.ToDecimal(DataGridTax.Rows[i].Cells[2].Value.ToString())),
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "Proc_PuchaseTax", sqlparameterstax);
                    }
                    MessageBox.Show("record hasbeen saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBillAddress.Text = string.Empty;
                    txtBillNo.Text = string.Empty;
                    txtBillTo.Text = string.Empty;
                    TotaltaxableValue.Text = string.Empty;
                    txtTotalTaxValue.Text = string.Empty;
                    txtRoundedOff.Text = string.Empty;
                    txtNetValue.Text = string.Empty;
                    DataGridItem.Rows.Clear();
                    DataGridTax.Rows.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnTaxOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTotalValue.Text == string.Empty)
                {
                    MessageBox.Show("taxble Value can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTotalValue.Focus();
                    return;
                }
                else if (txtTaxPercentage.SelectedIndex == -1)
                {
                    MessageBox.Show("tax percentage can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTaxPercentage.Focus();
                    return;
                }
                else if (txtTaxValue.Text == string.Empty)
                {
                    MessageBox.Show("Tax value can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTaxValue.Focus();
                    return;
                }
                else
                {
                    int Index = DataGridTax.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridTax.Rows[Index];
                    dataGridViewRow.Cells[0].Value = txtTotalValue.Text;
                    dataGridViewRow.Cells[1].Value = txtTaxPercentage.SelectedValue;
                    dataGridViewRow.Cells[2].Value = txtTaxValue.Text;
                    dataGridViewRow.Cells[3].Value = "0";
                    TextboxValue();
                    LoadAmountDetails();
                    txtTotalValue.Text = string.Empty;
                    txtTaxPercentage.SelectedIndex = -1;
                    txtTaxValue.Text = string.Empty;
                    txtTotalValue.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void TextboxValue()
        {
            try
            {
                decimal totalBasicValue = 0;
                decimal TaxValue = 0;
                for (int i = 0; i < DataGridTax.Rows.Count; i++)
                {
                    totalBasicValue += Convert.ToDecimal(DataGridTax.Rows[i].Cells[0].Value.ToString());
                    TaxValue += Convert.ToDecimal(DataGridTax.Rows[i].Cells[2].Value.ToString());
                }
                TotaltaxableValue.Text = totalBasicValue.ToString("0.00");
                txtTotalTaxValue.Text = TaxValue.ToString("0.00");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadAmountDetails()
        {
            try
            {
                if (txtTotalTaxValue.Text == string.Empty )
                {
                    txtTotalTaxValue.Text = "0";
                }
                else if (TotaltaxableValue.Text == string.Empty)
                {
                    TotaltaxableValue.Text = "0";
                }
                else
                {
                    decimal basicAmount = Convert.ToDecimal(TotaltaxableValue.Text);
                    decimal TaxAmount = Convert.ToDecimal(txtTotalTaxValue.Text);
                    double a = Convert.ToDouble(basicAmount + TaxAmount);
                    string[] str = a.ToString("0.00").Split('.');
                    double num1 = Convert.ToDouble("0." + str[1]);
                    double res;
                    if (num1 < 0.51)
                    {
                        res = Math.Floor(a);
                    }
                    else
                    {
                        res = Math.Round(a);
                    }
                    txtRoundedOff.Text = (res - a).ToString("0.00");
                    txtNetValue.Text = res.ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void txtBillTo_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = "Select Uid,Name,Code from PartyM Where Type ='Supplier'";
                DataTable dataTable = db.GetData(CommandType.Text, Query);
                bsBillFrom.DataSource = dataTable;
                FillDtataGrid(dataTable, 1);
                Point point = FindLocation(txtBillTo);
                GrSearch.Location = new Point(point.X, point.Y + 20);
                GrSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        int FillId = 0;
        private void FillDtataGrid(DataTable dataTable, int v)
        {
            try
            {
                if(v == 1)
                {
                    FillId = 1;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Name";
                    DataGridCommon.Columns[1].Width = 250;

                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "Code";
                    DataGridCommon.DataSource = bsBillFrom;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        int SelectId = 0;
        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if(FillId == 1)
                {
                    txtBillTo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtBillTo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                GrSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnSelect_Click(sender, e);
        }

        private void btnGridClose_Click(object sender, EventArgs e)
        {
            GrSearch.Visible = false;
        }

        private void txtBillTo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if(SelectId == 0)
                {
                    bsBillFrom.Filter = string.Format("Name Like '%{0}%'", txtBillTo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtTaxPercentage_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if(txtTotalValue.Text == string.Empty)
                {
                    txtTotalValue.Text = "0";
                }
                else if(txtTaxPercentage.SelectedIndex == -1)
                {
                    txtTaxPercentage.SelectedIndex = 0;
                }
                else
                {
                    double totalvlaue = Convert.ToDouble(txtTotalValue.Text);
                    double TaxPer = Convert.ToDouble(txtTaxPercentage.SelectedValue);
                    txtTaxValue.Text = ((totalvlaue * TaxPer) / 100).ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridItem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if(e.KeyCode == Keys.Delete)
                {
                    DialogResult dialogResult = MessageBox.Show("Do you want to delete this record !", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if(dialogResult == DialogResult.Yes)
                    {
                        int Index = DataGridItem.SelectedCells[0].RowIndex;
                        int Uid = Convert.ToInt32(DataGridItem.Rows[Index].Cells[6].Value.ToString());
                        if (Uid == 0)
                        {
                            DataGridItem.Rows.RemoveAt(Index);
                            DataGridItem.ClearSelection();
                            DataGridTax.Rows.Clear();
                        }
                        else
                        {
                            string Query = "Delete from PurchaseList Where Uid =" + Uid + "";
                            db.ExecuteQuery(CommandType.Text, Query);
                            DataGridItem.Rows.RemoveAt(Index);
                            DataGridItem.ClearSelection();
                            DataGridTax.Rows.Clear();
                        }
                    }
                    else
                    {
                       
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridTax_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult dialogResult = MessageBox.Show("Do you want to delete this record !", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int Index = DataGridTax.SelectedCells[0].RowIndex;
                        int Uid = Convert.ToInt32(DataGridTax.Rows[Index].Cells[3].Value.ToString());
                        if (Uid == 0)
                        {
                            DataGridTax.Rows.RemoveAt(Index);
                            DataGridTax.ClearSelection();
                        }
                        else
                        {
                            string Query = "Delete from PurchaseTax Where Uid =" + Uid + "";
                            db.ExecuteQuery(CommandType.Text, Query);
                            DataGridTax.Rows.RemoveAt(Index);
                            DataGridTax.ClearSelection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridPurchase.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGridPurchase.Rows[Index].Cells[0].Value.ToString());
                SqlParameter[] sqlParameters = { new SqlParameter("@Uid", Uid) };
                db.ExecuteQuery(CommandType.StoredProcedure, "Proc_DeletePurchase", sqlParameters);
                MessageBox.Show("Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridPurchase_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if(e.KeyCode == Keys.Delete)
                {
                    btnDelete_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                txtBillAddress.Text = string.Empty;
                txtBillNo.Text = string.Empty;
                txtBillTo.Text = string.Empty;
                TotaltaxableValue.Text = string.Empty;
                txtTotalTaxValue.Text = string.Empty;
                txtRoundedOff.Text = string.Empty;
                txtNetValue.Text = string.Empty;
                DataGridItem.Rows.Clear();
                DataGridTax.Rows.Clear();
                int Index = DataGridPurchase.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGridPurchase.Rows[Index].Cells[0].Value.ToString());
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Uid",Uid)
                };
                DataSet dataSet = db.GetMultipleData(CommandType.StoredProcedure, "Proc_EditPurchase", sqlParameters);
                DataTable dtPurchase = dataSet.Tables[0];
                DataTable dtPurchaseList = dataSet.Tables[1];
                DataTable dtPurchasetax = dataSet.Tables[2];

                txtBillNo.Text = dtPurchase.Rows[0]["BillNumber"].ToString();
                txtBillNo.Tag = dtPurchase.Rows[0]["Uid"].ToString();
                DtpBillDate.Text = dtPurchase.Rows[0]["BillDate"].ToString();
                txtBillTo.Text = dtPurchase.Rows[0]["Name"].ToString();
                txtBillTo.Tag = dtPurchase.Rows[0]["CustomerUid"].ToString();
                txtBillAddress.Text = dtPurchase.Rows[0]["Address1"].ToString() + "," + dtPurchase.Rows[0]["Address2"].ToString() + "," + dtPurchase.Rows[0]["City"].ToString() + " State :" + dtPurchase.Rows[0]["Ste"].ToString();
                TotaltaxableValue.Text = dtPurchase.Rows[0]["TaxableValue"].ToString();
                txtTotalTaxValue.Text = dtPurchase.Rows[0]["TaxValue"].ToString();
                txtRoundedOff.Text = dtPurchase.Rows[0]["Roff"].ToString();
                txtNetValue.Text = dtPurchase.Rows[0]["NetValue"].ToString();

                DataGridItem.Rows.Clear();
                for (int i = 0; i < dtPurchaseList.Rows.Count; i++)
                {
                    int Index1 = DataGridItem.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridItem.Rows[Index1];
                    dataGridViewRow.Cells[0].Value = dtPurchaseList.Rows[i]["ItemCode"].ToString();
                    dataGridViewRow.Cells[1].Value = dtPurchaseList.Rows[i]["ItemName"].ToString();
                    dataGridViewRow.Cells[2].Value = dtPurchaseList.Rows[i]["Uom"].ToString();
                    dataGridViewRow.Cells[3].Value = dtPurchaseList.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[4].Value = dtPurchaseList.Rows[i]["MRPRate"].ToString();
                    dataGridViewRow.Cells[5].Value = dtPurchaseList.Rows[i]["TotalValue"].ToString();
                    dataGridViewRow.Cells[6].Value = dtPurchaseList.Rows[i]["Uid"].ToString();
                }

                DataGridTax.Rows.Clear();
                for (int i = 0; i < dtPurchasetax.Rows.Count; i++)
                {
                    int Index2 = DataGridTax.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridTax.Rows[Index2];
                    dataGridViewRow.Cells[0].Value = dtPurchasetax.Rows[i]["TxableValue"].ToString();
                    dataGridViewRow.Cells[1].Value = dtPurchasetax.Rows[i]["TaxPer"].ToString();
                    dataGridViewRow.Cells[2].Value = dtPurchasetax.Rows[i]["TaxValue"].ToString();
                    dataGridViewRow.Cells[3].Value = dtPurchasetax.Rows[i]["Uid"].ToString();

                }
                decimal totalBasicValue = 0;
                decimal TaxValue = 0;
                for (int i = 0; i < dtPurchasetax.Rows.Count; i++)
                {
                    totalBasicValue += Convert.ToDecimal(dtPurchasetax.Rows[i]["TxableValue"].ToString());
                    TaxValue += Convert.ToDecimal(dtPurchasetax.Rows[i]["TaxValue"].ToString());
                }
                TotaltaxableValue.Text = totalBasicValue.ToString("0.00");
                txtTotalTaxValue.Text = TaxValue.ToString("0.00");
                LoadAmountDetails();
                SelectId = 0;
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadFrontGrid()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Uid","0")
                };
                DataTable dataTable = db.GetData(CommandType.StoredProcedure, "Proc_EditPurchase", sqlParameters);
                DataGridPurchase.DataSource = null;
                DataGridPurchase.AutoGenerateColumns = false;
                DataGridPurchase.ColumnCount = 5;

                DataGridPurchase.Columns[0].Name = "Uid";
                DataGridPurchase.Columns[0].HeaderText = "Uid";
                DataGridPurchase.Columns[0].DataPropertyName = "Uid";
                DataGridPurchase.Columns[0].Visible = false;

                DataGridPurchase.Columns[1].Name = "BillNumber";
                DataGridPurchase.Columns[1].HeaderText = "Bill Number";
                DataGridPurchase.Columns[1].DataPropertyName = "BillNumber";
                DataGridPurchase.Columns[1].Width = 120;

                DataGridPurchase.Columns[2].Name = "BillDate";
                DataGridPurchase.Columns[2].HeaderText = "Bill Date";
                DataGridPurchase.Columns[2].DataPropertyName = "BillDate";

                DataGridPurchase.Columns[3].Name = "Name";
                DataGridPurchase.Columns[3].HeaderText = "Name";
                DataGridPurchase.Columns[3].DataPropertyName = "Name";
                DataGridPurchase.Columns[3].Width = 400;

                DataGridPurchase.Columns[4].Name = "NetValue";
                DataGridPurchase.Columns[4].HeaderText = "Net Value";
                DataGridPurchase.Columns[4].DataPropertyName = "NetValue";
                DataGridPurchase.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridPurchase.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtTaxPercentage_SelectedIndexChanged(object sender, EventArgs e)
        {
            TxtTaxPercentage_TextChanged(sender, e);
        }
    }
}
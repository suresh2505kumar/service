﻿namespace Service
{
    partial class FrmItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grBack = new System.Windows.Forms.GroupBox();
            this.txttax = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chckActive = new System.Windows.Forms.CheckBox();
            this.grItemGroup = new System.Windows.Forms.GroupBox();
            this.btnGridClose = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridItemGrp = new System.Windows.Forms.DataGridView();
            this.txtItemGroup = new System.Windows.Forms.TextBox();
            this.cmbUom = new System.Windows.Forms.ComboBox();
            this.txtHsnCode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPartNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtItemCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panAdd = new System.Windows.Forms.Panel();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridItem = new System.Windows.Forms.DataGridView();
            this.grBack.SuspendLayout();
            this.grItemGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItemGrp)).BeginInit();
            this.panAdd.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItem)).BeginInit();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.txttax);
            this.grBack.Controls.Add(this.label30);
            this.grBack.Controls.Add(this.txtPrice);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.chckActive);
            this.grBack.Controls.Add(this.grItemGroup);
            this.grBack.Controls.Add(this.txtItemGroup);
            this.grBack.Controls.Add(this.cmbUom);
            this.grBack.Controls.Add(this.txtHsnCode);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.txtPartName);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.txtPartNo);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.txtItemName);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.txtItemCode);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(11, 3);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(546, 355);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // txttax
            // 
            this.txttax.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txttax.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.txttax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txttax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttax.FormattingEnabled = true;
            this.txttax.Location = new System.Drawing.Point(371, 211);
            this.txttax.Name = "txttax";
            this.txttax.Size = new System.Drawing.Size(143, 26);
            this.txttax.TabIndex = 152;
            this.txttax.SelectedIndexChanged += new System.EventHandler(this.txttax_SelectedIndexChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(339, 215);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(32, 21);
            this.label30.TabIndex = 151;
            this.label30.Text = "Tax";
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(385, 289);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(128, 26);
            this.txtPrice.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(340, 293);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 18);
            this.label8.TabIndex = 27;
            this.label8.Text = "Price";
            // 
            // chckActive
            // 
            this.chckActive.AutoSize = true;
            this.chckActive.Location = new System.Drawing.Point(149, 326);
            this.chckActive.Name = "chckActive";
            this.chckActive.Size = new System.Drawing.Size(65, 22);
            this.chckActive.TabIndex = 25;
            this.chckActive.Text = "Active";
            this.chckActive.UseVisualStyleBackColor = true;
            // 
            // grItemGroup
            // 
            this.grItemGroup.Controls.Add(this.btnGridClose);
            this.grItemGroup.Controls.Add(this.btnSelect);
            this.grItemGroup.Controls.Add(this.DataGridItemGrp);
            this.grItemGroup.Location = new System.Drawing.Point(149, 23);
            this.grItemGroup.Name = "grItemGroup";
            this.grItemGroup.Size = new System.Drawing.Size(286, 182);
            this.grItemGroup.TabIndex = 24;
            this.grItemGroup.TabStop = false;
            this.grItemGroup.Visible = false;
            // 
            // btnGridClose
            // 
            this.btnGridClose.BackColor = System.Drawing.Color.White;
            this.btnGridClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGridClose.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnGridClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGridClose.Location = new System.Drawing.Point(205, 195);
            this.btnGridClose.Name = "btnGridClose";
            this.btnGridClose.Size = new System.Drawing.Size(73, 28);
            this.btnGridClose.TabIndex = 7;
            this.btnGridClose.Text = "Close";
            this.btnGridClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGridClose.UseVisualStyleBackColor = false;
            this.btnGridClose.Click += new System.EventHandler(this.btnGridClose_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.Color.White;
            this.btnSelect.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSelect.Image = global::Service.Properties.Resources.ok1;
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelect.Location = new System.Drawing.Point(132, 195);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(73, 28);
            this.btnSelect.TabIndex = 6;
            this.btnSelect.Text = "Select";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // DataGridItemGrp
            // 
            this.DataGridItemGrp.AllowUserToAddRows = false;
            this.DataGridItemGrp.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridItemGrp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridItemGrp.Location = new System.Drawing.Point(3, 16);
            this.DataGridItemGrp.Name = "DataGridItemGrp";
            this.DataGridItemGrp.ReadOnly = true;
            this.DataGridItemGrp.RowHeadersVisible = false;
            this.DataGridItemGrp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridItemGrp.Size = new System.Drawing.Size(277, 177);
            this.DataGridItemGrp.TabIndex = 0;
            this.DataGridItemGrp.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridItemGrp_CellMouseDoubleClick);
            // 
            // txtItemGroup
            // 
            this.txtItemGroup.Location = new System.Drawing.Point(149, 251);
            this.txtItemGroup.Name = "txtItemGroup";
            this.txtItemGroup.Size = new System.Drawing.Size(365, 26);
            this.txtItemGroup.TabIndex = 5;
            this.txtItemGroup.Click += new System.EventHandler(this.txtItemGroup_Click);
            this.txtItemGroup.Enter += new System.EventHandler(this.txtItemGroup_Enter);
            this.txtItemGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemGroup_KeyDown);
            // 
            // cmbUom
            // 
            this.cmbUom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUom.FormattingEnabled = true;
            this.cmbUom.Items.AddRange(new object[] {
            "Kg",
            "Nos",
            "Pcs",
            "Ltrs"});
            this.cmbUom.Location = new System.Drawing.Point(149, 211);
            this.cmbUom.Name = "cmbUom";
            this.cmbUom.Size = new System.Drawing.Size(184, 26);
            this.cmbUom.TabIndex = 4;
            // 
            // txtHsnCode
            // 
            this.txtHsnCode.Location = new System.Drawing.Point(149, 289);
            this.txtHsnCode.Name = "txtHsnCode";
            this.txtHsnCode.Size = new System.Drawing.Size(184, 26);
            this.txtHsnCode.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(62, 293);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 18);
            this.label7.TabIndex = 10;
            this.label7.Text = "HSN Code";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(53, 255);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 18);
            this.label6.TabIndex = 9;
            this.label6.Text = "Item Group";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(94, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Uom";
            // 
            // txtPartName
            // 
            this.txtPartName.Location = new System.Drawing.Point(149, 171);
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.Size = new System.Drawing.Size(365, 26);
            this.txtPartName.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Part Name";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtPartNo
            // 
            this.txtPartNo.Location = new System.Drawing.Point(149, 127);
            this.txtPartNo.Name = "txtPartNo";
            this.txtPartNo.Size = new System.Drawing.Size(184, 26);
            this.txtPartNo.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(77, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Quantity";
            // 
            // txtItemName
            // 
            this.txtItemName.Location = new System.Drawing.Point(149, 82);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(365, 26);
            this.txtItemName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Item Name";
            // 
            // txtItemCode
            // 
            this.txtItemCode.Location = new System.Drawing.Point(149, 36);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Size = new System.Drawing.Size(184, 26);
            this.txtItemCode.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Item code";
            // 
            // panAdd
            // 
            this.panAdd.BackColor = System.Drawing.Color.White;
            this.panAdd.Controls.Add(this.btnEdit);
            this.panAdd.Controls.Add(this.btnAdd);
            this.panAdd.Controls.Add(this.btnExit);
            this.panAdd.Controls.Add(this.btnBack);
            this.panAdd.Controls.Add(this.btnDelete);
            this.panAdd.Controls.Add(this.btnSave);
            this.panAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panAdd.Location = new System.Drawing.Point(11, 361);
            this.panAdd.Name = "panAdd";
            this.panAdd.Size = new System.Drawing.Size(554, 37);
            this.panAdd.TabIndex = 2;
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEdit.Image = global::Service.Properties.Resources.if_edit_notes_46798;
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(310, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(74, 31);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.Image = global::Service.Properties.Resources.if_add_46776;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(235, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(74, 31);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(467, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(77, 31);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Image = global::Service.Properties.Resources.if_delete_46795;
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(467, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(77, 31);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "Back";
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelete.Image = global::Service.Properties.Resources.if_trash_46839___Copy;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(385, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(81, 31);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Image = global::Service.Properties.Resources.if_save_46830__1_;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(385, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 31);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DataGridItem);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(11, 3);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(554, 355);
            this.grFront.TabIndex = 14;
            this.grFront.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(6, 15);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(534, 26);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // DataGridItem
            // 
            this.DataGridItem.AllowUserToAddRows = false;
            this.DataGridItem.BackgroundColor = System.Drawing.Color.White;
            this.DataGridItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridItem.Location = new System.Drawing.Point(6, 44);
            this.DataGridItem.Name = "DataGridItem";
            this.DataGridItem.ReadOnly = true;
            this.DataGridItem.RowHeadersVisible = false;
            this.DataGridItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridItem.Size = new System.Drawing.Size(534, 306);
            this.DataGridItem.TabIndex = 0;
            // 
            // FrmItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(569, 403);
            this.Controls.Add(this.panAdd);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.grFront);
            this.Name = "FrmItem";
            this.Text = "Item";
            this.Load += new System.EventHandler(this.FrmItem_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grItemGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItemGrp)).EndInit();
            this.panAdd.ResumeLayout(false);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtItemCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPartNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPartName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtHsnCode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbUom;
        private System.Windows.Forms.TextBox txtItemGroup;
        private System.Windows.Forms.Panel panAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView DataGridItem;
        private System.Windows.Forms.GroupBox grItemGroup;
        private System.Windows.Forms.Button btnGridClose;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridItemGrp;
        private System.Windows.Forms.CheckBox chckActive;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox txttax;
        private System.Windows.Forms.Label label30;
    }
}
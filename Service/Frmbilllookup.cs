﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace Service
{
    public partial class Frmbilllookup : Form
    {
        FrmBill f2 = new FrmBill();
        public Frmbilllookup()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");

        SqlCommand qur = new SqlCommand();
        private void Frmbilllookup_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP3.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP3.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP4.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP4.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            tabC.TabPages.Remove(tabPage2);
            tabC.Visible = true;
            panit.Visible = false;
            Genclass.canclclik = false;
            this.CenterToScreen();
            Hfgp.RowHeadersVisible = false;

            HFGP2.RowHeadersVisible = false;
            HFGP3.RowHeadersVisible = false;
            HFGP4.RowHeadersVisible = false;
            Genclass.Module.buttonstyleform(this);

            if (Genclass.data1 == 3)
            {
                txtscr1.Width = 218;
                txtscr2.Width = 404;
                txtscr9.Width = 92;
                txtscr5.Width = 270;
                txtscr6.Width = 399;
                txtscr7.Width = 270;
                txtscr8.Width = 399;
            }
            else
            {
                txtscr1.Width = 404;
                txtscr2.Width = 220;
                txtscr2.Location = new Point(410, 7);
                txtscr9.Width = 92;
                txtscr5.Width = 399;
                txtscr6.Width = 270;
                txtscr6.Location = new Point(405, 15);
                txtscr7.Width = 399;
                txtscr8.Width = 270;
                txtscr8.Location = new Point(405, 15);
            }


        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Genclass.canclclik = true;
            this.Dispose();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            Genclass.ty = 1;



            FrmBill master = (FrmBill)Application.OpenForms["FrmBill"];
            master.button7.PerformClick();

            this.Dispose();


        }
        private void txtinp()
        {
            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Genclass.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {

                        if (ccontrol is Panel)
                        {

                            foreach (Control c in Genclass.Parent.Controls)
                            {
                                if (c is TextBox)
                                {

                                    if (c.Name == (string)Genclass.fieldone)
                                    {

                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[0].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldtwo)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[1].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldthree)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[2].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFour)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[3].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFive)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[4].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldSix)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[5].Value.ToString();
                                    }

                                }
                            }
                        }

                    }
                }

            }

            this.Dispose();
        }

        private void txtinp1()
        {
            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Genclass.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {

                        if (ccontrol is Panel)
                        {

                            foreach (Control c in Genclass.Parent.Controls)
                            {
                                if (c is TextBox)
                                {

                                    if (c.Name == (string)Genclass.fieldgrnone)
                                    {

                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[0].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldgrntwo)
                                    {
                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[1].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldgrnthree)
                                    {
                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[2].Value.ToString();
                                    }
                                    //else if (c.Name == Genclass.fieldFour)
                                    //{
                                    //    int i = HFGP4.SelectedCells[0].RowIndex;
                                    //    c.Text = HFGP4.Rows[i].Cells[3].Value.ToString();
                                    //}
                                    //else if (c.Name == Genclass.fieldFive)
                                    //{
                                    //    int i = HFGP4.SelectedCells[0].RowIndex;
                                    //    c.Text = HFGP4.Rows[i].Cells[4].Value.ToString();
                                    //}
                                    //else if (c.Name == Genclass.fieldSix)
                                    //{
                                    //    int i = HFGP4.SelectedCells[0].RowIndex;
                                    //    c.Text = HFGP4.Rows[i].Cells[5].Value.ToString();
                                    //}
                                }
                            }
                        }

                    }
                }

            }

            this.Dispose();
        }

        private void txtinp2()
        {
            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Genclass.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {

                        if (ccontrol is Panel)
                        {

                            foreach (Control c in Genclass.Parent.Controls)
                            {
                                if (c is TextBox)
                                {

                                    if (c.Name == (string)Genclass.fieldone)
                                    {

                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[0].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldtwo)
                                    {
                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[1].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldthree)
                                    {
                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[2].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldeight)
                                    {
                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[3].Value.ToString();
                                    }
                                    //else if (c.Name == Genclass.fieldFive)
                                    //{
                                    //    int i = HFGP2.SelectedCells[0].RowIndex;
                                    //    c.Text = HFGP2.Rows[i].Cells[4].Value.ToString();
                                    //}

                                }
                            }
                        }

                    }
                }

            }

            this.Dispose();
        }

        private void txtinp3()
        {
            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Genclass.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {

                        if (ccontrol is Panel)
                        {

                            foreach (Control c in Genclass.Parent.Controls)
                            {
                                if (c is TextBox)
                                {

                                    if (c.Name == (string)Genclass.fieldone)
                                    {

                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[0].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldtwo)
                                    {
                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[1].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldthree)
                                    {
                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[2].Value.ToString();
                                    }
                                    //else if (c.Name == Genclass.fieldFour)
                                    //{
                                    //    int i = HFGP3.SelectedCells[0].RowIndex;
                                    //    c.Text = HFGP3.Rows[i].Cells[3].Value.ToString();
                                    //}
                                    //else if (c.Name == Genclass.fieldFive)
                                    //{
                                    //    int i = HFGP3.SelectedCells[0].RowIndex;
                                    //    c.Text = HFGP3.Rows[i].Cells[4].Value.ToString();
                                    //}

                                }
                            }
                        }

                    }
                }

            }

            this.Dispose();
        }

        private void lookupload()
        {
            try
            {

                //conn.Open();


                Genclass.strsqltmp = Genclass.strsql3;

                Genclass.StrSrch = "";
                Genclass.StrSrch1 = "";





                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr6 + " like '%" + txtscr7.Text + "%'";
                    Genclass.StrSrch1 = Genclass.FSSQLSortStr7 + " like '%" + txtscr8.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr6 + " like '%" + txtscr7.Text + "%'";
                    Genclass.StrSrch1 = Genclass.StrSrch1 + " and " + Genclass.FSSQLSortStr7 + " like '%" + txtscr8.Text + "%'";
                }





                if (txtscr7.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr8.Text != "")
                {
                    Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                }

                Genclass.strsqltmp = Genclass.strsqltmp + " and " + Genclass.StrSrch + " and " + Genclass.StrSrch1 + " ";
                Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                this.HFGP3.DefaultCellStyle.Font = new Font("Calibri", 10);
                this.HFGP3.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                HFGP3.AutoGenerateColumns = false;
                HFGP3.DataSource = null;
                HFGP3.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP3.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP3.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                HFGP3.DataSource = tap;
                //txtscr7.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void lookupload1()
        {
            try
            {

                //conn.Open();


                Genclass.strsqltmp = Genclass.strsql;
                //Genclass.FSSQLSortStrtmp = Genclass.FSSQLSortStr;
                //Genclass.FSSQLSortStrtmp1 = Genclass.FSSQLSortStr1;
                //Genclass.FSSQLSortStrtmp2 = Genclass.FSSQLSortStr2;
                Genclass.StrSrch = "";
                Genclass.StrSrch1 = "";
                Genclass.StrSrch2 = "";




                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    Genclass.StrSrch1 = Genclass.FSSQLSortStr1 + " like '%" + txtscr2.Text + "%'";
                    Genclass.StrSrch2 = Genclass.FSSQLSortStr2 + " like '%" + txtscr9.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    Genclass.StrSrch1 = Genclass.StrSrch1 + " and " + Genclass.FSSQLSortStr1 + " like '%" + txtscr2.Text + "%'";
                    Genclass.StrSrch2 = Genclass.StrSrch2 + " and " + Genclass.FSSQLSortStr2 + " like '%" + txtscr9.Text + "%'";
                }




                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr2.Text != "")
                {
                    Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                }
                else
                {
                    Genclass.StrSrch2 = " " + Genclass.StrSrch2;
                }




                Genclass.strsqltmp = Genclass.strsqltmp + " where " + Genclass.StrSrch + " and " + Genclass.StrSrch1 + " and " + Genclass.StrSrch2 + " order by " + Genclass.FSSQLSortStr + "";
                Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                this.Hfgp.DefaultCellStyle.Font = new Font("Calibri", 10);
                this.Hfgp.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                Hfgp.AutoGenerateColumns = false;
                Hfgp.DataSource = null;
                Hfgp.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    Hfgp.Columns[Genclass.i].Name = column.ColumnName;
                    Hfgp.Columns[Genclass.i].HeaderText = column.ColumnName;
                    Hfgp.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                Hfgp.DataSource = tap;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }
        private void lookupload2()
        {
            try
            {

                //conn.Open();



                Genclass.strsqltmp = Genclass.strsql;

                Genclass.StrSrch = "";
                Genclass.StrSrch1 = "";





                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr3.Text + "%'";
                    Genclass.StrSrch1 = Genclass.FSSQLSortStr1 + " like '%" + txtscr4.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr3.Text + "%'";
                    Genclass.StrSrch1 = Genclass.StrSrch1 + " and " + Genclass.FSSQLSortStr1 + " like '%" + txtscr4.Text + "%'";
                }


                if (txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }

                else
                {
                    Genclass.StrSrch1 = " " + Genclass.StrSrch2;
                }



                Genclass.strsqltmp = Genclass.strsqltmp + " where " + Genclass.StrSrch + " and " + Genclass.StrSrch1 + " and " + Genclass.StrSrch2 + " order by " + Genclass.FSSQLSortStr + "";
                Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);




                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                this.HFGP4.DefaultCellStyle.Font = new Font("Calibri", 10);
                this.HFGP4.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                HFGP4.AutoGenerateColumns = false;
                HFGP4.DataSource = null;
                HFGP4.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP4.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP4.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP4.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                HFGP4.DataSource = tap;
                //txtscr3.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void lookupload3()
        {
            try
            {

                //conn.Open();


                Genclass.strsqltmp = Genclass.strsql2;

                Genclass.StrSrch = "";
                Genclass.StrSrch1 = "";





                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    Genclass.StrSrch1 = Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    Genclass.StrSrch1 = Genclass.StrSrch1 + " and " + Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                }





                if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                }



                Genclass.strsqltmp = Genclass.strsqltmp + " and " + Genclass.StrSrch + " and " + Genclass.StrSrch1 + "  order by " + Genclass.FSSQLSortStr5 + "";
                Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                this.HFGP2.DefaultCellStyle.Font = new Font("Calibri", 10);
                this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                HFGP2.AutoGenerateColumns = false;
                HFGP2.DataSource = null;
                HFGP2.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP2.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP2.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP2.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                HFGP2.DataSource = tap;
                //txtscr5.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        //private void HFGP3_CellEnter(object sender, DataGridViewCellEventArgs e)
        //{
        //    txtscr7.Focus();
        //}




        private void tabPage3_Click(object sender, EventArgs e)
        {
            txtscr5.Focus();
        }

        private void tabPage4_Click(object sender, EventArgs e)
        {
            txtscr7.Focus();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtinp1();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtinp2();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            txtinp3();
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            txtinp1();
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            Genclass.ty3 = 4;
            txtinp3();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Genclass.ty2 = 3;
            txtinp2();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            Genclass.ty1 = 2;
            txtinp1();
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            this.Dispose();
        }





        //private void Hfgp_CellEnter(object sender, DataGridViewCellEventArgs e)
        //{
        //    txtscr1.Focus();
        //}

        //private void HFGP4_CellEnter(object sender, DataGridViewCellEventArgs e)
        //{
        //    txtscr3.Focus();
        //}

        private void txtscr1_TextChanged_1(object sender, EventArgs e)
        {
            lookupload1();
        }

        private void txtscr2_TextChanged_1(object sender, EventArgs e)
        {
            lookupload1();
        }

        private void txtscr3_TextChanged_1(object sender, EventArgs e)
        {
            lookupload2();
        }

        private void txtscr4_TextChanged_1(object sender, EventArgs e)
        {
            lookupload2();
        }

        private void txtscr5_TextChanged_1(object sender, EventArgs e)
        {
            lookupload3();
        }

        private void txtscr6_TextChanged_1(object sender, EventArgs e)
        {
            lookupload3();
        }

        private void txtscr7_TextChanged_1(object sender, EventArgs e)
        {
            lookupload();
        }

        private void txtscr8_TextChanged_1(object sender, EventArgs e)
        {
            lookupload();
        }

        private void button6_Click_2(object sender, EventArgs e)
        {
            Genclass.canclclik = true;
            this.Dispose();
        }

        private void button4_Click_2(object sender, EventArgs e)
        {
            Genclass.canclclik = true;
            this.Dispose();
        }

        private void button2_Click_3(object sender, EventArgs e)
        {
            Genclass.canclclik = true;
            this.Dispose();
        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            Genclass.ty = 2;
            FrmBill master = (FrmBill)Application.OpenForms["FrmBill"];
            master.button7.PerformClick();
            //txtinp1();
        }

        private void button3_Click_2(object sender, EventArgs e)
        {
            Genclass.ty = 3;
            FrmBill master = (FrmBill)Application.OpenForms["FrmBill"];
            master.button7.PerformClick();

            this.Dispose();

        }

        private void button5_Click_2(object sender, EventArgs e)
        {

            Genclass.ty = 4;
            FrmBill master = (FrmBill)Application.OpenForms["FrmBill"];
            master.button7.PerformClick();

            this.Dispose();

        }
        private void loaduom()
        {
            Genclass.okclick = false;
            conn.Close();
            conn.Open();

            if (Genclass.type == 2)
            {
                Genclass.Module.Partylistviewcont("uid", "ItemGroup", Genclass.strsql, this, txtgrpid, txtgrp, panit);
                Genclass.strsql = "select uid,ItemGroup from ItemGroup where active=1 and companyid=" + Genclass.data1 + "";
                Genclass.FSSQLSortStr = "ItemGroup";
            }

            else if (Genclass.type == 1)
            {
                Genclass.Module.Partylistviewcont("uid", "UoM", Genclass.strsql, this, txtuomid, txtuom, panit);
                Genclass.strsql = "select uid,Generalname as UoM from Generalm where active=1 and typem_uid=1 and companyid=" + Genclass.data1 + "";
                Genclass.FSSQLSortStr = "Generalname";
            }


            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            dt.Columns[1].Width = 530;


            dt.DefaultCellStyle.Font = new Font("Calibri", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;
            conn.Close();
            contc.Show();


        }
        private void button7_Click(object sender, EventArgs e)
        {
            tabC.Visible = false;
            panit.Visible = true;
            hfgt.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            hfgt.EnableHeadersVisualStyles = false;
            hfgt.DefaultCellStyle.Font = new Font("Calibri", 10);
            hfgt.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
            hfgt.RowHeadersVisible = false;
            txtitemname.Text = "";
            Txtitcode.Text = "";
            txtuom.Text = "";
            txtgrp.Text = "";
            txtuom.ReadOnly = true;
            txtgrp.ReadOnly = true;
            hfgt.Refresh();
            hfgt.DataSource = null;
            hfgt.Rows.Clear();
            //Titlep();
        }
        private void Titlep()
        {
            //hfgt.ColumnCount = 2;

            hfgt.Columns[0].Name = "Itemcode";
            hfgt.Columns[1].Name = "Itemname";

            hfgt.Columns[0].Width = 190;
            hfgt.Columns[1].Width = 600;

        }



        private void txtgrp_Click(object sender, EventArgs e)
        {
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";

            Genclass.type = 2;
            loaduom();

        }

        private void txtuom_Click(object sender, EventArgs e)
        {
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";
            Genclass.type = 1;
            loaduom();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            tabC.Visible = true;
            panit.Visible = false;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (txtitemname.Text == "" || txtuom.Text == "" || txtgrp.Text == "")
            {
                MessageBox.Show("Fill all the entries to save");
                return;
            }
            conn.Open();
            qur.CommandText = "Insert into Itemm values ('" + Txtitcode.Text + "','" + txtitemname.Text + "','" + Txtitcode.Text + "','" + txtitemname.Text + "',''," + txtuomid.Text + ",0,0,1,0," + txtgrpid.Text + "," + Genclass.data1 + ",0)";
            qur.ExecuteNonQuery();
            conn.Close();
            MessageBox.Show("Record Saved");
            panit.Visible = false;
            tabC.Visible = true;
            f2.fo = txtitemname.Text;
            this.Dispose();
        }

        private void Txtitcode_TextChanged(object sender, EventArgs e)
        {
            if (Txtitcode.Text != "")
            {
                string quy = "Select ITEMCODE,ITEMNAME from itemm where companyid=" + Genclass.data1 + " and itemcode like  '" + Txtitcode.Text + "%'";
                Genclass.cmd = new SqlCommand(quy, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                hfgt.Refresh();
                hfgt.DataSource = null;
                hfgt.Rows.Clear();
                //hfgt.ColumnCount = 0;
                hfgt.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                hfgt.EnableHeadersVisualStyles = false;
                hfgt.DefaultCellStyle.Font = new Font("Calibri", 10);
                hfgt.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                hfgt.RowHeadersVisible = false;

                hfgt.DataSource = tap;
                Titlep();
            }
            else
            {
                hfgt.Refresh();
                hfgt.DataSource = null;
                hfgt.Rows.Clear();
                //hfgt.ColumnCount = 0;
                hfgt.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                hfgt.EnableHeadersVisualStyles = false;
                hfgt.DefaultCellStyle.Font = new Font("Calibri", 10);
                hfgt.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                hfgt.RowHeadersVisible = false;
            }
        }

        private void txtitemname_TextChanged(object sender, EventArgs e)
        {
            if (txtitemname.Text != "")
            {
                string quy = "Select ITEMCODE,ITEMNAME from itemm where companyid=" + Genclass.data1 + " and itemname like  '" + txtitemname.Text + "%'";
                Genclass.cmd = new SqlCommand(quy, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                hfgt.Refresh();
                hfgt.DataSource = null;
                hfgt.Rows.Clear();
                //hfgt.ColumnCount = 0;
                hfgt.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                hfgt.EnableHeadersVisualStyles = false;
                hfgt.DefaultCellStyle.Font = new Font("Calibri", 10);
                hfgt.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                hfgt.RowHeadersVisible = false;
                hfgt.DataSource = tap;
                Titlep();
            }
            else
            {
                hfgt.Refresh();
                hfgt.DataSource = null;
                hfgt.Rows.Clear();
                //hfgt.ColumnCount = 0;
                hfgt.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                hfgt.EnableHeadersVisualStyles = false;
                hfgt.DefaultCellStyle.Font = new Font("Calibri", 10);
                hfgt.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                hfgt.RowHeadersVisible = false;
            }

        }

        private void Hfgp_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (Hfgp.CurrentRow.Cells[0].Value.ToString() != "")
            {
                DataGridViewCell cell = Hfgp.CurrentRow.Cells[7];
                Hfgp.CurrentCell = cell;
                Hfgp.BeginEdit(true);

            }
        }

        private void Hfgp_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (Hfgp.Rows[0].Cells[1].Value != null)
            {
                conn.Open();
                if (Hfgp.CurrentCell.ColumnIndex == 7 && Hfgp.CurrentRow.Cells[7].Value.ToString() != "0")
                {
                    if (Hfgp.CurrentRow.Cells[5].Value.ToString() != "0")
                    {
                        Hfgp.CurrentRow.Cells[7].Style.BackColor = Color.LawnGreen;
                        if (Convert.ToInt64(Hfgp.CurrentRow.Cells[7].Value) > Convert.ToInt64(Hfgp.CurrentRow.Cells[5].Value))
                        {
                            MessageBox.Show("Qty Exeeds");
                            Hfgp.CurrentRow.Cells[7].Value = 0;
                            return;
                        }
                        else
                        {
                            if (Genclass.data1 == 1)
                            {
                                qur.CommandText = "delete from PgBatmp where refid=" + Hfgp.CurrentRow.Cells[4].Value + "";
                                qur.ExecuteNonQuery();
                                //qur.CommandText = "insert into PgBatmp select f.itemname,gg.generalname,b.prate,isnull(" + Hfgp.CurrentRow.Cells[7].Value + ",0)-isnull(" + Hfgp.CurrentRow.Cells[7].Value + ",0),convert(decimal(18,2),b.prate*(isnull(" + Hfgp.CurrentRow.Cells[7].Value + ",0)-isnull(" + Hfgp.CurrentRow.Cells[7].Value + ",0)),102) as basicvalue,'" + Hfgp.CurrentRow.Cells[3].Value + "',f.uid,case pp.stateuid when 24 then convert(decimal(18,2),j.sname/2,102) else j.sname end as sname,case pp.stateuid when 24 then convert(decimal(18,2),((b.prate*(isnull(b.PQty,0)-isnull(d.PQty,0)))/100*j.sname)/2,102) else (b.prate*(isnull(b.PQty,0)-isnull(d.PQty,0)))/100*j.sname end  as gstval,'" + Hfgp.CurrentRow.Cells[6].Value + "',b.uid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 90  left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=100   left join itemm f on b.ItemUId=f.Uid left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join partym pp on a.partyuid=PP.uid where b.uid=2317 group  by  a.uid,a.transp,b.pqty,d.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,gg.GeneralName,b.PRate,j.Sname,pp.stateuid";
                                qur.CommandText = "insert into PgBatmp select distinct f.itemname,gg.generalname,b.prate," + Hfgp.CurrentRow.Cells[7].Value + ",convert(decimal(18,2),b.prate*" + Hfgp.CurrentRow.Cells[7].Value + ",102) as basicvalue,'" + Hfgp.CurrentRow.Cells[3].Value + "',f.uid,hh.f1,(b.prate*" + Hfgp.CurrentRow.Cells[7].Value + ")/100*hh.f1 as gstval,'" + Hfgp.CurrentRow.Cells[6].Value + "',b.uid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 90  left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=100   left join itemm f on b.ItemUId=f.Uid left join generalm gg on f.UOM_UId=gg.Uid left join generalm hh on f.tax=hh.uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join partym pp on a.partyuid=PP.uid  where b.uid=" + Hfgp.CurrentRow.Cells[4].Value + " group  by  a.uid,a.transp,b.pqty,d.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,gg.GeneralName,b.PRate,j.Sname,pp.stateuid,hh.f1";

                                qur.ExecuteNonQuery();
                                qur.CommandText = "insert into Batemp select distinct f.itemname,gg.generalname," + Hfgp.CurrentRow.Cells[7].Value + "," + Hfgp.CurrentRow.Cells[7].Value + ", 0 ,'',f.uid,0, 0,'',f.uid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 90  left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=100   left join itemm f on b.ItemUId=f.Uid left join generalm gg on f.UOM_UId=gg.Uid left join generalm hh on f.tax=hh.uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join partym pp on a.partyuid=PP.uid  where b.uid=" + Hfgp.CurrentRow.Cells[4].Value + " group  by  a.uid,a.transp,b.pqty,d.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,gg.GeneralName,b.PRate,j.Sname,pp.stateuid,hh.f1";

                                qur.ExecuteNonQuery();
                            }
                            else
                            {
                                qur.CommandText = "delete from Batmp where refid=" + Hfgp.CurrentRow.Cells[4].Value + "";
                                qur.ExecuteNonQuery(); ;
                                qur.CommandText = "insert into Batmp select distinct f.itemname,gg.generalname,b.prate," + Hfgp.CurrentRow.Cells[7].Value + ",convert(decimal(18,2),b.prate*" + Hfgp.CurrentRow.Cells[7].Value + ",102) as basicvalue,'" + Hfgp.CurrentRow.Cells[3].Value + "',f.uid,hh.f1,(b.prate*" + Hfgp.CurrentRow.Cells[7].Value + ")/100*hh.f1 as gstval,'" + Hfgp.CurrentRow.Cells[6].Value + "',b.uid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 90  left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=100   left join itemm f on b.ItemUId=f.Uid left join generalm gg on f.UOM_UId=gg.Uid left join generalm hh on f.tax=hh.uid  left join ItemGroup j on f.itemgroup_Uid=j.UId left join partym pp on a.partyuid=PP.uid where b.uid=" + Hfgp.CurrentRow.Cells[4].Value + " group  by  a.uid,a.transp,b.pqty,d.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,gg.GeneralName,b.PRate,j.Sname,pp.stateuid,hh.f1";
                                //qur.CommandText = "insert into Batmp select distinct itemid,0,itemname,uom,listid," + Hfgp.CurrentRow.Cells[7].Value + ",PRate,convert(decimal(18,2)," + Hfgp.CurrentRow.Cells[7].Value + "*PRate,102),0,0,convert(decimal(18,2)," + Hfgp.CurrentRow.Cells[7].Value + "*PRate,102),f1,convert(decimal(18,2),((" + Hfgp.CurrentRow.Cells[7].Value + "*PRate)/100)*f1,102),0,Pono,Podate from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(d.PQty,0) as qty,b.PRate,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid,gg.GeneralName as uom,j.Sname as f1 from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 90    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=100   left join itemm f on b.ItemUId=f.Uid left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join ordreqqty o on a.uid=o.tpuid where b.uid=" + Hfgp.CurrentRow.Cells[4].Value + " group  by  a.uid,a.transp,b.pqty,d.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,gg.GeneralName,b.PRate,j.Sname having isnull(b.PQty,0)-isnull(d.PQty,0) >0) aa inner join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=" + Genclass.data1 + " left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab ";
                                qur.ExecuteNonQuery();
                            }
                        }
                    }
                }
                else
                {
                    if (Genclass.data1 == 1)
                    {
                        qur.CommandText = "delete from PgBatmp where refid=" + Hfgp.CurrentRow.Cells[4].Value + "";
                        qur.ExecuteNonQuery();
                    }
                    else
                    {
                        qur.CommandText = "delete from PgBatmp where refid=" + Hfgp.CurrentRow.Cells[4].Value + "";
                        qur.ExecuteNonQuery();
                    }

                }
                conn.Close();
            }
        }

        private void HFGP2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (HFGP2.CurrentRow.Cells[0].Value.ToString() != "")
            {
                DataGridViewCell cell = HFGP2.CurrentRow.Cells[4];
                HFGP2.CurrentCell = cell;
                HFGP2.BeginEdit(true);

            }
        }

        private void HFGP3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (HFGP3.CurrentRow.Cells[1].Value.ToString() != "" && HFGP3.CurrentCell.ColumnIndex == 3)
            {
                DataGridViewCell cell = HFGP3.CurrentRow.Cells[3];
                HFGP3.CurrentCell = cell;
                HFGP3.BeginEdit(true);

            }
            if (HFGP3.CurrentRow.Cells[1].Value.ToString() != "" && HFGP3.CurrentCell.ColumnIndex == 4)
            {
                DataGridViewCell cell = HFGP3.CurrentRow.Cells[4];
                HFGP3.CurrentCell = cell;
                HFGP3.BeginEdit(true);

            }
        }

        private void HFGP2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (HFGP2.Rows[0].Cells[1].Value != null)
            {
                conn.Open();
                if (HFGP2.CurrentCell.ColumnIndex == 4 && HFGP2.CurrentRow.Cells[4].Value.ToString() != "0.0")
                {
                    if (HFGP2.CurrentRow.Cells[4].Value.ToString() != "0")
                    {
                        HFGP2.CurrentRow.Cells[4].Style.BackColor = Color.LawnGreen;

                        if (Genclass.data1 == 1)
                        {
                            qur.CommandText = "delete from PgBatmp where itid=" + HFGP2.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();
                            //qur.CommandText = "insert into PgBatmp select f.itemname,gg.generalname," + HFGP2.CurrentRow.Cells[3].Value + "," + HFGP2.CurrentRow.Cells[4].Value + ",convert(decimal(18,2)," + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ",102) as basicvalue,'',f.uid,case " + Genclass.cat + " when 24 then convert(decimal(18,2),j.sname/2,102) else j.sname end as sname,case " + Genclass.cat + " when 24 then convert(decimal(18,2),(((" + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ")/100)*j.sname)/2,102) else convert(decimal(18,2),((" + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ")/100)*j.sname,102) end  as gstval,'',0  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId  where f.uid=" + HFGP2.CurrentRow.Cells[0].Value + "";
                            qur.CommandText = "insert into PgBatmp select f.itemname,gg.generalname," + HFGP2.CurrentRow.Cells[3].Value + "," + HFGP2.CurrentRow.Cells[4].Value + ",convert(decimal(18,2)," + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ",102) as basicvalue,'',f.uid, hh.f1, convert(decimal(18,2),((" + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ")/100)*hh.f1,102)   as gstval,'',0  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join generalm hh on f.tax=hh.uid  where f.uid=" + HFGP2.CurrentRow.Cells[0].Value + "";

                            qur.ExecuteNonQuery();

                            qur.CommandText = "insert into Batemp select f.itemname,gg.generalname," + HFGP2.CurrentRow.Cells[4].Value + "," + HFGP2.CurrentRow.Cells[4].Value + ", 0 ,'',f.uid,0, 0,'',f.uid  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join generalm hh on f.tax=hh.uid  where f.uid=" + HFGP2.CurrentRow.Cells[0].Value + "";

                            qur.ExecuteNonQuery();
                        }
                        else
                        {
                            qur.CommandText = "delete from Batmp where itid=" + HFGP2.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();
                            //qur.CommandText = "insert into Batmp select f.itemname,gg.generalname," + HFGP2.CurrentRow.Cells[3].Value + "," + HFGP2.CurrentRow.Cells[4].Value + ",convert(decimal(18,2)," + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ",102) as basicvalue,'',f.uid,case " + Genclass.cat + " when 24 then convert(decimal(18,2),j.sname/2,102) else j.sname end as sname,case " + Genclass.cat + " when 24 then convert(decimal(18,2),(((" + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ")/100)*j.sname)/2,102) else convert(decimal(18,2),((" + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ")/100)*j.sname,102) end  as gstval,'',0  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId  where f.uid=" + HFGP2.CurrentRow.Cells[0].Value + "";

                            qur.CommandText = "insert into Batmp select f.itemname,gg.generalname," + HFGP2.CurrentRow.Cells[3].Value + "," + HFGP2.CurrentRow.Cells[4].Value + ",convert(decimal(18,2)," + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ",102) as basicvalue,'',f.uid,hh.f1, convert(decimal(18,2),((" + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ")/100)*hh.f1,102)   as gstval,'',0  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join generalm hh on f.tax=hh.uid  where f.uid=" + HFGP2.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();
                        }

                    }
                }
                else
                {
                    if (Genclass.data1 == 1)
                    {
                        qur.CommandText = "delete from PgBatmp where itid=" + HFGP2.CurrentRow.Cells[0].Value + " and refid=0";
                        qur.ExecuteNonQuery();
                    }
                    else
                    {
                        qur.CommandText = "delete from Batmp where itid=" + HFGP2.CurrentRow.Cells[0].Value + " and refid=0";
                        qur.ExecuteNonQuery();
                    }

                }
                conn.Close();
            }
        }

        private void HFGP3_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (HFGP3.Rows[0].Cells[0].Value != null)
            {
                conn.Close();
                conn.Open();
                if (HFGP3.CurrentCell.ColumnIndex == 4 && HFGP3.CurrentRow.Cells[4].Value.ToString() != "0.0" || HFGP3.CurrentCell.ColumnIndex == 4 && HFGP3.CurrentRow.Cells[4].Value.ToString() != "0.0")
                {
                    if (HFGP3.CurrentRow.Cells[4].Value.ToString() != "0")
                    {
                        HFGP3.CurrentRow.Cells[4].Style.BackColor = Color.LawnGreen;
                        if (Genclass.data1 == 1)
                        {
                            qur.CommandText = "delete from PgBatmp where itid=" + HFGP3.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();
                            //qur.CommandText = "insert into PgBatmp select f.itemname,gg.generalname," + HFGP3.CurrentRow.Cells[3].Value + "," + HFGP3.CurrentRow.Cells[4].Value + ",convert(decimal(18,2)," + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ",102) as basicvalue,'',f.uid,case " + Genclass.cat + " when 24 then convert(decimal(18,2),j.sname/2,102) else j.sname end as sname,case " + Genclass.cat + " when 24 then convert(decimal(18,2),(((" + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ")/100)*j.sname)/2,102) else convert(decimal(18,2),((" + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ")/100)*j.sname,102) end  as gstval,'',0  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId  where f.uid=" + HFGP3.CurrentRow.Cells[0].Value + "";

                            qur.CommandText = "insert into PgBatmp select f.itemname,gg.generalname," + HFGP3.CurrentRow.Cells[3].Value + "," + HFGP3.CurrentRow.Cells[4].Value + ",convert(decimal(18,2)," + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ",102) as basicvalue,'',f.uid, isnull(hh.f1,0), convert(decimal(18,2),((" + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ")/100)*isnull(hh.f1,0),102)   as gstval,'',0  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join generalm hh on f.tax=hh.uid  where f.uid=" + HFGP3.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();

                            qur.CommandText = "insert into Batemp select f.itemname,gg.generalname," + HFGP3.CurrentRow.Cells[4].Value + "," + HFGP3.CurrentRow.Cells[4].Value + ", 0 ,'',f.uid,0, 0,'',f.uid  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join generalm hh on f.tax=hh.uid  where f.uid=" + HFGP3.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();
                        }
                        else
                        {
                            qur.CommandText = "delete from Batmp where itid=" + HFGP3.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();
                            //qur.CommandText = "insert into Batmp select f.itemname,gg.generalname," + HFGP3.CurrentRow.Cells[3].Value + "," + HFGP3.CurrentRow.Cells[4].Value + ",convert(decimal(18,2)," + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ",102) as basicvalue,'',f.uid,case " + Genclass.cat + " when 24 then convert(decimal(18,2),j.sname/2,102) else j.sname end as sname,case " + Genclass.cat + " when 24 then convert(decimal(18,2),(((" + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ")/100)*j.sname)/2,102) else convert(decimal(18,2),((" + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ")/100)*j.sname,102) end  as gstval,'',0  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId  where f.uid=" + HFGP3.CurrentRow.Cells[0].Value + "";

                            qur.CommandText = "insert into Batmp select f.itemname,gg.generalname," + HFGP3.CurrentRow.Cells[3].Value + "," + HFGP3.CurrentRow.Cells[4].Value + ",convert(decimal(18,2)," + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ",102) as basicvalue,'',f.uid, isnull(hh.f1,0), convert(decimal(18,2),((" + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ")/100)*isnull(hh.f1,0),102)  as gstval,'',0  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId  left join generalm hh on f.tax=hh.uid where f.uid=" + HFGP3.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();
                        }

                    }
                }
                else
                {
                    if (Genclass.data1 == 1)
                    {
                        qur.CommandText = "delete from PgBatmp where itid=" + HFGP3.CurrentRow.Cells[0].Value + " and refid=0";
                        qur.ExecuteNonQuery();
                    }
                    else
                    {
                        qur.CommandText = "delete from Batmp where itid=" + HFGP3.CurrentRow.Cells[0].Value + " and refid=0";
                        qur.ExecuteNonQuery();
                    }

                }
                conn.Close();
            }
        }

        private void HFGP3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFGP4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Hfgp_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtscr9_TextChanged(object sender, EventArgs e)
        {
            lookupload1();
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Service
{
    public partial class FrmGeneral : Form
    {
        public FrmGeneral()
        {
            InitializeComponent();
        }
        string MenuKey;
        SQLDBHelper db = new SQLDBHelper();
        private void FrmGeneral_Load(object sender, EventArgs e)
        {
            MenuKey = FrmMain.MenuKey;
            if (MenuKey == "1")
            {
                this.Text = "Model";
                lblName.Text = "Model Name";
                GetData();
            }
            else if (MenuKey == "2")
            {
                this.Text = "Accessories";
                lblName.Text = "Name";
                GetData();
            }
            else if (MenuKey == "7")
            {
                this.Text = "General Jobs";
                lblName.Text = "Description";
                GetData();
            }
            else if (MenuKey == "5")
            {
                this.Text = "Item Group";
                lblName.Text = "Item Group";
                GetData();
            }
            LoadButton(1);
        }

        private void LoadButton(int Id)
        {
            try
            {
                if (Id == 1) //Load
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                    btnSave.Text = "Save";
                }
                else if (Id == 2)//Add
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Save";
                }
                else if (Id == 3)//Edit
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                LoadButton(2);
                txtGeneralName.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool Active;
                if (chckActive.Checked == true)
                {
                    Active = true;
                }
                else
                {
                    Active = false;
                }
                if (btnSave.Text == "Save")
                {
                    SqlParameter[] para = {
                        new SqlParameter("@TypeM_Uid",MenuKey),
                        new SqlParameter("@GeneralName",txtGeneralName.Text),
                        new SqlParameter("@Active",Active),
                        new SqlParameter("@F1",DBNull.Value),
                        new SqlParameter("@F2",DBNull.Value),
                        new SqlParameter("@F3",DBNull.Value),
                        new SqlParameter("@CompanyID",1),
                        new SqlParameter("@Uid",0)
                    };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_GeneralM", para);
                    MessageBox.Show("Record has been Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    SqlParameter[] para = {
                        new SqlParameter("@TypeM_Uid",MenuKey),
                        new SqlParameter("@GeneralName",txtGeneralName.Text),
                        new SqlParameter("@Active",Active),
                        new SqlParameter("@F1",DBNull.Value),
                        new SqlParameter("@F2",DBNull.Value),
                        new SqlParameter("@F3",DBNull.Value),
                        new SqlParameter("@CompanyID",1),
                        new SqlParameter("@Uid",txtGeneralName.Tag)
                    };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_GeneralM", para);
                    MessageBox.Show("Record has been Updated Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                txtGeneralName.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void GetData()
        {
            try
            {
                SqlParameter[] para = {
                    new SqlParameter("@TypeM_Uid",MenuKey)
                };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralM", para);
                DataGridGeneralM.DataSource = null;
                DataGridGeneralM.AutoGenerateColumns = false;
                DataGridGeneralM.ColumnCount = 3;
                DataGridGeneralM.Columns[0].Name = "Uid";
                DataGridGeneralM.Columns[0].HeaderText = "Uid";
                DataGridGeneralM.Columns[0].DataPropertyName = "Uid";
                DataGridGeneralM.Columns[0].Visible = false;
                DataGridGeneralM.Columns[1].Name = "Name";
                DataGridGeneralM.Columns[1].HeaderText = "Name";
                DataGridGeneralM.Columns[1].DataPropertyName = "GeneralName";
                DataGridGeneralM.Columns[1].Width = 475;
                DataGridGeneralM.Columns[2].Name = "Active";
                DataGridGeneralM.Columns[2].HeaderText = "Active";
                DataGridGeneralM.Columns[2].DataPropertyName = "Active";
                DataGridGeneralM.Columns[2].Visible = false;
                DataGridGeneralM.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridGeneralM.SelectedCells[0].RowIndex;
                txtGeneralName.Text = DataGridGeneralM.Rows[Index].Cells[1].Value.ToString();
                txtGeneralName.Tag = DataGridGeneralM.Rows[Index].Cells[0].Value.ToString();
                bool active = Convert.ToBoolean(DataGridGeneralM.Rows[Index].Cells[2].Value.ToString());
                if (active == true)
                {
                    chckActive.Checked = true;
                }
                else
                {
                    chckActive.Checked = false;
                }
                LoadButton(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                LoadButton(1);
                GetData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridGeneralM.SelectedCells[0].RowIndex;
                int Tag = Convert.ToInt32(DataGridGeneralM.Rows[Index].Cells[0].Value.ToString());
                int Meny = Convert.ToInt32(MenuKey);
                SqlParameter[] para = {
                        new SqlParameter("@Active",false),
                        new SqlParameter("@Uid",Tag)
                    };
                db.ExecuteQuery(CommandType.StoredProcedure, "SP_UpdateGeneralM", para);
                MessageBox.Show("Record has been Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}

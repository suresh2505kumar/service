﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;



namespace Service
{
    public partial class FrmMec : Form
    {
        public FrmMec()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;
        //SqlCommand cmd;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        private void FrmMec_Load(object sender, EventArgs e)
        {
            Left = (MdiParent.ClientRectangle.Width - Width) / 4;
            Top = (MdiParent.ClientRectangle.Height - Height) / 4;
            //label1.Location = new Point(60, 50);
            qur.Connection = conn;


            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            HFGP.RowHeadersVisible = false;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            Genpan.Visible = true;
            EditPnl.Visible = false;
            Loadgrid();


        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();

                Genclass.StrSrch = "";

             
                    Genclass.FSSQLSortStr = "Mechanic Name";
                

                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }


                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else

                {
                 
                        Genclass.StrSrch = "uid <> 0";
                    }
                




             
                    string quy = "select uid,MechanicName ,Code ,active from mechanicmas  where " + Genclass.StrSrch + " and active=1";
                    Genclass.cmd = new SqlCommand(quy, conn);

                

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);




                this.HFGP.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 11, FontStyle.Bold);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 250;
                HFGP.Columns[3].Visible = false;
                HFGP.Columns[2].Width = 200;


                HFGP.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
         
            EditPnl.Visible = true;
            txtgen.Focus();
       
        }

        private void butnsave_Click(object sender, EventArgs e)
        {
            if (txtgen.Text == "")
            {
                MessageBox.Show("Enter the Machine Name");
                txtgen.Focus();
                return;

            }

            if (txtmachinecopde.Text == "")
            {
                MessageBox.Show("Enter the Machine code");
                txtmachinecopde.Focus();
                return;

            }

            conn.Open();
            
            if (mode == 1)
            {
             
                    qur.CommandText = "insert into mechanicmas (mechanicName,code,active) values ('" + txtgen.Text + "','" + txtmachinecopde.Text + "',1)";
                

                qur.ExecuteNonQuery();
                MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            }


            else if (mode == 2)
            {


                

                    qur.CommandText = "Update mechanicmas set mechanicName='" + txtgen.Text + "',code='" + txtmachinecopde.Text + "'  where uid=" + uid + "";
                    qur.ExecuteNonQuery();
                

                MessageBox.Show("Record has been Updated", "Save", MessageBoxButtons.OK);
            }




         


            conn.Close();
            Loadgrid();
            EditPnl.Visible = false;
            Genpan.Visible = true;
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            txtgen.Text = "";
            txtmachinecopde.Text = "";
            EditPnl.Visible = false;
         
            Genpan.Visible = true;
            Loadgrid();
        }

        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            Genpan.Visible = false;
            EditPnl.Visible = true;
      
            int i = HFGP.SelectedCells[0].RowIndex;
            txtgen.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            txtmachinecopde.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            
        }

        private void button21_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            int i = HFGP.SelectedCells[0].RowIndex;
        
          
                qur.CommandText = "Update mechanicmas set active=0 where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            

            qur.ExecuteNonQuery();
            MessageBox.Show("Record has been Deactivated", "Delete", MessageBoxButtons.OK);


            Loadgrid();
        }
    }
}

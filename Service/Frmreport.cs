﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
//using System.IO.FileSystemInfo;
//using System.IO.DirectoryInfo;
//using excel = Microsoft.Office.Interop.Excel;

using Excel = Microsoft.Office.Interop.Excel;
using ExcelAutoFormat = Microsoft.Office.Interop.Excel.XlRangeAutoFormat;

namespace Service
{
    public partial class Frmreport : Form
    {
        public Frmreport()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();


        private static Microsoft.Office.Interop.Excel.Workbook mWorkBook;
        private static Microsoft.Office.Interop.Excel.Sheets mWorkSheets;
        private static Microsoft.Office.Interop.Excel.Worksheet mWSheet1;
        private static Microsoft.Office.Interop.Excel.Application oXL;
        private object workbook;

        private void buttnext_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Frmreport_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            //cbotype.SelectedIndex = -1;
            cbotype.Text = "Sales Register";
            dtpfrom.Value = DateTime.Now;
            dtpto.Value = DateTime.Now;
        }

        private void buttnshow_Click(object sender, EventArgs e)
        {
            if (cbotype.Text == "JobCard Register")
            {
                conn.Close();
                conn.Open();

                Genclass.slno = 1;
                Genclass.Dtype = 116;
                textBox1.Text = dtpfrom.Text;
                textBox2.Text = dtpto.Text;
                Genclass.rrtt1 = textBox1.Text;
                Genclass.err1 = textBox2.Text;
                GenParameters.ReportType = "JobCard Register";
                FrmReportViewver crv = new FrmReportViewver();
                crv.Show();
                conn.Close();

            }

        }
    }
}
    


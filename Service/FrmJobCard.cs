﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using System.Configuration;



namespace Service
{
    public partial class FrmJobCard : Form
    {
        public FrmJobCard()
        {
            InitializeComponent();
        }
        int mode = 0;
        int AUid = 0;
 
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        int LastJobNo;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);

        SqlCommand qur = new SqlCommand();
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmJobCard_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

            this.DataGridDescription.DefaultCellStyle.Font = new Font("calibri", 10);
            this.DataGridDescription.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            DataGridDescription.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DataGridDescription.EnableHeadersVisualStyles = false;
            DataGridDescription.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            DataGridDescription.RowHeadersVisible = false;


            this.dataGridServiceAdvice.DefaultCellStyle.Font = new Font("calibri", 10);
            this.dataGridServiceAdvice.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            dataGridServiceAdvice.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridServiceAdvice.EnableHeadersVisualStyles = false;
            dataGridServiceAdvice.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            dataGridServiceAdvice.RowHeadersVisible = false;



            this.dataGridAccessories.DefaultCellStyle.Font = new Font("calibri", 10);
            this.dataGridAccessories.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            dataGridAccessories.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridAccessories.EnableHeadersVisualStyles = false;
            dataGridAccessories.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            dataGridAccessories.RowHeadersVisible = false;
            LoadAutoComplete();
            GetData();
            LoadAccGrid();
            LoadComplients();
            LoadServiceAdvice();
            GetGeneralJobsData();
            LoadButton(1);
            //txtJobCardNo.Text = GetLastJobNo();
            txtRegNo.Focus();
            LoadGetJobCard(1);
            //Loadgrid();
            Loadmech();
        }
        private void LoadAutoComplete()
        {
            AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
            SqlParameter[] para = { new SqlParameter("@Uid", AUid) };
            DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetCustM", para);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string RegNo = dt.Rows[i]["RegNo"].ToString();
                    coll.Add(RegNo);
                }
            }
            txtRegNo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtRegNo.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtRegNo.AutoCompleteCustomSource = coll;
        }

        private void Loadmech()
        {
            AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
            SqlParameter[] para = { new SqlParameter("@Uid", AUid) };
            DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_mech", para);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string RegNo = dt.Rows[i]["mechanicname"].ToString();
                    coll.Add(RegNo);
                }
            }
            txtmech.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtmech.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtmech.AutoCompleteCustomSource = coll;
        }
        private void txtRegNo_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtRegNo.Text != string.Empty)
                {
                    SqlParameter[] para = { new SqlParameter("@RegNo", txtRegNo.Text) };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetCustMByRegNo", para);
                    txtModel.Text = dt.Rows[0]["GeneralName"].ToString();
                    txtCustomer.Text = dt.Rows[0]["Name"].ToString();
                    txtMobile.Text = dt.Rows[0]["Mobile"].ToString();
                    txtCustomer.Tag = dt.Rows[0]["Uid"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCustomer cus = new FrmCustomer();
                cus.MdiParent = this.MdiParent;
                cus.StartPosition = FormStartPosition.CenterScreen;
                cus.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadAccGrid()
        {
            dataGridAccessories.DataSource = null;
            dataGridAccessories.AutoGenerateColumns = false;
   
            dataGridAccessories.ColumnCount = 4;
            dataGridAccessories.Columns[0].Name = "uid";
            dataGridAccessories.Columns[0].HeaderText = "uid";
            dataGridAccessories.Columns[0].Visible = false;
            dataGridAccessories.Columns[1].Name = "SlNo";
            dataGridAccessories.Columns[1].HeaderText = "SlNo";
            dataGridAccessories.Columns[1].Width = 65;
            dataGridAccessories.Columns[2].Name = "Accessseries";
            dataGridAccessories.Columns[2].HeaderText = "Accessseries";
            dataGridAccessories.Columns[2].Width = 350;
            dataGridAccessories.Columns[3].Name = "JlUid";
            dataGridAccessories.Columns[3].HeaderText = "JlUid";
            dataGridAccessories.Columns[3].Visible = false;

      

        }
    

        protected void LoadComplients()
        {
            DataGridDescription.DataSource = null;
            DataGridDescription.AutoGenerateColumns = false;
       
            DataGridDescription.ColumnCount = 4;
            DataGridDescription.Columns[0].Name = "uid";
            DataGridDescription.Columns[0].HeaderText = "uid";
            DataGridDescription.Columns[0].Visible = false;
            DataGridDescription.Columns[1].Name = "SlNo";
            DataGridDescription.Columns[1].HeaderText = "SlNo";
            DataGridDescription.Columns[1].Width = 65;
            DataGridDescription.Columns[2].Name = "Complients";
            DataGridDescription.Columns[2].HeaderText = "Complients";
            DataGridDescription.Columns[2].Width = 350;
            DataGridDescription.Columns[3].Name = "JlUid";
            DataGridDescription.Columns[3].HeaderText = "JlUid";
            DataGridDescription.Columns[3].Visible = false;

        }

        protected void LoadServiceAdvice()
        {
            dataGridServiceAdvice.DataSource = null;
            dataGridServiceAdvice.AutoGenerateColumns = false;
         
            dataGridServiceAdvice.ColumnCount = 4;
            dataGridServiceAdvice.Columns[0].Name = "uid";
            dataGridServiceAdvice.Columns[0].HeaderText = "uid";
            dataGridServiceAdvice.Columns[0].Visible = false;
            dataGridServiceAdvice.Columns[1].Name = "SlNo";
            dataGridServiceAdvice.Columns[1].HeaderText = "SlNo";
            dataGridServiceAdvice.Columns[1].Width = 65;
            dataGridServiceAdvice.Columns[2].Name = "Service";
            dataGridServiceAdvice.Columns[2].HeaderText = "Service";
            dataGridServiceAdvice.Columns[2].Width = 350;
            dataGridServiceAdvice.Columns[3].Name = "JlUid";
            dataGridServiceAdvice.Columns[3].HeaderText = "JlUid";
            dataGridServiceAdvice.Columns[3].Visible = false;
            
        }

        private string GetLastJobNo()
        {
            string JobCardNo = string.Empty;
            try
            {
                SqlParameter[] para = { new SqlParameter("@DocType", "JobCard") };
                DataTable dt = new DataTable();
                dt = db.GetData(CommandType.StoredProcedure, "SP_GetDocLastNo", para);
                if (dt.Rows.Count != 0)
                {
                    LastJobNo = ((Convert.ToInt32(dt.Rows[0]["LastSNo"].ToString() + 1)));
                    JobCardNo = dt.Rows[0]["PreFix"].ToString() + "/" + LastJobNo.ToString("D5");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return JobCardNo;
        }

        private void GetData()
        {
            try
            {
                AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
                int MenuKey = 2;
                SqlParameter[] para = {
                    new SqlParameter("@TypeM_Uid",MenuKey)
                };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralM", para);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string RegNo = dt.Rows[i]["GeneralName"].ToString();
                        coll.Add(RegNo);
                    }
                }
                txtAcc.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtAcc.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtAcc.AutoCompleteCustomSource = coll;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetGeneralJobsData()
        {
            try
            {
                AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
                int MenuKey = 3;
                SqlParameter[] para = {
                    new SqlParameter("@TypeM_Uid",MenuKey)
                };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralM", para);
                cmbTypeOfJob.DataSource = null;
                cmbTypeOfJob.DisplayMember = "GeneralName";
                cmbTypeOfJob.ValueMember = "Uid";
                cmbTypeOfJob.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadButton(int Id)
        {
            try
            {
                if (Id == 1) //Load
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                    btnSave.Text = "Save";
                }
                else if (Id == 2)//Add
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Save";
                }
                else if (Id == 3)//Edit
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            try
            {
                //txtJobCardNo.Text = GetLastJobNo();
                ClearControl();
                dataGridAccessories.Rows.Clear();
                DataGridDescription.Rows.Clear();
                dataGridServiceAdvice.Rows.Clear();
                conn.Close();
                conn.Open();
                qur.CommandText = "truncate table comptemp ";
                qur.ExecuteNonQuery();

                qur.CommandText = "truncate table sertemp ";
                qur.ExecuteNonQuery();
                LoadButton(2);
                 mode = 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            LoadButton(1);
            ClearControl();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCustComp.Text != string.Empty)
                {
                    conn.Close();
                    conn.Open();
                    qur.CommandText = "insert into comptemp values ('"+ txtCustComp.Text + "')";
                    qur.ExecuteNonQuery();

 
                    Genclass.strsql = "select comp from comptemp ";




                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);



                    for (int i = 0; i < tap1.Rows.Count; i++)
                    {

                        var index = DataGridDescription.Rows.Add();
                        DataGridDescription.Rows[index].Cells[0].Value = 0;
                        if (DataGridDescription.Rows.Count == 1)
                        {
                            DataGridDescription.Rows[index].Cells[1].Value = DataGridDescription.Rows.Count -1;
                        }
                        else
                        {
                            DataGridDescription.Rows[index].Cells[1].Value = DataGridDescription.Rows.Count - 1;

                        }
                        DataGridDescription.Rows[index].Cells[2].Value = tap1.Rows[i]["comp"].ToString();

                        DataGridDescription.Rows[index].Cells[3].Value = 0;

                    }
                }
                conn.Close();
                conn.Open();
                qur.CommandText = "truncate table comptemp ";
                qur.ExecuteNonQuery();

                txtCustComp.Text = string.Empty;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtServiceAdvise.Text != string.Empty)
                {
                    conn.Close();
                    conn.Open();
                    qur.CommandText = "insert into sertemp values ('" + txtServiceAdvise.Text + "')";
                    qur.ExecuteNonQuery();

                    Genclass.strsql = "select ser from sertemp ";

              


                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);



                    for (int i = 0; i < tap1.Rows.Count; i++)
                    {

                        var index = dataGridServiceAdvice.Rows.Add();
                        dataGridServiceAdvice.Rows[index].Cells[0].Value = 0;
                        if (dataGridServiceAdvice.Rows.Count == 1)
                        {
                            dataGridServiceAdvice.Rows[index].Cells[1].Value = dataGridServiceAdvice.Rows.Count - 1;
                        }
                        else
                        {
                            dataGridServiceAdvice.Rows[index].Cells[1].Value = dataGridServiceAdvice.Rows.Count - 1;

                        }
                        dataGridServiceAdvice.Rows[index].Cells[2].Value = tap1.Rows[i]["ser"].ToString();

                        dataGridServiceAdvice.Rows[index].Cells[3].Value = 0;

                    }
                }
                conn.Close();
                conn.Open();
                qur.CommandText = "truncate table sertemp ";
                qur.ExecuteNonQuery();
                txtServiceAdvise.Text = string.Empty;
            }

                 
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnPOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtAcc.Text != string.Empty)
                {
                    SqlParameter[] para = {
                        new SqlParameter("@GeneralName",txtAcc.Text),
                        new SqlParameter("@TypeM_Uid",2)
                    };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralMByName", para);

                    Genclass.strsql = "select generalname as access from Generalm  where  uid="+ dt.Rows[0]["Uid"].ToString() +"  ";




                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);



                    for (int i = 0; i < tap1.Rows.Count; i++)
                    {

                        var index = dataGridAccessories.Rows.Add();
                        dataGridAccessories.Rows[index].Cells[0].Value = 0;
                        if (dataGridAccessories.Rows.Count == 1)
                        {
                            dataGridAccessories.Rows[index].Cells[1].Value = dataGridAccessories.Rows.Count - 1;
                        }
                        else
                        {
                            dataGridAccessories.Rows[index].Cells[1].Value = dataGridAccessories.Rows.Count - 1;

                        }
                        dataGridAccessories.Rows[index].Cells[2].Value = tap1.Rows[i]["access"].ToString();

                        dataGridAccessories.Rows[index].Cells[3].Value = dt.Rows[0]["Uid"].ToString();



                        //int Index = dataGridAccessories.Rows.Add();
                        //DataGridViewRow row = dataGridAccessories.Rows[Index];
                        //if (dataGridAccessories.Rows.Count == 1)
                        //{
                        //    row.Cells[1].Value = dataGridAccessories.Rows.Count;
                        //}
                        //else
                        //{
                        //    row.Cells[1].Value = dataGridAccessories.Rows.Count;
                        //}
                        //row.Cells[2].Value = txtAcc.Text;
                        //row.Cells[0].Value = 0;
                        //row.Cells[3].Value = dt.Rows[0]["Uid"].ToString();
                        txtAcc.Text = string.Empty;
                    }
                }
                }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int Uid = 0;
                if(mode==2)
                {

                 
                        conn.Close();
                        conn.Open();
                        qur.CommandText = "delete from jobcardlist where juid=" + Genclass.h + "";
                        qur.ExecuteNonQuery();
                    
                }

                if (mode==1)
                {
                    SqlParameter[] para = {
                              new SqlParameter("@JUid","0"),
                        new SqlParameter("@JobCardDate",Convert.ToDateTime(dtpJobDate.Text)),
                        new SqlParameter("@JobCardNo","JC/"),
                        new SqlParameter("@RegNo",txtRegNo.Text),
                        new SqlParameter("@Km",txtKm.Text),
                        new SqlParameter("@TypeofJob",cmbTypeOfJob.SelectedValue),
                        new SqlParameter("@FuelLevel",cmbFuelLevel.Text),
                        new SqlParameter("@LeftSide",txtLeftSide.Text),
                        new SqlParameter("@RightSide",txtRightSide.Text),
                        new SqlParameter("@DeliveryDate",Convert.ToDateTime(dtpdeleiveryDate.Text)),
                        new SqlParameter("@DeleiveryTime",cmbDTime.Text),
                        new SqlParameter("@CustUId",txtCustomer.Tag),
                        new SqlParameter("@companyID","1"),
                  
                        new SqlParameter("@YearId","1"),
                        new SqlParameter("@Eoil",Convert.ToDecimal(txtOil.Text)),
                        new SqlParameter("@ESpares",Convert.ToDecimal(txtSpares.Text)),
                        new SqlParameter("@ELabour",Convert.ToDecimal(txtlabour.Text)),
                         new SqlParameter("@CreatedDate",Convert.ToDateTime(dtpcr.Text)),
                         //new SqlParameter("@CreatedDate",Convert.ToDateTime(dtpmod.Text)),
                         new SqlParameter("@UserID","1"),
                         new SqlParameter("@SOD",Convert.ToDateTime(dtpsod.Text)),
                              new SqlParameter("@MechanicID",Convert.ToInt16(txtmech.Tag)),
                    };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "Prc_JC_InsertJobCard", para);

                    SqlParameter[] para1 = {
                        new SqlParameter("@CUSTID",txtCustomer.Tag),
                        new SqlParameter("@chaseno",txtframe.Text),
                        new SqlParameter("@ENGINENO",txtengine.Text),
                        new SqlParameter("@COLOUR",txtcolor.Text),

                    };
                    db.GetData(CommandType.StoredProcedure, "RPTDETAILS", para1);

                    if (dt.Rows.Count == 0)
                    {
                        MessageBox.Show("Jobcard add failed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                    {
                        Uid = Convert.ToInt32(dt.Rows[0]["Juid"].ToString());
                    }
                    for (int i = 0; i < DataGridDescription.Rows.Count - 1; i++)
                    {
                        SqlParameter[] paraDesc = {
                            new SqlParameter("@JUid",Uid),
                            new SqlParameter("@SlNo",DataGridDescription.Rows[i].Cells[1].Value),
                            new SqlParameter("@JobType","8"),
                            new SqlParameter("@Description",DataGridDescription.Rows[i].Cells[2].Value),
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "Prc_JC_InsertJobCardDetail", paraDesc);
                    }

                    for (int i = 0; i < dataGridAccessories.Rows.Count - 1; i++)
                    {
                        SqlParameter[] paraAcc = {
                            new SqlParameter("@JUid",Uid),
                            new SqlParameter("@SlNo",dataGridAccessories.Rows[i].Cells[1].Value),
                            new SqlParameter("@JobType","2"),
                            new SqlParameter("@Description",dataGridAccessories.Rows[i].Cells[2].Value),
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "Prc_JC_InsertJobCardDetail", paraAcc);
                    }

                    for (int i = 0; i < dataGridServiceAdvice.Rows.Count - 1; i++)
                    {
                        SqlParameter[] paraAcc = {
                            new SqlParameter("@JUid",Uid),
                            new SqlParameter("@SlNo",dataGridServiceAdvice.Rows[i].Cells[1].Value),
                            new SqlParameter("@JobType","9"),
                            new SqlParameter("@Description",dataGridServiceAdvice.Rows[i].Cells[2].Value),
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "Prc_JC_InsertJobCardDetail", paraAcc);
                    }
                    MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {



                    SqlParameter[] para = {
                        new SqlParameter("@JobCardDate",Convert.ToDateTime(dtpJobDate.Text)),
                        new SqlParameter("@JobCardNo",txtJobCardNo.Text),
                        new SqlParameter("@RegNo",txtRegNo.Text),
                        new SqlParameter("@Km",txtKm.Text),
                        new SqlParameter("@TypeofJob",cmbTypeOfJob.SelectedValue),
                        new SqlParameter("@FuelLevel",cmbFuelLevel.Text),
                        new SqlParameter("@LeftSide",txtLeftSide.Text),
                        new SqlParameter("@RightSide",txtRightSide.Text),
                        new SqlParameter("@DeliveryDate",Convert.ToDateTime(dtpdeleiveryDate.Text)),
                        new SqlParameter("@DeliveryTime",cmbDTime.Text),
                        new SqlParameter("@CustId",txtCustomer.Tag),
                        new SqlParameter("@Juid", Genclass.h),
                   
                        new SqlParameter("@YearId","1"),
                        new SqlParameter("@Eoil",Convert.ToDecimal(txtOil.Text)),
                        new SqlParameter("@ESpares",Convert.ToDecimal(txtSpares.Text)),
                        new SqlParameter("@ELabour",Convert.ToDecimal(txtlabour.Text)),
                        new SqlParameter("@JCStatus","Booked"),
                        new SqlParameter("@CreatedDate",Convert.ToDateTime(dtpcr.Text)),
                         new SqlParameter("@ModifiedDate",Convert.ToDateTime(dtpmod.Text)),
                         new SqlParameter("@UserID","1"),
                         new SqlParameter("@SOD",Convert.ToDateTime(dtpsod.Text)),
                        new SqlParameter("@MechanicID",Convert.ToInt16(txtmech.Tag)),
                    };
               
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_JobCard", para);


                    SqlParameter[] para1 = {
                        new SqlParameter("@CUSTID",txtCustomer.Tag),
                        new SqlParameter("@chaseno",txtframe.Text),
                        new SqlParameter("@ENGINENO",txtengine.Text),
                        new SqlParameter("@COLOUR",txtcolor.Text),

                    };
                    db.GetData(CommandType.StoredProcedure, "RPTDETAILS", para1);
                    for (int i = 0; i < DataGridDescription.Rows.Count - 1; i++)
                    {
                        SqlParameter[] paraDesc = {
                            new SqlParameter("@JUid", Genclass.h),
                            new SqlParameter("@SlNo",DataGridDescription.Rows[i].Cells[1].Value),
                            new SqlParameter("@JobType","8"),
                            new SqlParameter("@Description",DataGridDescription.Rows[i].Cells[2].Value),
                         
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "SP_JobCardList", paraDesc);
                    }

                    for (int i = 0; i < dataGridAccessories.Rows.Count - 1; i++)
                    {
                        SqlParameter[] paraAcc = {
                            new SqlParameter("@JUid", Genclass.h),
                            new SqlParameter("@SlNo",dataGridAccessories.Rows[i].Cells[1].Value),
                            new SqlParameter("@JobType","2"),
                            new SqlParameter("@Description",dataGridAccessories.Rows[i].Cells[2].Value),
                          
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "SP_JobCardList", paraAcc);
                    }

                    for (int i = 0; i < dataGridServiceAdvice.Rows.Count - 1; i++)
                    {
                        SqlParameter[] paraAcc = {
                            new SqlParameter("@JUid", Genclass.h),
                            new SqlParameter("@SlNo",dataGridServiceAdvice.Rows[i].Cells[1].Value),
                            new SqlParameter("@JobType","9"),
                            new SqlParameter("@Description",dataGridServiceAdvice.Rows[i].Cells[2].Value),
                        
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "SP_JobCardList", paraAcc);
                    }
                    MessageBox.Show("Record has been updated successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                ClearControl();
                LoadButton(1);
                LoadGetJobCard(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ClearControl()
        {
            txtRegNo.Text = string.Empty;
            txtCustComp.Text = string.Empty;
            txtCustomer.Text = string.Empty;
            txtModel.Text = string.Empty;
            txtMobile.Text = string.Empty;
            txtJobCardNo.Text = string.Empty;
            txtKm.Text = string.Empty;
            cmbTypeOfJob.SelectedIndex = -1;
            cmbFuelLevel.SelectedIndex = -1;
            txtAcc.Text = string.Empty;
            txtServiceAdvise.Text = string.Empty;
            txtOil.Text = "0";
            txtSpares.Text = "0";
            txtlabour.Text = "0";
            txtLeftSide.Text = string.Empty;
            txtRightSide.Text = string.Empty;
            txtcolor.Text = string.Empty;
            txtmech.Text = string.Empty;

            txtframe.Text = string.Empty;
            txtengine.Text = string.Empty;
            //dataGridAccessories.Rows.Clear();
            //DataGridDescription.Rows.Clear();
            //dataGridServiceAdvice.Rows.Clear();
        }


        protected DataTable LoadGetJobCard(int tag)
        {
            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@CompanyID","1"),
                    new SqlParameter("@Date",Convert.ToDateTime(dtpJobCardDate.Text))
                };
                dt = db.GetData(CommandType.StoredProcedure, "Prc_JC_GetJobCardFrintload", para);
                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                dataGridJobCard.DataSource = null;
                dataGridJobCard.AutoGenerateColumns = false;
                dataGridJobCard.ColumnCount = 9;
                dataGridJobCard.Columns[0].Name = "JUid";
                dataGridJobCard.Columns[0].HeaderText = "JUid";
                dataGridJobCard.Columns[0].DataPropertyName = "JUid";
                dataGridJobCard.Columns[0].Visible = false;
                dataGridJobCard.Columns[1].Name = "JobCardDate";
                dataGridJobCard.Columns[1].HeaderText = "Date";
                dataGridJobCard.Columns[1].DataPropertyName = "JobCardDate";

                dataGridJobCard.Columns[2].Name = "JobCardNo";
                dataGridJobCard.Columns[2].HeaderText = "JobCard No";
                dataGridJobCard.Columns[2].DataPropertyName = "JobCardNo";
                dataGridJobCard.Columns[2].Width = 110;
                dataGridJobCard.Columns[3].Name = "RegNo";
                dataGridJobCard.Columns[3].HeaderText = "Reg No";
                dataGridJobCard.Columns[3].DataPropertyName = "Regno";

                dataGridJobCard.Columns[4].Name = "Name";
                dataGridJobCard.Columns[4].HeaderText = "CustomerName";
                dataGridJobCard.Columns[4].DataPropertyName = "Name";
                dataGridJobCard.Columns[4].Width = 200;
                dataGridJobCard.Columns[5].Name = "Mobile";
                dataGridJobCard.Columns[5].HeaderText = "Mobile";
                dataGridJobCard.Columns[5].DataPropertyName = "Mobile";

                dataGridJobCard.Columns[6].Name = "GeneralName";
                dataGridJobCard.Columns[6].HeaderText = "Typeof Job";
                dataGridJobCard.Columns[6].DataPropertyName = "GeneralName";
                dataGridJobCard.Columns[6].Width = 200;
                dataGridJobCard.Columns[7].Name = "JCStatus";
                dataGridJobCard.Columns[7].HeaderText = "Status";
                dataGridJobCard.Columns[7].DataPropertyName = "JCStatus";

                dataGridJobCard.Columns[8].Name = "BillAmount";
                dataGridJobCard.Columns[8].HeaderText = "Bill Amount";
                dataGridJobCard.Columns[8].DataPropertyName = "BillAmount";
                bs.DataSource = dt;

                dataGridJobCard.DataSource = bs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();
      

                string quy = "Select	distinct JUid,JobCardDate,JobCardNo,JC.RegNo,Name as CustomerName,Mobile,N.GeneralName as Job,JC.JCStatus,'0' BillAmount from    JobCard JC left Join CustM CM with(nolock) on JC.CustUid = CM.Uid left Join CustBike CB with(nolock) on CB.RegNo = JC.RegNo and CM.Uid = CB.CustUid left Join GeneralM M with(nolock) on M.Uid = CB.BrandId and M.companyid = 1 left Join GeneralM N with(nolock) on N.Uid = JC.TypeOfJob and M.companyid = 1 Where JC.companyID = 1 order by jc.JUID desc"; 
                Genclass.cmd = new SqlCommand(quy, conn);


                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);




                dataGridJobCard.AutoGenerateColumns = false;
                dataGridJobCard.Refresh();
                dataGridJobCard.DataSource = null;
                dataGridJobCard.Rows.Clear();


                dataGridJobCard.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;

                foreach (DataColumn column in tap.Columns)
                {
                    dataGridJobCard.Columns[Genclass.i].Name = column.ColumnName;
                    dataGridJobCard.Columns[Genclass.i].HeaderText = column.ColumnName;
                    dataGridJobCard.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }
               
                dataGridJobCard.Columns[0].Name = "JUid";
                dataGridJobCard.Columns[0].HeaderText = "JUid";
                dataGridJobCard.Columns[0].DataPropertyName = "JUid";
                dataGridJobCard.Columns[0].Visible = false;
                dataGridJobCard.Columns[1].Name = "JobCardDate";
                dataGridJobCard.Columns[1].HeaderText = "Date";
                dataGridJobCard.Columns[1].DataPropertyName = "JobCardDate";

                dataGridJobCard.Columns[2].Name = "JobCardNo";
                dataGridJobCard.Columns[2].HeaderText = "JobCard No";
                dataGridJobCard.Columns[2].DataPropertyName = "JobCardNo";
                dataGridJobCard.Columns[2].Width = 110;
                dataGridJobCard.Columns[3].Name = "RegNo";
                dataGridJobCard.Columns[3].HeaderText = "Reg No";
                dataGridJobCard.Columns[3].DataPropertyName = "Regno";

                dataGridJobCard.Columns[4].Name = "CustomerName";
                dataGridJobCard.Columns[4].HeaderText = "Name";
                dataGridJobCard.Columns[4].DataPropertyName = "CustomerName";
                dataGridJobCard.Columns[4].Width = 200;
                dataGridJobCard.Columns[5].Name = "Mobile";
                dataGridJobCard.Columns[5].HeaderText = "Mobile";
                dataGridJobCard.Columns[5].DataPropertyName = "Mobile";

                dataGridJobCard.Columns[6].Name = "Job";
                dataGridJobCard.Columns[6].HeaderText = "Typeof Job";
                dataGridJobCard.Columns[6].DataPropertyName = "Job";
                dataGridJobCard.Columns[6].Width = 200;
                dataGridJobCard.Columns[7].Name = "JCStatus";
                dataGridJobCard.Columns[7].HeaderText = "Status";
                dataGridJobCard.Columns[7].DataPropertyName = "JCStatus";

                dataGridJobCard.Columns[8].Name = "BillAmount";
                dataGridJobCard.Columns[8].HeaderText = "Bill Amount";
                dataGridJobCard.Columns[8].DataPropertyName = "BillAmount";
                dataGridJobCard.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();

            }
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = dataGridJobCard.SelectedCells[0].RowIndex;
                GenParameters.ReportType = "JobCard";
                GenParameters.ReportId = Convert.ToInt32(dataGridJobCard.Rows[Index].Cells[0].Value);
                FrmReportViewver rpt = new FrmReportViewver();
                rpt.MdiParent = this.MdiParent;
                rpt.Show();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            mode = 2;
            ClearControl();

            conn.Close();
            conn.Open();
            qur.CommandText = "truncate table comptemp ";
            qur.ExecuteNonQuery();

            qur.CommandText = "truncate table sertemp ";
            qur.ExecuteNonQuery();

            grFront.Visible = false;
            grBack.Visible = true;

            DataGridDescription.AutoGenerateColumns = false;
            DataGridDescription.Refresh();
            DataGridDescription.DataSource = null;
            DataGridDescription.Rows.Clear();

            dataGridServiceAdvice.AutoGenerateColumns = false;
            dataGridServiceAdvice.Refresh();
            dataGridServiceAdvice.DataSource = null;
            dataGridServiceAdvice.Rows.Clear();
            dataGridAccessories.AutoGenerateColumns = false;
            dataGridAccessories.Refresh();
            dataGridAccessories.DataSource = null;
            dataGridAccessories.Rows.Clear();


            btnAdd.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            btnExit.Visible = false;
            btnSave.Visible = true;
            btnBack.Visible = true;
            btnSave.Text = "Update";
            int index = dataGridJobCard.SelectedCells[0].RowIndex;
           Genclass.h = Convert.ToInt32(dataGridJobCard.Rows[index].Cells[0].Value.ToString());


            SqlParameter[] para = { new SqlParameter("@juid", dataGridJobCard.Rows[index].Cells[0].Value) };

            DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_Editjobcard", para, conn);
            DataTable dtMaster = ds.Tables[0];
            DataTable dtdetails = ds.Tables[1];
            DataTable dtdet = ds.Tables[2];
            DataTable dtdet123 = ds.Tables[3];

            dtpJobCardDate.Text = dtMaster.Rows[0]["JobCardDate"].ToString();
            txtJobCardNo.Text = dtMaster.Rows[0]["JobCardNo"].ToString();
            txtRegNo.Text = dtMaster.Rows[0]["RegNo"].ToString();
            txtKm.Text = dtMaster.Rows[0]["Km"].ToString();

            cmbTypeOfJob.SelectedValue = dtMaster.Rows[0]["TypeOfJob"].ToString();
            cmbTypeOfJob.Text = dtMaster.Rows[0]["job"].ToString();

            cmbFuelLevel.Text = dtMaster.Rows[0]["FuelLevel"].ToString();
            txtLeftSide.Text = dtMaster.Rows[0]["LeftSide"].ToString();
            txtRightSide.Text = dtMaster.Rows[0]["RightSide"].ToString();
            dtpdeleiveryDate.Text = dtMaster.Rows[0]["DeliveryDate"].ToString();
            cmbDTime.Text = dtMaster.Rows[0]["DeleiveryTime"].ToString();
            txtCustomer.Tag = dtMaster.Rows[0]["CustUid"].ToString();

            txtCustomer.Text = dtMaster.Rows[0]["CustomerName"].ToString();
            txtMobile.Text = dtMaster.Rows[0]["Mobile"].ToString();
            txtModel.Tag = dtMaster.Rows[0]["BrandId"].ToString();
            txtModel.Text = dtMaster.Rows[0]["Model"].ToString();
            txtOil.Text = dtMaster.Rows[0]["Eoil"].ToString();
            txtSpares.Text = dtMaster.Rows[0]["ESpares"].ToString();
            txtlabour.Text = dtMaster.Rows[0]["ELabour"].ToString();


            dtpcr.Text = dtMaster.Rows[0]["CreatedDate"].ToString();
            dtpmod.Text = dtMaster.Rows[0]["modifieddate"].ToString();
            dtpsod.Text = dtMaster.Rows[0]["sod"].ToString();
            txtmech.Tag = dtMaster.Rows[0]["MechanicID"].ToString();
            txtmech.Text = dtMaster.Rows[0]["mechanicname"].ToString();

            txtengine.Text = dtMaster.Rows[0]["engineno"].ToString();
            txtframe.Text = dtMaster.Rows[0]["fno"].ToString();
            txtcolor.Text = dtMaster.Rows[0]["colour"].ToString();

            //for (int i = 0; i < dtdetails.Rows.Count; i++)
            //{
            //    DataGridViewRow Row = (DataGridViewRow)dataGridAccessories.Rows[0].Clone();
            //    Row.Cells[1].Value = dtdetails.Rows[i]["slno"].ToString();
            //    Row.Cells[2].Value = dtdetails.Rows[i]["Description"].ToString();
            //    Row.Cells[3].Value = dtdetails.Rows[i]["juid"].ToString();


            //    dataGridAccessories.Rows.Add(Row);

            //}
            //for (int i = 0; i < dtdet.Rows.Count; i++)
            //{
            //    DataGridViewRow Row = (DataGridViewRow)DataGridDescription.Rows[0].Clone();
            //    Row.Cells[1].Value = dtdet.Rows[i]["slno"].ToString();
            //    Row.Cells[2].Value = dtdet.Rows[i]["Description"].ToString();
            //    Row.Cells[3].Value = dtdet.Rows[i]["juid"].ToString();


            //    DataGridDescription.Rows.Add(Row);

            //}
            //for (int i = 0; i < dtdet123.Rows.Count; i++)
            //{
            //    DataGridViewRow Row = (DataGridViewRow)dataGridServiceAdvice.Rows[0].Clone();
            //    Row.Cells[1].Value = dtdet123.Rows[i]["slno"].ToString();
            //    Row.Cells[2].Value = dtdet123.Rows[i]["Description"].ToString();
            //    Row.Cells[3].Value = dtdet123.Rows[i]["juid"].ToString();
            //    dataGridServiceAdvice.Rows.Add(Row);

            //}

            loadspareedit();
            loadspareedit2();
            loadspareedit3();

        }
        private void loadspareedit()
        {
            Genclass.strsql = "SELECT jluid,slno,description as Accessseries,juid  FROM jobcardlist where  juid=" + Genclass.h + "  and jobtype=2  ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);


            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = dataGridAccessories.Rows.Add();

                dataGridAccessories.Rows[index].Cells[0].Value = tap1.Rows[k]["jluid"].ToString();
                dataGridAccessories.Rows[index].Cells[1].Value = tap1.Rows[k]["slno"].ToString();

                dataGridAccessories.Rows[index].Cells[2].Value = tap1.Rows[k]["Accessseries"].ToString();

                dataGridAccessories.Rows[index].Cells[3].Value = tap1.Rows[k]["juid"].ToString();


            }




        }
        private void loadspareedit2()
        {
            string quy = "SELECT jluid,slno,description as Complients,juid  FROM jobcardlist where  juid=" + Genclass.h + "  and   jobtype=8   ";
            Genclass.cmd = new SqlCommand(quy, conn);
            
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr.Fill(tap1);





            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = DataGridDescription.Rows.Add();

                DataGridDescription.Rows[index].Cells[0].Value = tap1.Rows[k]["jluid"].ToString();
                DataGridDescription.Rows[index].Cells[1].Value = tap1.Rows[k]["slno"].ToString();

                DataGridDescription.Rows[index].Cells[2].Value = tap1.Rows[k]["Complients"].ToString();

                DataGridDescription.Rows[index].Cells[3].Value = tap1.Rows[k]["juid"].ToString();


            }
         




        }
        private void loadspareedit3()
        {
            string quy = "SELECT jluid,slno,description as Service,juid   FROM jobcardlist where  juid=" + Genclass.h + "  and jobtype=9  ";
            Genclass.cmd = new SqlCommand(quy, conn);





            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr.Fill(tap1);



            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = dataGridServiceAdvice.Rows.Add();

                dataGridServiceAdvice.Rows[index].Cells[0].Value = tap1.Rows[k]["jluid"].ToString();
                dataGridServiceAdvice.Rows[index].Cells[1].Value = tap1.Rows[k]["slno"].ToString();

                dataGridServiceAdvice.Rows[index].Cells[2].Value = tap1.Rows[k]["Service"].ToString();

                dataGridServiceAdvice.Rows[index].Cells[3].Value = tap1.Rows[k]["juid"].ToString();


            }





        }
        private void txtRegNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void panAdd_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtCustComp_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtServiceAdvise_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAcc_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtmech_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtmech.Text != string.Empty)
                {
                    SqlParameter[] para = { new SqlParameter("@mechanicname", txtRegNo.Text) };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_mech", para);

                    txtmech.Tag = dt.Rows[0]["Uid"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtmech_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("CustomerName Like '%{0}%' or  RegNo Like '%{1}%'  ", txtSearch.Text, txtSearch.Text);
        }

        private void grFront_Enter(object sender, EventArgs e)
        {

        }

        private void dtpJobCardDate_ValueChanged(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }
    }
}

﻿namespace Service
{
    partial class Frmbilllookup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmbilllookup));
            this.tabC = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtscr9 = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.txtscr2 = new System.Windows.Forms.TextBox();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.Hfgp = new System.Windows.Forms.DataGridView();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.cbogrp = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.txtscr3 = new System.Windows.Forms.TextBox();
            this.HFGP4 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtscr6 = new System.Windows.Forms.TextBox();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.HFGP2 = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.txtscr8 = new System.Windows.Forms.TextBox();
            this.txtscr7 = new System.Windows.Forms.TextBox();
            this.HFGP3 = new System.Windows.Forms.DataGridView();
            this.panit = new System.Windows.Forms.Panel();
            this.txtuom = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtgrp = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtitemname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Txtitcode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.hfgt = new System.Windows.Forms.DataGridView();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.txtuomid = new System.Windows.Forms.TextBox();
            this.txtgrpid = new System.Windows.Forms.TextBox();
            this.tabC.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Hfgp)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP4)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP2)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP3)).BeginInit();
            this.panit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hfgt)).BeginInit();
            this.SuspendLayout();
            // 
            // tabC
            // 
            this.tabC.Controls.Add(this.tabPage1);
            this.tabC.Controls.Add(this.tabPage2);
            this.tabC.Controls.Add(this.tabPage3);
            this.tabC.Controls.Add(this.tabPage4);
            this.tabC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabC.Location = new System.Drawing.Point(1, 4);
            this.tabC.Name = "tabC";
            this.tabC.SelectedIndex = 0;
            this.tabC.Size = new System.Drawing.Size(895, 432);
            this.tabC.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtscr9);
            this.tabPage1.Controls.Add(this.button7);
            this.tabPage1.Controls.Add(this.btnadd);
            this.tabPage1.Controls.Add(this.txtscr2);
            this.tabPage1.Controls.Add(this.btnaddrcan);
            this.tabPage1.Controls.Add(this.txtscr1);
            this.tabPage1.Controls.Add(this.Hfgp);
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Controls.Add(this.cbogrp);
            this.tabPage1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(887, 403);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "From PO";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtscr9
            // 
            this.txtscr9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr9.Location = new System.Drawing.Point(629, 7);
            this.txtscr9.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr9.Name = "txtscr9";
            this.txtscr9.Size = new System.Drawing.Size(92, 26);
            this.txtscr9.TabIndex = 186;
            this.txtscr9.TextChanged += new System.EventHandler(this.txtscr9_TextChanged);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(7, 362);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(90, 30);
            this.button7.TabIndex = 185;
            this.button7.Text = "Add Item";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(347, 361);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(61, 32);
            this.btnadd.TabIndex = 90;
            this.btnadd.Text = "Ok";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // txtscr2
            // 
            this.txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr2.Location = new System.Drawing.Point(225, 7);
            this.txtscr2.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr2.Name = "txtscr2";
            this.txtscr2.Size = new System.Drawing.Size(404, 26);
            this.txtscr2.TabIndex = 91;
            this.txtscr2.TextChanged += new System.EventHandler(this.txtscr2_TextChanged_1);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(418, 361);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(66, 32);
            this.btnaddrcan.TabIndex = 89;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(7, 7);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(218, 26);
            this.txtscr1.TabIndex = 90;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged_1);
            // 
            // Hfgp
            // 
            this.Hfgp.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Hfgp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Hfgp.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.Hfgp.Location = new System.Drawing.Point(6, 38);
            this.Hfgp.Name = "Hfgp";
            this.Hfgp.Size = new System.Drawing.Size(878, 319);
            this.Hfgp.TabIndex = 1;
            this.Hfgp.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Hfgp_CellClick);
            this.Hfgp.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Hfgp_CellContentClick);
            this.Hfgp.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.Hfgp_CellValueChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(312, 174);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 93;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // cbogrp
            // 
            this.cbogrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbogrp.FormattingEnabled = true;
            this.cbogrp.Location = new System.Drawing.Point(333, 171);
            this.cbogrp.Name = "cbogrp";
            this.cbogrp.Size = new System.Drawing.Size(234, 26);
            this.cbogrp.TabIndex = 92;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.txtscr4);
            this.tabPage2.Controls.Add(this.txtscr3);
            this.tabPage2.Controls.Add(this.HFGP4);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(887, 403);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "From GRN";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button1.Location = new System.Drawing.Point(320, 361);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(61, 32);
            this.button1.TabIndex = 97;
            this.button1.Text = "Ok";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_3);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(391, 361);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(66, 32);
            this.button2.TabIndex = 96;
            this.button2.Text = "Back";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_3);
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(320, 12);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(560, 22);
            this.txtscr4.TabIndex = 93;
            this.txtscr4.TextChanged += new System.EventHandler(this.txtscr4_TextChanged_1);
            // 
            // txtscr3
            // 
            this.txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr3.Location = new System.Drawing.Point(6, 12);
            this.txtscr3.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr3.Name = "txtscr3";
            this.txtscr3.Size = new System.Drawing.Size(315, 22);
            this.txtscr3.TabIndex = 92;
            this.txtscr3.TextChanged += new System.EventHandler(this.txtscr3_TextChanged_1);
            // 
            // HFGP4
            // 
            this.HFGP4.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP4.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP4.Location = new System.Drawing.Point(6, 41);
            this.HFGP4.Name = "HFGP4";
            this.HFGP4.Size = new System.Drawing.Size(875, 316);
            this.HFGP4.TabIndex = 2;
            this.HFGP4.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP4_CellContentClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.txtscr6);
            this.tabPage3.Controls.Add(this.txtscr5);
            this.tabPage3.Controls.Add(this.HFGP2);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(887, 403);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Mapped Items";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button3.Location = new System.Drawing.Point(310, 364);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(61, 32);
            this.button3.TabIndex = 95;
            this.button3.Text = "Ok";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_2);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(381, 364);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(66, 32);
            this.button4.TabIndex = 94;
            this.button4.Text = "Back";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_2);
            // 
            // txtscr6
            // 
            this.txtscr6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr6.Location = new System.Drawing.Point(275, 15);
            this.txtscr6.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr6.Name = "txtscr6";
            this.txtscr6.Size = new System.Drawing.Size(399, 26);
            this.txtscr6.TabIndex = 93;
            this.txtscr6.TextChanged += new System.EventHandler(this.txtscr6_TextChanged_1);
            // 
            // txtscr5
            // 
            this.txtscr5.AcceptsReturn = true;
            this.txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(6, 15);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(270, 26);
            this.txtscr5.TabIndex = 92;
            this.txtscr5.TextChanged += new System.EventHandler(this.txtscr5_TextChanged_1);
            // 
            // HFGP2
            // 
            this.HFGP2.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP2.Location = new System.Drawing.Point(6, 44);
            this.HFGP2.Name = "HFGP2";
            this.HFGP2.Size = new System.Drawing.Size(875, 316);
            this.HFGP2.TabIndex = 2;
            this.HFGP2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP2_CellClick);
            this.HFGP2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP2_CellContentClick);
            this.HFGP2.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP2_CellValueChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button5);
            this.tabPage4.Controls.Add(this.button6);
            this.tabPage4.Controls.Add(this.txtscr8);
            this.tabPage4.Controls.Add(this.txtscr7);
            this.tabPage4.Controls.Add(this.HFGP3);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(887, 403);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Items List";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button5.Location = new System.Drawing.Point(321, 366);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(61, 32);
            this.button5.TabIndex = 95;
            this.button5.Text = "Ok";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_2);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(392, 366);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(66, 32);
            this.button6.TabIndex = 94;
            this.button6.Text = "Back";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_2);
            // 
            // txtscr8
            // 
            this.txtscr8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr8.Location = new System.Drawing.Point(276, 15);
            this.txtscr8.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr8.Name = "txtscr8";
            this.txtscr8.Size = new System.Drawing.Size(399, 26);
            this.txtscr8.TabIndex = 93;
            this.txtscr8.TextChanged += new System.EventHandler(this.txtscr8_TextChanged_1);
            // 
            // txtscr7
            // 
            this.txtscr7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr7.Location = new System.Drawing.Point(7, 15);
            this.txtscr7.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr7.Name = "txtscr7";
            this.txtscr7.Size = new System.Drawing.Size(270, 26);
            this.txtscr7.TabIndex = 92;
            this.txtscr7.TextChanged += new System.EventHandler(this.txtscr7_TextChanged_1);
            // 
            // HFGP3
            // 
            this.HFGP3.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP3.Location = new System.Drawing.Point(6, 44);
            this.HFGP3.Name = "HFGP3";
            this.HFGP3.Size = new System.Drawing.Size(875, 316);
            this.HFGP3.TabIndex = 2;
            this.HFGP3.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP3_CellClick);
            this.HFGP3.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP3_CellContentClick);
            this.HFGP3.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP3_CellValueChanged);
            // 
            // panit
            // 
            this.panit.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panit.Controls.Add(this.txtuom);
            this.panit.Controls.Add(this.label7);
            this.panit.Controls.Add(this.txtgrp);
            this.panit.Controls.Add(this.label24);
            this.panit.Controls.Add(this.txtitemname);
            this.panit.Controls.Add(this.label3);
            this.panit.Controls.Add(this.Txtitcode);
            this.panit.Controls.Add(this.label2);
            this.panit.Controls.Add(this.hfgt);
            this.panit.Controls.Add(this.button8);
            this.panit.Controls.Add(this.button9);
            this.panit.Controls.Add(this.txtuomid);
            this.panit.Controls.Add(this.txtgrpid);
            this.panit.Location = new System.Drawing.Point(-3, -2);
            this.panit.Name = "panit";
            this.panit.Size = new System.Drawing.Size(809, 438);
            this.panit.TabIndex = 187;
            // 
            // txtuom
            // 
            this.txtuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuom.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom.Location = new System.Drawing.Point(4, 90);
            this.txtuom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtuom.Name = "txtuom";
            this.txtuom.Size = new System.Drawing.Size(187, 22);
            this.txtuom.TabIndex = 138;
            this.txtuom.Click += new System.EventHandler(this.txtuom_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 69);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 16);
            this.label7.TabIndex = 140;
            this.label7.Text = "UoM";
            // 
            // txtgrp
            // 
            this.txtgrp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrp.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrp.Location = new System.Drawing.Point(195, 90);
            this.txtgrp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrp.Name = "txtgrp";
            this.txtgrp.Size = new System.Drawing.Size(246, 22);
            this.txtgrp.TabIndex = 139;
            this.txtgrp.Click += new System.EventHandler(this.txtgrp_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(192, 69);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(79, 16);
            this.label24.TabIndex = 141;
            this.label24.Text = "Item Group";
            // 
            // txtitemname
            // 
            this.txtitemname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitemname.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitemname.Location = new System.Drawing.Point(195, 36);
            this.txtitemname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtitemname.MaxLength = 100;
            this.txtitemname.Name = "txtitemname";
            this.txtitemname.Size = new System.Drawing.Size(612, 22);
            this.txtitemname.TabIndex = 117;
            this.txtitemname.TextChanged += new System.EventHandler(this.txtitemname_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(194, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 16);
            this.label3.TabIndex = 118;
            this.label3.Text = "ItemName";
            // 
            // Txtitcode
            // 
            this.Txtitcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtitcode.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtitcode.Location = new System.Drawing.Point(4, 36);
            this.Txtitcode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Txtitcode.MaxLength = 20;
            this.Txtitcode.Name = "Txtitcode";
            this.Txtitcode.Size = new System.Drawing.Size(188, 22);
            this.Txtitcode.TabIndex = 116;
            this.Txtitcode.TextChanged += new System.EventHandler(this.Txtitcode_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1, 16);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 16);
            this.label2.TabIndex = 115;
            this.label2.Text = "Itemcode";
            // 
            // hfgt
            // 
            this.hfgt.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.hfgt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.hfgt.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.hfgt.Location = new System.Drawing.Point(4, 125);
            this.hfgt.Margin = new System.Windows.Forms.Padding(4);
            this.hfgt.Name = "hfgt";
            this.hfgt.ReadOnly = true;
            this.hfgt.Size = new System.Drawing.Size(804, 260);
            this.hfgt.TabIndex = 93;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button8.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Image = ((System.Drawing.Image)(resources.GetObject("button8.Image")));
            this.button8.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button8.Location = new System.Drawing.Point(306, 399);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(110, 32);
            this.button8.TabIndex = 92;
            this.button8.Text = "Save && Insert";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button9.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Image = ((System.Drawing.Image)(resources.GetObject("button9.Image")));
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.Location = new System.Drawing.Point(422, 399);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(66, 32);
            this.button9.TabIndex = 91;
            this.button9.Text = "Back";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // txtuomid
            // 
            this.txtuomid.Location = new System.Drawing.Point(197, 208);
            this.txtuomid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtuomid.Name = "txtuomid";
            this.txtuomid.Size = new System.Drawing.Size(64, 20);
            this.txtuomid.TabIndex = 142;
            // 
            // txtgrpid
            // 
            this.txtgrpid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrpid.Location = new System.Drawing.Point(584, 208);
            this.txtgrpid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrpid.Name = "txtgrpid";
            this.txtgrpid.Size = new System.Drawing.Size(27, 22);
            this.txtgrpid.TabIndex = 143;
            // 
            // Frmbilllookup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 435);
            this.Controls.Add(this.tabC);
            this.Controls.Add(this.panit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frmbilllookup";
            this.Text = "Bill Accounting look Up";
            this.Load += new System.EventHandler(this.Frmbilllookup_Load);
            this.tabC.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Hfgp)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP4)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP2)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP3)).EndInit();
            this.panit.ResumeLayout(false);
            this.panit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hfgt)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabC;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.DataGridView Hfgp;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox cbogrp;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox txtscr3;
        private System.Windows.Forms.DataGridView HFGP4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView HFGP2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DataGridView HFGP3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Panel panit;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.DataGridView hfgt;
        private System.Windows.Forms.TextBox txtitemname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txtitcode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtuom;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtgrp;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtuomid;
        private System.Windows.Forms.TextBox txtgrpid;
        internal System.Windows.Forms.Button btnadd;
        internal System.Windows.Forms.Button button1;
        internal System.Windows.Forms.Button button3;
        internal System.Windows.Forms.Button button5;
        internal System.Windows.Forms.TextBox txtscr2;
        internal System.Windows.Forms.TextBox txtscr1;
        internal System.Windows.Forms.TextBox txtscr6;
        internal System.Windows.Forms.TextBox txtscr5;
        internal System.Windows.Forms.TextBox txtscr8;
        internal System.Windows.Forms.TextBox txtscr7;
        internal System.Windows.Forms.TextBox txtscr9;

    }
}